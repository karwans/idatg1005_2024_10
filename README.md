# Portfolio project IDATA1003 - 2023


Welcome to our project repository for the "MatVettApp" application developed by Group 10 in the 
System Development course at NTNU Gjøvik. 
"MatVettApp"
is developed as part of the course project to demonstrate our skills in software development,
including teamwork, project management, and software engineering principles.
This document provides an overview of the project,
its structure, and how to run the application and tests.

## Team Members
- Karwan Shekhe
- Henrik Weflen Gjermundsen 
- Mikal Andre Løftamo Øverland

<br><br>

## Project description

In this project, we have developed a desktop application called "MatVett" 
that helps users manage their groceries, recipes, and shopping lists. 
The application allows users to add, edit, and delete groceries and recipes, 
as well as create shopping lists based on the available groceries and recipes. 
The goal of the application is to help users keep track of their food items, plan meals, 
and generate shopping lists to streamline the grocery shopping process.

Our team has implemented the application using JavaFX for the graphical user interface (GUI)
and Java for the backend logic. 
The application features a user-friendly interface with
separate views for managing groceries, recipes, and shopping lists. 

Some key features of the application include:
 - Viewing recipes in a categorized manner
 - Searching for recipes based on name
 - Generating shopping lists based on selected recipes
 - Adding recipes to favorites for quick access
 - Logging recently viewed recipes, to easily revisit them
 - Printing/saving a recipe to local device
 - Managing groceries and updating inventory levels
 - Creating shopping lists based on available groceries and recipes
 - Exporting shopping lists to a text file for easy reference
 - Saving a specific shopping under "My Shopping Lists" list for weekly/monthly shopping, or future use
 - Adding custom items to the shopping list
 - Inventory management for groceries
 - Adding, editing, and deleting groceries from the inventory
 - Saving and loading data to/from local files for persistence



## Project Structure

The project is organized into a modular architecture with separate packages for backend, 
frontend, and common utilities. 
Below is an overview of each package and its role in the application:

### Backend
Responsible for the application's business logic, data handling, and interaction with the file system.

- `accountmanager`: Handles user account creation, authentication, and management.
- `food`: Contains domain entities such as `Grocery` and `Recipe`.
- `managerclasses`: Provides management logic for groceries and recipes.
- `storageclasses`: Responsible for data persistence, including `Inventory` and `ShoppingList` storage.
- `commonutilities`: Shared utilities across the backend, including data storage and error handling.
    - `datastorage`: Manages data access and storage for inventory and recipes.
    - `errorhandler`: Custom exception handling and application-wide error management.
    - `filehandler`: File input/output operations, specifically for groceries and recipes.

### Frontend
Contains the user interface components and controllers to manage user interaction.

- `controllers`: Control the flow of data in the GUI and handle user input.
- `graphicalutility`: Reusable GUI components such as buttons, dialogs, and layouts.
- `guiclasses`: Defines the graphical user interface for different application features like home, inventory, and shopping list views.

### Common Utilities
Shared resources and utilities used by both frontend and backend.

- `app`: Main application entry point that ties together backend and frontend components.

### Main Application Components

- `AppEnsemble`: The main class that launches the application and initializes all components.
- `MainApplication`: The entry point for the JavaFX application, setting up the primary stage and scene.

Each package is designed
to function independently while interacting with each other to form a cohesive application.

<br><br><br><br><br>


## File Organization:
### Source Code

- `src`
    - `main`
        - `java`
            - `idatg1005.gruppe10.matvetapp`
                - `backend`
                    - `accountmanager`
                        - [AccountType.java](src/main/java/idatg1005/gruppe10/matvettapp/backend/accountmanager/AccountType.java)
                        - [Admin.java](src/main/java/idatg1005/gruppe10/matvettapp/backend/accountmanager/Admin.java)
                        - [EndUser.java](src/main/java/idatg1005/gruppe10/matvettapp/backend/accountmanager/EndUser.java)
                        - [LoginManager.java](src/main/java/idatg1005/gruppe10/matvettapp/backend/accountmanager/LoginManager.java)
                        - [ThirdParty.java](src/main/java/idatg1005/gruppe10/matvettapp/backend/accountmanager/ThirdParty.java)
                        - [UserAccount.java](src/main/java/idatg1005/gruppe10/matvettapp/backend/accountmanager/UserAccount.java)
                    - `food`
                        - [Grocery.java](src/main/java/idatg1005/gruppe10/matvettapp/backend/food/Grocery.java)
                        - [Recipe.java](src/main/java/idatg1005/gruppe10/matvettapp/backend/food/Recipe.java)
                    - `managerclasses`
                        - [GroceryManager.java](src/main/java/idatg1005/gruppe10/matvettapp/backend/managerclasses/GroceryManager.java)
                        - [RecipeManager.java](src/main/java/idatg1005/gruppe10/matvettapp/backend/managerclasses/RecipeManager.java)
                    - `storageclasses`
                        - [Inventory.java](src/main/java/idatg1005/gruppe10/matvettapp/backend/storageclasses/Inventory.java)
                        - [ShoppingList.java](src/main/java/idatg1005/gruppe10/matvettapp/backend/storageclasses/ShoppingList.java)
                - `commonutilities`
                    - `datastorage`
                        - [GroceryDataStorage.java](src/main/java/idatg1005/gruppe10/matvettapp/commonutilities/datastorage/GroceryDataStorage.java)
                        - [RecipeDataStorage.java](src/main/java/idatg1005/gruppe10/matvettapp/commonutilities/datastorage/RecipeDataStorage.java)
                        - [StorageUtility.java](src/main/java/idatg1005/gruppe10/matvettapp/commonutilities/datastorage/StorageUtility.java)
                    - `errorhandler`
                        - [ErrorHandler.java](src/main/java/idatg1005/gruppe10/matvettapp/commonutilities/errorhandler/ErrorHandler.java)
                        - [ErrorType.java](src/main/java/idatg1005/gruppe10/matvettapp/commonutilities/errorhandler/ErrorType.java)
                    - `filehandler`
                        - [GroceryFileHandler.java](src/main/java/idatg1005/gruppe10/matvettapp/commonutilities/filehandler/GroceryFileHandler.java)
                        - [RecipeFileHandler.java](src/main/java/idatg1005/gruppe10/matvettapp/commonutilities/filehandler/RecipeFileHandler.java)
                - `frontend`
                    - `controllers`
                        - [HomeController.java](src/main/java/idatg1005/gruppe10/matvettapp/frontend/controllers/HomeController.java)
                        - [InterfaceCategoryController.java](src/main/java/idatg1005/gruppe10/matvettapp/frontend/controllers/InterfaceCategoryController.java)
                        - [InventoryController.java](src/main/java/idatg1005/gruppe10/matvettapp/frontend/controllers/InventoryController.java)
                        - [RecipeController.java](src/main/java/idatg1005/gruppe10/matvettapp/frontend/controllers/RecipeController.java)
                        - [RecipeDetailViewController.java](src/main/java/idatg1005/gruppe10/matvettapp/frontend/controllers/RecipeDetailViewController.java)
                        - [ShoppingListController.java](src/main/java/idatg1005/gruppe10/matvettapp/frontend/controllers/ShoppingListController.java)
                    - `graphicalutility`
                        - [ButtonFactory.java](src/main/java/idatg1005/gruppe10/matvettapp/frontend/graphicalutilities/ButtonFactory.java)
                        - [CategoryAccordion.java](src/main/java/idatg1005/gruppe10/matvettapp/frontend/graphicalutilities/CategoryAccordion.java)
                        - [CommonLayouts.java](src/main/java/idatg1005/gruppe10/matvettapp/frontend/graphicalutilities/CommonLayouts.java)
                        - [DialogFactory.java](src/main/java/idatg1005/gruppe10/matvettapp/frontend/graphicalutilities/DialogFactory.java)
                        - [EditableTableCell.java](src/main/java/idatg1005/gruppe10/matvettapp/frontend/graphicalutilities/EditableTableCell.java)
                        - [GuiErrorDisplay.java](src/main/java/idatg1005/gruppe10/matvettapp/frontend/graphicalutilities/GuiErrorDisplay.java)
                    - `guiclasses`
                        - [HomeGui.java](src/main/java/idatg1005/gruppe10/matvettapp/frontend/guiclasses/HomeGui.java)
                        - [InventoryGui.java](src/main/java/idatg1005/gruppe10/matvettapp/frontend/guiclasses/InventoryGui.java)
                        - [LoginPage.java](src/main/java/idatg1005/gruppe10/matvettapp/frontend/guiclasses/LoginPage.java)
                        - [RecipeDetailView.java](src/main/java/idatg1005/gruppe10/matvettapp/frontend/guiclasses/RecipeDetailView.java)
                        - [RecipeGui.java](src/main/java/idatg1005/gruppe10/matvettapp/frontend/guiclasses/RecipeGui.java)
                        - [ShoppingListGui.java](src/main/java/idatg1005/gruppe10/matvettapp/frontend/guiclasses/ShoppingListGui.java)
                - [AppEnsemble.java](src/main/java/idatg1005/gruppe10/matvettapp/AppEnsemble.java) (Initializes the application components)
                - [MainApplication.java](src/main/java/idatg1005/gruppe10/matvettapp/MainApplication.java) (Main class to launch the application)

### Resources 

- `src`
    - `main`
        - `resources`
            - `css` (CSS styles for the application)
            - `fxml` (FXML files for the GUI layout)
            - `images` (Image resources used in the application) 
                - 'button-icons' (Icons for buttons)
                - 'recipe-images' (Images for recipes)
            - `recipes` (Recipe txt files containing recipe data)


### Test Code    

- `src`
    - `test`
        - `java`
            - `idatg1005.gruppe10.matvetapp`
                - `backend`
                    - `food`
                        - [GroceryTest.java](src/test/java/idatg1005/gruppe10/matvettapp/backend/food/GroceryTest.java)
                        - [RecipeTest.java](src/test/java/idatg1005/gruppe10/matvettapp/backend/food/RecipeTest.java)
                    - `managerclasses`
                        - [GroceryManagerTest.java](src/test/java/idatg1005/gruppe10/matvettapp/backend/managerclasses/GroceryManagerTest.java)
                        - [RecipeManagerTest.java](src/test/java/idatg1005/gruppe10/matvettapp/backend/managerclasses/RecipeManagerTest.java)
                    - `storageclasses`
                        - [InventoryTest.java](src/test/java/idatg1005/gruppe10/matvettapp/backend/storageclasses/InventoryTest.java)
                        - [ShoppingListTest.java](src/test/java/idatg1005/gruppe10/matvettapp/backend/storageclasses/ShoppingListTest.java) 
                - `commonutilities`
                    - `datastorage`
                        - [InventoryDataStorageTest.java](src/test/java/idatg1005/gruppe10/matvettapp/commonutilities/datastorage/InventoryDataStorageTest.java) 
                        - [RecipeDataStorageTest.java](src/test/java/idatg1005/gruppe10/matvettapp/commonutilities/datastorage/RecipeDataStorageTest.java)
                        - [ShoppingListDataStorageTest.java](src/test/java/idatg1005/gruppe10/matvettapp/commonutilities/datastorage/ShoppingListDataStorageTest.java)
                    - `errorhandler`
                        - [CustomExceptionUtilityTest.java](src/test/java/idatg1005/gruppe10/matvettapp/commonutilities/errorhandler/CustomExceptionUtilityTest.java) 
                        - [ErrorHandlerTest.java](src/test/java/idatg1005/gruppe10/matvettapp/commonutilities/errorhandler/ErrorHandlerTest.java) 
                    - `filehandler`
                        - [GroceryFileHandlerTest.java](src/test/java/idatg1005/gruppe10/matvettapp/commonutilities/filehandler/GroceryFileHandlerTest.java) 
                        - [RecipeFileHandlerTest.java](src/test/java/idatg1005/gruppe10/matvettapp/commonutilities/filehandler/RecipeFileHandlerTest.java) 
                - `frontend`
                    - `controllers`
                        - [HomeControllerTest.java](src/test/java/idatg1005/gruppe10/matvettapp/frontend/controllers/HomeControllerTest.java) 
                        - [InventoryControllerTest.java](src/test/java/idatg1005/gruppe10/matvettapp/frontend/controllers/InventoryControllerTest.java) 
                    - `guiclasses`
                        - [HomeGuiTest.java](src/test/java/idatg1005/gruppe10/matvettapp/frontend/guiclasses/HomeGuiTest.java)
                        - [InventoryGuiTest.java](src/test/java/idatg1005/gruppe10/matvettapp/frontend/guiclasses/InventoryGuiTest.java)
        - `resources`
            - `txt_test_files` (Test data files for unit tests)





<br><br><br><br>


## How to Run the Project

Ensure you have Java 21 and Maven installed on your system to run the MatVett application. Follow the steps below to get it up and running:

1. **Clone the repository**:
   Clone the project repository to your local machine using the following command in the terminal:
    ```sh
    git clone https://gitlab.stud.idi.ntnu.no/karwans/idatg1005_2024_10.git
    ```

2. **Navigate to the project directory**:
   Change to the project directory with this command:
    ```sh
    cd idatg1005_2024_10
    ```

3. **Build the project**:
   Build the project using Maven by running:
    ```sh
    mvn clean install
    ```

4. **Run the application**:
   Start the application with Maven's JavaFX plugin using:
    ```sh
    mvn javafx:run
    ```

## How to Run the Tests

Use the following Maven commands to run the tests within the project:

- **Run all tests**:
  Execute all tests with the following command:
    ```sh
    mvn test
    ```

- **Run specific test classes**:
  To run tests for a specific class, use this command (replacing `ClassName` with the actual test class name):
    ```sh
    mvn -Dtest=ClassName test
    ```

- **Run specific test methods**:
  To run a specific method within a test class, use this command (replacing `ClassName` with the test class name and `testMethodName` with the method name):
    ```sh
    mvn -Dtest=ClassName#testMethodName test
    ```
  

<br><br><br><br>

## Link to repository:

https://gitlab.stud.idi.ntnu.no/karwans/idatg1005_2024_10.git


## Link to resources that where helpfull during the developed of the project:

### JavaFX
https://openjfx.io/
https://docs.oracle.com/javafx/2/
https://www.javatpoint.com/javafx-tutorial
https://www.tutorialspoint.com/javafx/index.htm

### CSS Styling 
https://www.javatpoint.com/javafx-css
https://www.tutorialspoint.com/javafx/javafx_css.htm

### GSON/JSON
https://www.tutorialspoint.com/json/json_java_example.htm
https://www.oracle.com/technical-resources/articles/java/json.html
https://github.com/google/gson/blob/main/UserGuide.md
https://docs.oracle.com/javase/tutorial/jndi/objects/serial.html

### File Handling

https://docs.oracle.com/javase/tutorial/essential/io/file.html

### Other

https://docs.oracle.com/javase/8/docs/technotes/guides/collections/overview.html
https://blogs.oracle.com/javamagazine/post/java-21-now-available


