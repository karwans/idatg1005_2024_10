package idatg1005.gruppe10.matvettapp.frontend.controllers;

import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import idatg1005.gruppe10.matvettapp.backend.food.Recipe;
import idatg1005.gruppe10.matvettapp.backend.managerclasses.RecipeManager;
import idatg1005.gruppe10.matvettapp.backend.storageclasses.ShoppingList;
import idatg1005.gruppe10.matvettapp.frontend.graphicalutilities.ButtonFactory;
import idatg1005.gruppe10.matvettapp.frontend.graphicalutilities.DialogFactory;
import idatg1005.gruppe10.matvettapp.frontend.guiclasses.RecipeGui;
import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import idatg1005.gruppe10.matvettapp.commonutilities.filehandler.RecipeFileHandler;



/**
 * Controller class that manages the interactions within a detailed recipe view.
 * It handles operations such as adding/removing recipes from favorites,
 * downloading recipes, and generating shopping lists based on recipe ingredients.
 */
public class RecipeDetailViewController {

  private static final Logger LOGGER =
      Logger.getLogger("idatg1005.gruppe10.matvettapp.controllers.frontend.RecipeDetailViewController");
  private final Recipe recipe;
  private final RecipeGui recipeGui;
  private final ShoppingList shoppingList;
  private final RecipeManager recipeManager;
  private final Set<Recipe> favoriteRecipes;
  private final ShoppingListController shoppingListController;


  private Button favoriteButton;
  private Button downloadRecipeButton;
  private Button generateShoppingListButton;


  private static final String BUTTON_FAVORITE_ADDED = "button-favorite-added";
  private static final String BUTTON_FAVORITE_REMOVED = "button-favorite-removed";



  /**
   * Constructs a RecipeDetailViewController for managing detailed recipe interactions.
   *
   * @param recipe the recipe object this controller will manage
   * @param recipeGui the associated GUI class for displaying the recipe
   * @param shoppingList the shopping list to which ingredients may be added
   * @param favoriteRecipes a set of favorite recipes for the user
   * @param shoppingListController the controller that manages shopping list interactions
   */
  public RecipeDetailViewController(Recipe recipe, RecipeGui recipeGui, ShoppingList shoppingList,
      Set<Recipe> favoriteRecipes, ShoppingListController shoppingListController) {
    this.recipe = recipe;
    this.recipeGui = recipeGui;
    this.recipeManager = new RecipeManager();
    this.shoppingList = shoppingList;
    this.favoriteRecipes = favoriteRecipes;
    this.shoppingListController = shoppingListController;
    this.favoriteButton = getFavoriteButton();
    updateFavoriteButtonState();
  }




  /**
   * Creates and returns a button for adding a recipe to favorites.
   * This method configures the button with appropriate icons and
   * text based on the favorite status of the recipe.
   *
   * @return a configured favorite button
   */
  public Button getFavoriteButton() {
    favoriteButton =
        ButtonFactory.createButton(
            "Add to Favorites", "/images/button-icons/favorites-icon.png",
            event -> handleAddToFavorites());
    updateFavoriteButtonState();
    return favoriteButton;
  }




  /**
   * Creates and returns a button for generating a shopping list from recipe ingredients.
   * This method configures the button with an appropriate icon and action event.
   *
   * @return a configured shopping list generation button
   */
  public Button getGenerateShoppingListButton() {
    generateShoppingListButton =
        ButtonFactory.createButton(
            "Generate Shopping List", "/images/button-icons/generate-shopping-list.png",
            event -> handleGenerateShoppingList());
    return generateShoppingListButton;
  }




  /**
   * Creates and returns a button for downloading the recipe.
   * This method configures the button with an appropriate icon and action event.
   *
   * @return a configured download recipe button
   */
  public Button getDownloadRecipeButton() {
    downloadRecipeButton =
        ButtonFactory.createButton(
            "Download Recipe", "/images/button-icons/download-icon.png",
            event -> handleDownloadRecipe());

    return downloadRecipeButton;
  }




  /**
   * Handles the "Download Recipe" button click event.
   */
  private void handleDownloadRecipe() {
    String originalIconPath = "/images/button-icons/download-icon.png";
    FileChooser fileChooser = new FileChooser();

    // initial file name is the recipe name with spaces replaced by underscores
    fileChooser.setInitialFileName(
        recipe.getRecipeName().replaceAll("\\s+", "_") + ".txt");
    fileChooser.getExtensionFilters().add(
        new FileChooser.ExtensionFilter("Text Files", "*.txt"));
    File file = fileChooser.showSaveDialog(null);

    if (file != null) {
      RecipeFileHandler.saveRecipeToFile(this.recipe, file.getAbsolutePath());

      ButtonFactory.applyTemporaryStyle(downloadRecipeButton,
          "/images/button-icons/check-icon.png", "Recipe Saved",
          "-fx-background-color: green; -fx-text-fill: white;", 2000);
    } else {
      ButtonFactory.applyTemporaryStyle(downloadRecipeButton,
          originalIconPath, "Download Failed",
          "-fx-background-color: red; -fx-text-fill: white;", 2000);
    }
  }





  /**
   * Handles the "Add to Favorites" button click event.
   */
  private void handleAddToFavorites() {
    if (favoriteRecipes.contains(recipe)) {
      favoriteButton.setText("Removing from Favorites...");
      recipeGui.confirmAndRemoveRecipe(recipe);
      favoriteRecipes.remove(recipe);
      recipeManager.removeRecipeFromFavorites(recipe);
      revertFavoriteButtonStyle();

      //  setting the button text back to "Add to Favorites" after a delay
      new Timer().schedule(
          new TimerTask() {
            @Override
            public void run() {
              Platform.runLater(() -> setFavoriteButtonToAddState());
            }
          },
          1500 // delay in milliseconds
      );
    } else {
      favoriteButton.setText("Saving to Favorites...");
      favoriteRecipes.add(recipe);
      recipeGui.addRecipeToFavorites(recipe); // this method is also updating the GUI
      recipeManager.addRecipeToFavorites(recipe);

      // setting the button text back to "Remove from Favorites" after a delay
      new Timer().schedule(
          new TimerTask() {
            @Override
            public void run() {
              Platform.runLater(() -> setFavoriteButtonToRemoveState());
            }
          },
          1500
      );
    }
  }




  /**
   * Sets the favorite button to the "Add to Favorites" state.
   */
  private void setFavoriteButtonToAddState() {
    favoriteButton.getStyleClass().removeAll(BUTTON_FAVORITE_ADDED, BUTTON_FAVORITE_REMOVED);
    favoriteButton.getStyleClass().add(BUTTON_FAVORITE_ADDED);
    ButtonFactory.updateButton(favoriteButton,
        "/images/button-icons/favorites-icon.png", "Add to Favorites");
  }




  /**
   * Sets the favorite button to the "Remove from Favorites" state.
   */
  private void setFavoriteButtonToRemoveState() {
    favoriteButton.getStyleClass().removeAll(
        BUTTON_FAVORITE_ADDED, BUTTON_FAVORITE_REMOVED);
    favoriteButton.getStyleClass().add(BUTTON_FAVORITE_REMOVED);

    ButtonFactory.updateButton(favoriteButton,
        "/images/button-icons/remove-from-favorites-icon.png", "Remove from Favorites");
  }




  /**
   * Reverts the favorite button style to the default state.
   */
  private void revertFavoriteButtonStyle() {
    favoriteButton.getStyleClass().removeAll(BUTTON_FAVORITE_ADDED, BUTTON_FAVORITE_REMOVED);
    favoriteButton.getStyleClass().add("button");
  }



  /**
   * Updates the state of the favorite button based on the favorite status of the recipe.
   */
  private void updateFavoriteButtonState() {
    if (favoriteRecipes.contains(recipe)) {
      setFavoriteButtonToRemoveState();
    } else {
      setFavoriteButtonToAddState();
    }
  }




  /**
   * Handles the "Generate Shopping List" button click event.
   */
  private void handleGenerateShoppingList() {
    String originalIconPath = "/images/button-icons/generate-shopping-list.png";


    TableView<Grocery> table = DialogFactory.setupTableView();
    ObservableList<Grocery> data = FXCollections.observableArrayList();
    recipe.getIngredientDescriptions().forEach(
        ingredient -> data.add(new Grocery(DialogFactory.parseIngredientName(ingredient), ""))
    );
    table.setItems(data);

    DialogFactory.setTableSize(table, 5, 25); // default 5 rows, 25 cell size


    // Creating a dialog for setting quantities
    Dialog<List<Grocery>> dialog = DialogFactory.createDialog("Specify Quantities",
        "Set quantities for the ingredients:");
    dialog.getDialogPane().setContent(table);
    DialogFactory.setupDialogButtons(dialog, data);

    Optional<List<Grocery>> result = dialog.showAndWait();
    processDialogResult(result);

    if (result.isPresent()) {
      // success, shopping list generated:
      ButtonFactory.applyTemporaryStyle(generateShoppingListButton,
          "/images/button-icons/check-icon.png", "Shopping List Generated",
          "-fx-background-color: green; -fx-text-fill: white;", 2000);
    } else {
      // error or canceled by user:
      ButtonFactory.applyTemporaryStyle(generateShoppingListButton,
          originalIconPath, "Generate Failed",
          "-fx-background-color: red; -fx-text-fill: white;", 2000);
    }
  }



  /**
   * Processes the result of a dialog.
   * If the dialog result is present, the method adds new items to the shopping list.
   *
   * @param result the result of the dialog, containing a list of grocery items.
   */
  private void processDialogResult(Optional<List<Grocery>> result) {
    result.ifPresent(shoppingListItems -> {
      // Checking for new items in the shopping list
      List<Grocery> newItems = shoppingListItems.stream()
          .filter(item -> !this.shoppingList.getGroceryItems().contains(item))
          .toList();

      // Adding new items to the shopping list
      boolean listModified = !newItems.isEmpty();
      newItems.forEach(this.shoppingList::addGroceryItem);

      shoppingListController.updateShoppingListView();
      showConfirmationAlert(listModified);
    });
  }




  /**
   * Shows a confirmation alert.
   *
   * @param itemsAdded whether items were added
   */
  private void showConfirmationAlert(boolean itemsAdded) {
    Alert alert = getAlert(itemsAdded);

    alert.getDialogPane().setPrefSize(300, 200);

    // Showing the alert for 2.5 seconds
    alert.show();
    new Thread(() -> {
      try {
        Thread.sleep(2500); // Waiting before hiding the alert (in milliseconds)
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt(); // restoring the interrupted status
        LOGGER.log(Level.SEVERE, "Thread was interrupted during sleep", e);
      } finally {
        if (alert.isShowing()) {
          Platform.runLater(alert::hide);
        }
      }
    }).start();
  }



  /**
   * Creates an alert dialog for shopping list updates.
   *
   * @param itemsAdded whether items were added
   * @return the alert dialog
   */
  private static Alert getAlert(boolean itemsAdded) {
    Alert alert = new Alert(Alert.AlertType.INFORMATION);
    alert.setTitle("Shopping List Update");

    // Icons for the alert dialog:
    ImageView checkIcon =
        new ImageView(new Image("/images/button-icons/shopping-check-icon.png"));
    ImageView canceledIcon =
        new ImageView(new Image("/images/button-icons/canceled-shopping-icon.png"));

    checkIcon.setFitHeight(60);
    checkIcon.setFitWidth(60);
    checkIcon.setSmooth(true);
    checkIcon.setCache(true);

    if (itemsAdded) {
      alert.setGraphic(checkIcon);
      alert.setContentText("Successfully added new ingredients to the shopping list.");
    } else {
      alert.setGraphic(canceledIcon);
      alert.setContentText("No new ingredients were added to the shopping list.");
    }
    return alert;
  }


}
