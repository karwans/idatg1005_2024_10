package idatg1005.gruppe10.matvettapp.frontend.controllers;

import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import java.util.List;

/**
 * The {@code ICategoryController} interface provides the blueprint for creating controllers
 * that manage food categories.
 * Implementations of this interface should handle operations related to retrieving category data
 * and corresponding groceries within those categories.
 */
public interface InterfaceCategoryController {

  /**
   * Retrieves a list of all food categories.
   * This method is intended to provide a list of all the distinct categories available within
   * the grocery data source.
   * Each string in the returned list represents a unique category name.
   *
   * @return a {@code List} of {@code String} representing the names of all categories available.
   */
  List<String> getCategories();

  /**
   * Retrieves a list of {@code Grocery} objects that belong to a specific category.
   * This method allows fetching groceries that are classified under a particular category.
   * The method will filter all groceries by the specified category.
   *
   * @param category the category for which groceries are to be retrieved.
   * @return a list of {@code Grocery} objects that are associated with the specified category.
   * @throws IllegalArgumentException if the provided category is {@code null}.
   */
  List<Grocery> getGroceriesForCategory(String category);
}
