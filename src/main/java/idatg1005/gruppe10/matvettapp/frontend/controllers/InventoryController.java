package idatg1005.gruppe10.matvettapp.frontend.controllers;

import static idatg1005.gruppe10.matvettapp.frontend.guiclasses.InventoryGui.DEFAULT_CATEGORY;

import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import idatg1005.gruppe10.matvettapp.backend.storageclasses.Inventory;
import idatg1005.gruppe10.matvettapp.frontend.guiclasses.InventoryGui;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;
import idatg1005.gruppe10.matvettapp.commonutilities.errorhandler.ErrorHandler;
import idatg1005.gruppe10.matvettapp.commonutilities.datastorage.InventoryDataStorage;


/**
 * Controller class for managing inventory operations in the GUI.
 * This class handles adding, removing items, saving changes, and updating the GUI view.
 */
public class InventoryController implements InterfaceCategoryController {

  private static final Logger LOGGER = Logger.getLogger("idatg1005.gruppe10.matvettapp.controllers.frontend.InventoryController");
  private static final String INVENTORY_GUI_NOT_SET = "Inventory GUI is not set";

  private Inventory inventory;
  private final InventoryDataStorage storage;  // Data storage for inventory
  private InventoryGui inventoryGui;




  /**
   * Constructs an InventoryController with a specified file path for data storage.
   *
   * @param inventory the inventory to be managed by this controller.
   */
  public InventoryController(Inventory inventory) {
    this.inventory = inventory;
    this.storage = new InventoryDataStorage();

    try {
      this.inventory = storage.loadInventory();
    } catch (Exception e) {
      LOGGER.severe("Failed to load inventory: " + e.getMessage());
      ErrorHandler.handleException(e, "Failed to load inventory");
    }

  }




  /**
   * Sets the GUI component associated with this controller and updates the table view.
   *
   * @param inventoryGui the InventoryGui to be updated by this controller.
   */
  public void setInventoryGui(InventoryGui inventoryGui) {
    this.inventoryGui = inventoryGui;
    updateTableView();
    updateAccordion();
  }



  /**
   * Saves changes to the inventory by writing them back to the data storage.
   * If an error occurs, an error message is displayed to the user.
   */
  public void saveChanges() {
    try {
      storage.saveInventory();

    } catch (Exception e) {
      LOGGER.severe("Failed to save inventory: " + e.getMessage());
      ErrorHandler.handleException(e, "Failed to save inventory");
    }
  }




  /**
   * Adds a new item to the inventory and updates the GUI.
   *
   * @param item The grocery item to be added.
   * @param category The category under which the item should be added.
   */
  public void addNewItem(Grocery item, String category) {
    inventory.addItem(item, category);
    updateTableView();
    updateAccordion();
    saveChanges();
  }




  /**
   * Removes an item from the inventory based on its description and updates the GUI.
   * And then updates the table view and accordion view in the GUI,
   * and lastly saves the changes to the data storage.
   *
   * @param itemDescription The description of the item to be removed.
   */
  public void removeItem(String itemDescription) {
    Grocery itemToRemove = inventory.getInventoryItems().stream()
        .filter(item -> item.getGroceryItemName().equalsIgnoreCase(itemDescription))
        .findFirst()
        .orElse(null);

    if (itemToRemove != null) {
      if (inventory.removeInventoryItem(itemToRemove, DEFAULT_CATEGORY)) {
        updateTableView();  // update the table view in the GUI
        updateAccordion();  // update the accordion view in the GUI
        saveChanges();      // save the changes
      } else {
        LOGGER.log(Level.SEVERE,
            "Item not removed. It might not exist in the default category: {0}", itemDescription);
        ErrorHandler.handleException(
            new IllegalArgumentException("Item not found in default category: " + itemDescription),
            "Failed to remove item because it might not exist in the default category.");
      }
    } else {
      LOGGER.log(Level.SEVERE, "Item not found: {0}", itemDescription);
      ErrorHandler.handleException(
          new IllegalArgumentException("Item not found: " + itemDescription),
          "Failed to remove item because it was not found.");
    }
  }




  /**
   * Updates the table view in the associated GUI.
   */
  public void updateTableView() {
    if (inventoryGui != null) {
      inventoryGui.updateTableView();

    } else {

      LOGGER.warning(INVENTORY_GUI_NOT_SET);
      ErrorHandler.handleException(new IllegalStateException(INVENTORY_GUI_NOT_SET),
          INVENTORY_GUI_NOT_SET);

      throw new IllegalStateException(INVENTORY_GUI_NOT_SET);
    }
  }




  /**
   * Updates the accordion view in the associated GUI.
   */
  public void updateAccordion() {
    if (inventoryGui != null) {
      inventoryGui.updateAccordion();

    } else {

      LOGGER.warning(INVENTORY_GUI_NOT_SET);
      ErrorHandler.handleException(new IllegalStateException(INVENTORY_GUI_NOT_SET),
          INVENTORY_GUI_NOT_SET);

      throw new IllegalStateException(INVENTORY_GUI_NOT_SET);
    }
  }




  /**
   * Retrieves all groceries for a specific category.
   *
   * @param category the category to retrieve groceries from.
   * @return a list of groceries in the specified category.
   */
  public List<Grocery> getGroceriesForCategory(String category) {
    return inventory.getGroceriesForCategory(category);
  }





  /**
   * Provides a BiConsumer action for adding items with quantity to the inventory,
   * using the default category.
   *
   * @return a BiConsumer taking a Grocery item and a Pair of String for quantity and unit.
   */
  public BiConsumer<Grocery, Pair<String, String>> getAddItemWithQuantityAction() {
    return (item, quantityUnit) -> addNewItem(item, DEFAULT_CATEGORY);
  }


  /**
   * Retrieves a list of all grocery items across all categories from the inventory.
   *
   * @return a list of all {@link Grocery} items in the inventory.
   */
  public List<Grocery> getInventoryItems() {
    return inventory.getInventoryItems();
  }




  /**
   * Retrieves a list of categories in the inventory.
   *
   * @return a list of category names.
   */
  public List<String> getCategories() {
    return inventory.getCategories();
  }


}
