package idatg1005.gruppe10.matvettapp.frontend.controllers;


import idatg1005.gruppe10.matvettapp.frontend.guiclasses.HomeGui;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import idatg1005.gruppe10.matvettapp.commonutilities.errorhandler.ErrorHandler;


/**
 * Controls user interactions within the HomeGUI,
 * particularly managing tab switching within the application.
 * This controller sets up and handles the events associated
 * with buttons for navigating to different parts
 * of the application such as Recipes, Shopping List, and Inventory.
 */
public class HomeController {

  private static final Logger LOGGER = Logger.getLogger("idatg1005.gruppe10.matvettapp.controllers.frontend.HomeController");
  private final HomeGui homeGui; // Reference to the HomeGUI for interaction management.
  private Consumer<String> tabSwitcher;  // Functional interface to handle tab switching actions.



  /**
   * Constructs a HomeController with a specified HomeGUI object.
   * This constructor also attaches event handlers to the GUI elements to manage user interactions.
   *
   * @param homeGui The HomeGUI instance this controller will manage.
   */
  public HomeController(HomeGui homeGui) {
    this.homeGui = homeGui;
    attachEventHandlers(); // Set up event handlers upon instantiation
  }




  /**
   * Sets the consumer that defines actions for tab switching based on button presses.
   * This method allows dynamic assignment of tab switch behavior post-construction.
   *
   * @param tabSwitcher The consumer that handles tab switching.
   */
  public void setOnTabSwitchRequested(Consumer<String> tabSwitcher) {
    this.tabSwitcher = tabSwitcher;
  }



  /**
   * Attaches event handlers to the navigation buttons in the HomeGUI.
   * This method ensures that button clicks result in appropriate actions being taken,
   * with error handling for unassigned actions.
   */
  private void attachEventHandlers() {

    homeGui.getRecipeButton().setOnAction(event ->
        handleButtonAction("Recipes"));

    homeGui.getShoppingListButton().setOnAction(event ->

        handleButtonAction("Shopping List"));

    homeGui.getInventoryButton().setOnAction(event ->
        handleButtonAction("Inventory"));

  }




  /**
   * Handles the action of switching tabs based on user interaction.
   * This method ensures the tab switcher is used to change tabs.
   * If the tab switcher is not initialized,
   * it throws and handles an IllegalStateException, otherwise,
   * it handles any unexpected exceptions that might occur during the tab switching process.
   *
   * @param tabName The name of the tab to switch to, as specified by the user interface.
   * @throws IllegalStateException If the tab switcher consumer is not initialized.
   */
  void handleButtonAction(String tabName) { // default, package-private access, for testing purposes
    try {
      if (tabSwitcher == null) {
        throw new IllegalStateException("Tab switcher consumer is not initialized.");
      }
      tabSwitcher.accept(tabName);

    } catch (IllegalStateException e) {
      // Log severe errors:
      LOGGER.log(Level.SEVERE, "Tab switcher consumer is not initialized for {0}.", tabName);
      ErrorHandler.handleException(e, HomeController.class.getName());

    } catch (Exception e) {
      // other types of exceptions that could occur during tab switching:
      LOGGER.log(Level.WARNING,
          "An error occurred while handling action for {0}: {1}",
          new Object[]{tabName, e.getMessage()});
      ErrorHandler.handleException(e, HomeController.class.getName());
    }
  }

}