package idatg1005.gruppe10.matvettapp.frontend.controllers;

import idatg1005.gruppe10.matvettapp.backend.food.Recipe;
import idatg1005.gruppe10.matvettapp.backend.managerclasses.RecipeManager;
import idatg1005.gruppe10.matvettapp.frontend.guiclasses.RecipeGui;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Controller class for managing interactions between the RecipeGUI and the RecipeManager.
 * This class facilitates operations such as searching, displaying recipes by category,
 * and managing favorites and recently viewed recipes.
 */
public class RecipeController {

  private final RecipeGui recipeGui;
  private final RecipeManager recipeManager;




  /**
   * Constructor for RecipeController.
   * Initializes the controller with the GUI and the manager responsible for recipe data.
   *
   * @param recipeGui The RecipeGUI instance this controller will interact with.
   * @param recipeManager The RecipeManager instance used for retrieving and managing recipes.
   */
  public RecipeController(RecipeGui recipeGui, RecipeManager recipeManager) {
    this.recipeGui = recipeGui;
    this.recipeManager = recipeManager;
  }




  /**
   * Searches for recipes based on the provided search text and
   * updates the recipe display in the GUI.
   * If the search text is empty or null, all recipes are displayed.
   *
   * @param searchText The search query text to filter recipes.
   */
  public void performSearch(String searchText) {
    Collection<Recipe> recipes;
    if (searchText == null || searchText.trim().isEmpty()) {
      recipes = recipeManager.getAllRecipes();
    } else {
      String lowerCaseSearchText = searchText.toLowerCase();
      recipes = recipeManager.getAllRecipes().stream()
          .filter(recipe -> recipe.getRecipeName().toLowerCase().contains(lowerCaseSearchText))
          .collect(Collectors.toList());
    }
    recipeGui.loadAndDisplayRecipes(recipes);
  }





  /**
   * Updates the view to display recipes of a specific category.
   *
   * @param category The category of recipes to display.
   */
  public void updateRecipeViewByCategory(String category) {
    Collection<Recipe> recipes;
    if ("All Recipes".equals(category)) {
      recipes = recipeManager.getAllRecipes();
    } else {
      recipes = recipeManager.getRecipesByCategory(category);
    }
    recipeGui.loadAndDisplayRecipes(recipes);
  }




  /**
   * Adds a recipe to the user's list of favorites and saves the updated list.
   *
   * @param recipe The recipe to be added to favorites.
   */
  public void addRecipeToFavorites(Recipe recipe) {
    recipeManager.addRecipeToFavorites(recipe);
    recipeManager.saveFavorites();
  }





  /**
   * Removes a recipe from the user's list of favorites and saves the updated list.
   *
   * @param recipe The recipe to be removed from favorites.
   */
  public void removeRecipeFromFavorites(Recipe recipe) {
    recipeGui.removeRecipeFromFavorites(recipe);
    recipeManager.saveFavorites();
  }


  /**
   * Adds a recipe to the list of recently viewed recipes and saves the updated list.
   *
   * @param recipe The recipe to add to the recently viewed list.
   */
  public void addRecipeToRecentlyViewed(Recipe recipe) {
    recipeGui.addToRecentlyViewedList(recipe.getRecipeName());
    recipeManager.addRecipeToRecentlyViewed(recipe.getRecipeName());
    recipeManager.saveRecentlyViewed();
  }
}
