package idatg1005.gruppe10.matvettapp.frontend.controllers;

import idatg1005.gruppe10.matvettapp.backend.managerclasses.GroceryManager;
import idatg1005.gruppe10.matvettapp.backend.storageclasses.ShoppingList;
import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import idatg1005.gruppe10.matvettapp.frontend.guiclasses.ShoppingListGui;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import javafx.application.Platform;
import javafx.util.Pair;
import idatg1005.gruppe10.matvettapp.commonutilities.datastorage.ShoppingListDataStorage;
import idatg1005.gruppe10.matvettapp.commonutilities.filehandler.GroceryFileHandler;



/**
 * Controls the interactions between the shopping list GUI and the data model.
 * It manages operations such as adding, removing, and saving items in a shopping list.
 */
public class ShoppingListController implements InterfaceCategoryController {
  private final ShoppingList shoppingList;
  private ShoppingListGui shoppingListGUI;
  private final GroceryManager groceryManager;
  private final ShoppingListDataStorage shoppingListDataStorage;




  /**
   * Constructs a ShoppingListController.
   *
   * @param shoppingList The shopping list being managed.
   */
  public ShoppingListController(ShoppingList shoppingList) {
    this.shoppingList = shoppingList;
    this.shoppingListDataStorage = new ShoppingListDataStorage();
    GroceryFileHandler groceryFileHandler = new GroceryFileHandler();

    String filePath = "src/main/resources/grocery-items.txt";
    this.groceryManager = new GroceryManager(filePath, groceryFileHandler);
  }




  /**
   * Sets the GUI associated with this controller.
   *
   * @param shoppingListGUI The GUI to be used with this controller.
   */
  public void setShoppingListGUI(ShoppingListGui shoppingListGUI) {
    this.shoppingListGUI = shoppingListGUI;
  }




  /**
   * Adds an item to the shopping list.
   *
   * @param item The item to be added.
   */
  public void addNewItem(Grocery item) {
    shoppingList.addGroceryItem(item);
    shoppingListGUI.updateTableView();
    shoppingListDataStorage.saveShoppingList(shoppingList);
  }




  /**
   * Removes an item from the shopping list based on its description.
   *
   * @param itemDescription The description of the item to be removed.
   */
  public void removeItem(String itemDescription) {
    shoppingList.getGroceryItems().stream()
        .filter(item -> item.getGroceryItemName().equals(itemDescription))
        .findFirst().ifPresent(this::performRemoval);
  }



  /**
   * Removes an item from the shopping list.
   *
   * @param itemToRemove The item to be removed.
   */
  public void performRemoval(Grocery itemToRemove) {
    shoppingList.removeGroceryItem(itemToRemove);
    shoppingListGUI.updateTableView();
    shoppingListDataStorage.saveShoppingList(shoppingList);
  }






  /**
   * Updates the view of the shopping list in the GUI.
   */
  public void updateShoppingListView() {
    shoppingListGUI.updateTableView();
  }




  /**
   * Retrieves the shopping list managed by this controller.
   *
   * @return The shopping list.
   */
  public ShoppingList getShoppingList() {
    return shoppingList;
  }



  /**
   * Retrieves a list of categories from the grocery manager.
   *
   * @return A list of category names.
   */
  public List<String> getCategories() {
    return groceryManager.getCategories();
  }




  /**
   * Retrieves a list of groceries for a specific category.
   *
   * @param category The category for which to retrieve groceries.
   * @return A list of groceries in the specified category.
   */
  public List<Grocery> getGroceriesForCategory(String category) {
    return groceryManager.getGroceriesForCategory(category);
  }




  /**
   * Adds a new item to the shopping list with a specified quantity.
   *
   * @param item The item to be added.
   * @param quantity The quantity of the item.
   * @param unit The unit of measurement for the quantity.
   */
  public void addNewItemWithQuantity(Grocery item, String quantity, String unit) {
    item.setQuantity(quantity);
    shoppingList.addGroceryItem(item);
    shoppingListGUI.updateTableView();
    shoppingListDataStorage.saveShoppingList(shoppingList);
  }



  /**
   * Retrieves a BiConsumer that adds an item with a specified quantity to the shopping list.
   *
   * @return A BiConsumer that adds an item with a specified quantity to the shopping list.
   */
  public BiConsumer<Grocery, Pair<String, String>> getAddItemWithQuantityAction() {
    return (item, quantityUnit) ->
        addNewItemWithQuantity(item, quantityUnit.getKey(), quantityUnit.getValue());
  }




  /**
   * Saves the current shopping list with a specified name.
   *
   * @param listName The name of the shopping list to save.
   */
  public void saveCurrentShoppingList(String listName) {
    Map<String, ShoppingList> lists = shoppingListDataStorage.loadMyShoppingLists();
    lists.put(listName, shoppingList);
    shoppingListDataStorage.saveMyShoppingLists(lists);
  }





  /**
   * Loads a shopping list with a specified name.
   *
   * @param listName The name of the shopping list to load.
   */
  public void loadShoppingList(String listName) {
    Map<String, ShoppingList> lists = shoppingListDataStorage.loadMyShoppingLists();
    if (lists.containsKey(listName)) {
      ShoppingList loadedList = lists.get(listName);
      shoppingList.replaceItems(loadedList.getGroceryItems());
      shoppingListGUI.updateTableView();
    }
  }




  /**
   * Retrieves a list of saved shopping lists.
   *
   * @return A list of saved shopping lists.
   */
  public List<String> getSavedShoppingLists() {
    return new ArrayList<>(shoppingListDataStorage.loadMyShoppingLists().keySet());
  }





  /**
   * Saves a shopping list with a specified name.
   *
   * @param listName The name of the shopping list to save.
   * @param shoppingList The shopping list to save.
   */
  public void saveMyShoppingList(String listName, ShoppingList shoppingList) {
    shoppingListDataStorage.saveMyShoppingListByName(listName, shoppingList);
  }


  /**
   * Deletes a specific shopping list and updates the UI and storage.
   *
   * @param listName The name of the shopping list to delete.
   */
  public void deleteShoppingList(String listName) {
    // checking if the list exists in the storage first
    Map<String, ShoppingList> lists = shoppingListDataStorage.loadMyShoppingLists();
    if (lists.containsKey(listName)) {

      // removing the list from the storage
      lists.remove(listName);
      shoppingListDataStorage.saveMyShoppingLists(lists); // save the updated list

      // updating the GUI if the current shopping list is the one being deleted:
      if (shoppingList != null && shoppingList.equals(lists.get(listName))) {
        Platform.runLater(() -> {
          shoppingListGUI.clearTableView(); // clearing the TableView
          shoppingListGUI.updateShoppingListsUI(); // refreshing the list in the UI
        });
      }
    }
  }


}
