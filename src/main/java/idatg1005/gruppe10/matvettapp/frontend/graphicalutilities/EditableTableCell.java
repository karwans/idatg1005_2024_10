package idatg1005.gruppe10.matvettapp.frontend.graphicalutilities;

import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.StringConverter;


/**
 * A custom implementation of a TextFieldTableCell that allows for editing of cell content.
 * This cell displays an edit icon when not in edit mode, and a text field when in edit mode.
 *
 * @param <S> The type of the TableView generic type (i.e., the type of the items in the table).
 * @param <T> The type of the content in the cell.
 */
public class EditableTableCell<S, T> extends TextFieldTableCell<S, T> {

  private final TextField textField = new TextField();


  private final StringConverter<T> converter;


  private final ImageView editIcon =
      new ImageView(new Image("/images/button-icons/edit-icon.png"));





  /**
   * Constructs an EditableTableCell using the provided StringConverter.
   * This converter is used to convert the edited string value back to its original type.
   *
   * @param converter converter used to convert the cell value from
   *                  a string back to its original type.
   */
  public EditableTableCell(StringConverter<T> converter) {
    this.converter = converter;
    editIcon.setFitHeight(16);
    editIcon.setFitWidth(16);
  }


  /**
   * Starts the editing process for the cell.
   * This method is called when the cell enters edit mode.
   * It creates a TextField for editing and sets it as the graphic for the cell.
   */
  @Override
  public void startEdit() {
    super.startEdit();  // calling the superclass method to start the edit
    if (isEmpty()) {
      return;
    }
    createTextField();         // creating a text field for editing
    setText(null);             // clearing the text
    setGraphic(textField);     // setting the text field as the graphic
    textField.selectAll();     // selecting all the text in the text field
    textField.requestFocus();  // setting the focus on the text field
  }





  /**
   * Cancels the editing process and reverts any changes.
   * This method is called when the cell exits edit mode.
   * It sets the cell's text to the original item and sets the edit icon as the graphic.
   */
  @Override
  public void cancelEdit() {
    super.cancelEdit();
    setText(getItem().toString());
    setGraphic(editIcon);
  }





  /**
   * Updates the item displayed by the cell and
   * adjusts its appearance based on whether the cell is editing or not.
   *
   * @param item The item to be displayed in the cell.
   * @param empty A flag indicating whether the cell is empty.
   */
  @Override
  public void updateItem(T item, boolean empty) {
    super.updateItem(item, empty);  // calling the superclass method to update the item
    if (empty) {
      setText(null);
      setGraphic(null);


    } else {
      if (isEditing()) {
        if (textField != null) {
          textField.setText(getString());   // setting the text field text

        }
        setText(null);     // clearing the text
        setGraphic(textField);

      } else {
        setText(getString());
        setGraphic(editIcon);
      }
    }
  }






  /**
   * Commits the edit and updates the item in the TableView, ensuring the data model is updated.
   *
   * @param newValue The new value to commit, typically the edited value.
   */
  @Override
  public void commitEdit(T newValue) {
    super.commitEdit(newValue);
    // getting the 'Grocery' object from the TableView
    Grocery grocery = (Grocery) getTableView().getItems().get(getIndex());
    grocery.setQuantity(newValue.toString());  // updating the quantity of the grocery object
    updateItem(newValue, false);  // updating the cell item and graphic
    getTableView().refresh();  // refreshing the TableView to show changes
  }





  /**
   * Creates a TextField for editing that is displayed when the cell is in edit mode.
   */
  private void createTextField() {
    textField.setText(getString());
    textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);


    // confirming the change when the text field loses focus,
    // meaning the user clicks away or goes to another GUI component:
    textField.focusedProperty().addListener((observable, oldValue, newValue) -> {
      if (!newValue) {
        commitEdit(converter.fromString(textField.getText()));
      }
    });
  }





  /**
   * Returns the string representation of the cell's item, or an empty string if the item is null.
   *
   * @return A string representation of the item, or an empty string if the item is null.
   */
  private String getString() {
    return getItem() == null ? "" : getItem().toString();
  }
}
