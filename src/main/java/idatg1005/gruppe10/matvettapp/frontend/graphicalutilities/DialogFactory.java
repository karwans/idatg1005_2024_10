package idatg1005.gruppe10.matvettapp.frontend.graphicalutilities;

import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.ObservableList;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.converter.DefaultStringConverter;


/**
 * A utility class for creating and configuring dialogs with tables for
 * displaying and editing items in JavaFX.
 */
public class DialogFactory {

  DialogFactory() {
    // no instantiation for this utility class needed.
  }



  /**
   * Creates a generic dialog for displaying a list of Grocery items.
   *
   * @param title The title of the dialog.
   * @param headerText The header text for the dialog.
   * @return A Dialog configured with the specified title and header text.
   */
  public static Dialog<List<Grocery>> createDialog(String title, String headerText) {
    Dialog<List<Grocery>> dialog = new Dialog<>();
    dialog.setTitle(title);
    dialog.setHeaderText(headerText);
    dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
    return dialog;
  }




  /**
   * Sets up a TableView for Grocery items with columns for ingredient name and quantity.
   *
   * @return A TableView for Grocery items that is editable.
   */
  public static TableView<Grocery> setupTableView() {
    TableView<Grocery> table = new TableView<>();
    table.setEditable(true);

    // columns for the table (ingredient name and quantity):
    TableColumn<Grocery, String> ingredientColumn =
        setupColumn("Ingredient", "groceryItemName");
    TableColumn<Grocery, String> quantityColumn =
        setupColumn("Quantity", "quantity");

    quantityColumn.setCellFactory(column -> new EditableTableCell<>(new DefaultStringConverter()));

    // committing changes on editing the quantity column:
    quantityColumn.setOnEditCommit(event -> {
      Grocery grocery = event.getRowValue();
      grocery.setQuantity(event.getNewValue());
    });

    table.getColumns().addAll(ingredientColumn, quantityColumn);
    table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    return table;
  }





  /**
   * Configures the preferred size of the TableView based on the number of rows and cell size.
   *
   * @param table The table to configure.
   * @param rowsToDisplay The number of rows to display without scrolling.
   * @param cellSize The height of each cell in the table.
   */
  public static void setTableSize(TableView<Grocery> table, int rowsToDisplay, double cellSize) {
    double tableHeight = rowsToDisplay * cellSize + cellSize; // the total height required
    table.setPrefHeight(tableHeight);
    table.setFixedCellSize(cellSize);
  }




  /**
   * Creates and configures a TableColumn with the specified title and property name.
   *
   * @param title The title of the column.
   * @param propertyName The property name of the data model to bind to the column.
   * @return A configured TableColumn.
   */
  public static TableColumn<Grocery, String> setupColumn(String title, String propertyName) {
    TableColumn<Grocery, String> column = new TableColumn<>(title);
    column.setCellValueFactory(new PropertyValueFactory<>(propertyName));
    return column;
  }




  /**
   * Configures the actions for dialog buttons, specifically handling the data collection on OK.
   *
   * @param dialog The dialog to configure buttons for.
   * @param data The observable list of data items displayed in the dialog's table.
   */
  public static void setupDialogButtons(
      Dialog<List<Grocery>> dialog, ObservableList<Grocery> data) {

    dialog.setResultConverter(
        dialogButton -> dialogButton == ButtonType.OK ? new ArrayList<>(data) : null);


  }




  /**
   * Parses and clean up an ingredient name from a descriptive string.
   * Removes parenthesized descriptions and splits by ':' to isolate the ingredient name.
   *
   * @param ingredientDesc The descriptive string of an ingredient.
   * @return The cleaned-up ingredient name.
   */
  public static String parseIngredientName(String ingredientDesc) {
    String nameWithoutParentheses =
        ingredientDesc.replaceAll("\\s*\\([^)]*\\)", "").trim();
    return nameWithoutParentheses.split(":")[0].trim();
  }
}