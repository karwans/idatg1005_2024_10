package idatg1005.gruppe10.matvettapp.frontend.graphicalutilities;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;


/**
 * Provides utility functions for displaying error messages in a GUI using alert dialog boxes.
 * This class simplifies the process of showing errors to the user in a standardized format
 * across an application.
 */
public class GuiErrorDisplay {


  GuiErrorDisplay() {
    // private constructor to prevent instantiation
  }

  /**
   * Displays an error message in a GUI dialog box.
   * This method ensures that the alert is displayed on the JavaFX application thread.
   *
   * @param message The error message to be displayed to the user.
   */
  public static void showError(String message) {

    Platform.runLater(() -> {
      Alert alert = new Alert(AlertType.ERROR);
      alert.setTitle("Error");
      alert.setHeaderText(null);  // No header text
      alert.setContentText(message);  // The actual error message
      alert.showAndWait();  // Blocks execution until the dialog is dismissed
    });


  }



}