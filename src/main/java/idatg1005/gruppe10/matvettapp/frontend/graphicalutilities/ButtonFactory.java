package idatg1005.gruppe10.matvettapp.frontend.graphicalutilities;

import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ContentDisplay;
import java.util.Objects;



/**
 * A utility class for creating and managing buttons with additional features like icons,
 * temporary style changes, and timed style resets.
 */
public class ButtonFactory {


  ButtonFactory() {
    // no instantiation of this utility class.
  }


  /**
   * Creates a button with text, an icon, and an action handler.
   *
   * @param buttonText The text to display on the button.
   * @param iconPath The resource path for the icon image.
   * @param action The event handler to execute when the button is pressed.
   * @return A fully configured Button with the specified text, icon, and action.
   */
  public static Button createButton(
      String buttonText, String iconPath, EventHandler<ActionEvent> action) {

    Button button = new Button(buttonText);   // Create a new button with the specified text

    // Load the icon image from the specified resource path
    ImageView icon =
        new ImageView(
            new Image(
                Objects.requireNonNull(ButtonFactory.class.getResourceAsStream(iconPath)))
        );

    icon.setFitHeight(30);
    icon.setFitWidth(30);
    button.setGraphic(icon);
    button.setContentDisplay(ContentDisplay.LEFT);
    button.setOnAction(action);
    return button;
  }



  /**
   * Updates the text and icon of an existing button.
   *
   * @param button The button to update.
   * @param iconPath The new resource path for the icon image.
   * @param newText The new text to display on the button.
   */
  public static void updateButton(Button button, String iconPath, String newText) {

    ImageView icon =
        new ImageView(
            new Image(
                Objects.requireNonNull(ButtonFactory.class.getResourceAsStream(iconPath)))
        );

    icon.setFitHeight(16);
    icon.setFitWidth(16);
    button.setGraphic(icon);
    button.setText(newText);
  }





  /**
   * Temporarily applies a new style to a button and automatically reverts to its original
   * style after a specified duration.
   * This method is useful for giving visual feedback to the user after an action.
   *
   * @param button The button to temporarily style.
   * @param iconPath The resource path for the temporary icon image.
   * @param newText The temporary text to display on the button.
   * @param newStyle The new style to apply to the button.
   * @param duration The duration in milliseconds after which the button
   *                 will revert to its original style.
   */
  public static void applyTemporaryStyle(
      Button button, String iconPath, String newText, String newStyle, long duration) {
    // saving existing values for later restoration:
    String originalText = button.getText();
    String originalStyle = button.getStyle();
    ImageView originalGraphic = (ImageView)button.getGraphic();

    // initiate the temporary style:
    updateButton(button, iconPath, newText);
    button.setStyle(newStyle);

    // planning to reset the style after the specified duration:
    new Thread(() -> {
      try {
        Thread.sleep(duration);
        Platform.runLater(() -> {
          button.setText(originalText);
          button.setGraphic(originalGraphic);
          button.setStyle(originalStyle);
        });
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
      }
    }).start();
  }


}
