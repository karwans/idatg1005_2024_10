package idatg1005.gruppe10.matvettapp.frontend.graphicalutilities;

import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;


/**
 * Utility class for creating common JavaFX layout components with predefined configurations.
 * This class simplifies the creation of boxes and
 * scroll panes with specific spacing and fill settings.
 */
public class CommonLayouts {

  CommonLayouts() {
    // no instantiation for this utility class needed.
  }



  /**
   * Creates a VBox with specified spacing between children.
   *
   * @param spacing the amount of vertical space between each child in the VBox.
   * @return a configured VBox with the specified spacing.
   */
  public static VBox createVboxWithSpacing(double spacing) {
    VBox vbox = new VBox(spacing);
    vbox.setFillWidth(true); // ensures that children will fill the height with the VBox
    return vbox;
  }





  /**
   * Creates an HBox with specified spacing between children.
   *
   * @param spacing the amount of horizontal space between each child in the HBox.
   * @return a configured HBox with the specified spacing.
   */
  public static HBox createHboxWithSpacing(double spacing) {
    HBox hbox = new HBox(spacing);
    hbox.setFillHeight(true); // ensures that children will fill the height with the HBox
    return hbox;
  }





  /**
   * Wraps a given Pane in a ScrollPane,
   * configuring it to optionally fit the content width and height.
   *
   * @param content the Pane to be wrapped in the ScrollPane.
   * @param fitWidth if true, the content width is bounded by the ScrollPane width.
   * @param fitHeight if true, the content height is bounded by the ScrollPane height.
   * @return a ScrollPane containing the provided Pane, with fitting behavior as specified.
   */
  public static ScrollPane wrapInScrollPane(Pane content, boolean fitWidth, boolean fitHeight) {
    ScrollPane scrollPane = new ScrollPane(content);
    scrollPane.setFitToWidth(fitWidth); //whether content width should fit the ScrollPane width
    scrollPane.setFitToHeight(fitHeight); //whether content height should fit the ScrollPane height
    return scrollPane;
  }


}
