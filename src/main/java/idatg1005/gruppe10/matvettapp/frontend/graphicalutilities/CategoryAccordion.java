package idatg1005.gruppe10.matvettapp.frontend.graphicalutilities;

import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import idatg1005.gruppe10.matvettapp.frontend.controllers.InterfaceCategoryController;
import idatg1005.gruppe10.matvettapp.frontend.guiclasses.InventoryGui;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Accordion;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.util.Pair;




/**
 * A custom Accordion component that displays categories of groceries.
 * Each category is presented as a TitledPane with its own set of grocery items
 * displayed as images with corresponding names below them.
 * The content is made scrollable to accommodate a large number of items.
 */
public class CategoryAccordion extends Accordion {

  private final InterfaceCategoryController categoryController;
  private final BiConsumer<Grocery, Pair<String, String>> itemClickAction;




  /**
   * Constructs a CategoryAccordion with a controller
   * to manage grocery categories and an action for item clicks.
   *
   * @param categoryController the controller for managing grocery categories.
   * @param itemClickAction action to execute on item click,
   *                        receiving the grocery and a pair of quantity and unit.
   */
  public CategoryAccordion(InterfaceCategoryController categoryController,
      BiConsumer<Grocery, Pair<String, String>> itemClickAction) {

    this.categoryController = categoryController;
    this.itemClickAction = itemClickAction;
    populateCategories();
  }




  /**
   * Populates the accordion with categories by creating a pane for each category.
   */
  public void populateCategories() {
    this.getPanes().clear(); // clearing existing panes to refresh
    List<String> categories = categoryController.getCategories(); // a call to getCategories

    // checking if there are any categories returned from the controller:
    if (categories.isEmpty()) {
      categories.add(InventoryGui.DEFAULT_CATEGORY); // Add default if empty
    }

    // creating a pane for each category:
    for (String category : categories) {
      TitledPane categoryPane = createCategoryPane(category);
      this.getPanes().add(categoryPane);
    }

  }





  /**
   * Creates a TitledPane for a specific category containing scrollable grocery items.
   *
   * @param category the category for which to create the pane.
   * @return a TitledPane with a scrollable content area filled with grocery items.
   */
  private TitledPane createCategoryPane(String category) {
    VBox itemLayout = createItemLayoutForCategory(category);

    return new TitledPane(category, itemLayout); // TitledPane with category name and item layout
  }




  /**
   * Creates the layout for grocery items within a category.
   *
   * @param category the category for which to create the item layout.
   * @return a VBox containing a ScrollPane with grocery items laid out.
   */
  private VBox createItemLayoutForCategory(String category) {
    FlowPane itemPane = new FlowPane(Orientation.HORIZONTAL);
    itemPane.setHgap(10);
    itemPane.setVgap(10);
    itemPane.setPadding(new Insets(10));

    List<Grocery> items = categoryController.getGroceriesForCategory(category);
    if (items.isEmpty()) {
      Label noItemsLabel = new Label("No items to display");

      noItemsLabel.setAlignment(Pos.CENTER);
      itemPane.getChildren().add(noItemsLabel);

    } else {

      for (Grocery item : items) {          // creating an ImageView and Label for each item
        ImageView imageView = createImageViewForItem(item);
        Label nameLabel = createLabelForItem(item);

        VBox vbox = new VBox(5);        // 5 is the spacing between the ImageView and Label
        vbox.setAlignment(Pos.TOP_CENTER);
        vbox.getChildren().addAll(imageView, nameLabel);
        vbox.setOnMouseClicked(event -> showQuantityDialog(item).ifPresent(quantityUnitPair ->
            itemClickAction.accept(item, quantityUnitPair)));

        itemPane.getChildren().add(vbox);
      }
    }

    return createScrollablePane(itemPane);
  }





  /**
   * Creates an ImageView for a grocery item.
   *
   * @param item the grocery item for which to create the ImageView
   * @return an ImageView representing the grocery item
   */
  private ImageView createImageViewForItem(Grocery item) {
    Image image = new Image("/images/default-image.png");
    ImageView imageView = new ImageView(image);

    imageView.setFitWidth(100);
    imageView.setFitHeight(100);
    imageView.setPreserveRatio(true);
    imageView.setSmooth(true);

    return imageView;
  }





  /**
   * Creates a Label for a grocery item.
   *
   * @param item the grocery item for which to create the Label
   * @return a Label with the name of the grocery item
   */
  private Label createLabelForItem(Grocery item) {
    Label nameLabel = new Label(item.getGroceryItemName());

    nameLabel.setWrapText(true);
    nameLabel.setMaxWidth(100);
    nameLabel.setAlignment(Pos.CENTER);

    return nameLabel;
  }





  /**
   * Wraps a FlowPane in a ScrollPane to make it scrollable.
   *
   * @param flowPane the FlowPane to be made scrollable
   * @return a VBox containing the ScrollPane wrapping the FlowPane
   */
  private VBox createScrollablePane(FlowPane flowPane) {
    ScrollPane scrollPane = new ScrollPane(flowPane);
    scrollPane.setFitToWidth(true);
    scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);

    return new VBox(scrollPane);  // VBox containing the ScrollPane
  }




  /**
   * Displays a dialog for entering the quantity and unit of a grocery item.
   *
   * @param item the grocery item for which the dialog is displayed.
   * @return Optional containing a Pair of quantity and unit if provided.
   */
  private Optional<Pair<String, String>> showQuantityDialog(Grocery item) {
    // initializer dialog
    Dialog<Pair<String, String>> dialog = new Dialog<>();
    dialog.setTitle("Add Quantity");
    dialog.setHeaderText("Enter the quantity for " + item.getGroceryItemName());

    // setting OK and Cancel buttons:
    dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

    // grid pane for quantity and unit input
    GridPane grid = new GridPane();
    grid.setHgap(10);
    grid.setVgap(10);
    grid.setPadding(new Insets(20, 150, 10, 10));

    TextField quantityField = new TextField();
    quantityField.setPromptText("Quantity");

    ComboBox<String> unitComboBox = new ComboBox<>();
    unitComboBox.getItems().addAll("Gram", "Kg", "Liter", "Pieces");

    grid.add(new Label("Quantity:"), 0, 0);
    grid.add(quantityField, 1, 0);
    grid.add(new Label("Unit:"), 0, 1);
    grid.add(unitComboBox, 1, 1);

    // activate OK button only when quantity is entered
    Node okButton = dialog.getDialogPane().lookupButton(ButtonType.OK);
    okButton.setDisable(true);

    // validation for quantity input
    quantityField.textProperty().addListener((observable, oldValue, newValue) ->
        okButton.setDisable(newValue.trim().isEmpty()));

    dialog.getDialogPane().setContent(grid);


    // return the quantity and unit if OK is pressed
    dialog.setResultConverter(dialogButton -> {
      if (dialogButton == ButtonType.OK) {
        return new Pair<>(quantityField.getText(), unitComboBox.getValue());
      }
      return null;
    });

    return dialog.showAndWait();
  }

}
