package idatg1005.gruppe10.matvettapp.frontend.guiclasses;

import idatg1005.gruppe10.matvettapp.backend.food.Recipe;
import idatg1005.gruppe10.matvettapp.backend.managerclasses.RecipeManager;
import idatg1005.gruppe10.matvettapp.backend.storageclasses.ShoppingList;
import idatg1005.gruppe10.matvettapp.frontend.controllers.RecipeDetailViewController;
import idatg1005.gruppe10.matvettapp.frontend.controllers.ShoppingListController;
import java.util.Objects;
import java.util.Set;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;



/**
 * A graphical user interface class that provides a detailed view of a recipe.
 * It displays information about a recipe including ingredients, instructions,
 * and allows interactions such as adding to favorites,
 * downloading, and generating shopping lists.
 */
public class RecipeDetailView extends VBox {


  private final Recipe recipe;
  private Set<Recipe> favoriteRecipes;
  private final RecipeDetailViewController recipeDetailViewController;

  private static final String RECIPE_DETAIL_CSS = "/css/recipe-detail-view.css";


  private static final String ARIAL_FONT = "Arial";




  /**
   * Constructs a detailed view for a given recipe, enabling management of recipe-specific tasks
   * such as favoring, shopping list generation, and recipe downloading.
   *
   * @param recipe the recipe to be displayed
   * @param shoppingList the shopping list associated with the user
   * @param shoppingListController the controller for managing shopping list interactions
   * @param recipeGui the GUI class that displays the recipe
   */
  public RecipeDetailView(Recipe recipe, ShoppingList shoppingList,
      ShoppingListController shoppingListController, RecipeGui recipeGui) {
    this.recipe = recipe;
    this.favoriteRecipes = loadedFavoriteRecipes();

    this.recipeDetailViewController = new RecipeDetailViewController(recipe, recipeGui,
        shoppingList, favoriteRecipes, shoppingListController);

    this.getStylesheets()
        .add(Objects.requireNonNull(getClass().getResource(RECIPE_DETAIL_CSS)).toExternalForm());

    initializeLayout();
  }




  /**
   * Loads the favorite recipes from the RecipeManager.
   *
   * @return the set of favorite recipes
   */
  private Set<Recipe> loadedFavoriteRecipes() {
    RecipeManager recipeManager = new RecipeManager();
    favoriteRecipes = recipeManager.getFavoriteRecipes();
    return favoriteRecipes;
  }




  /**
   * Initializes the layout of the recipe detail view.
   */
  private void initializeLayout() {
    this.getChildren().addAll(createButtonLayout(), createAllSections());
    this.setPadding(new Insets(10));
    this.setSpacing(10);
  }



  /**
   * Creates the layout for all sections of the recipe detail view.
   *
   * @return the layout for all sections of the recipe detail view
   */
  private VBox createAllSections() {
    VBox allSections
        = new VBox(10, createHeaderSection(), createImageAndDetails(), createBottomSection());
    allSections.setAlignment(Pos.TOP_CENTER);
    return allSections;
  }



  /**
   * Creates the layout for the image and details section of the recipe detail view.
   *
   * @return the layout for the image and details section of the recipe detail view
   */
  private HBox createImageAndDetails() {
    ImageView recipeImage = createRecipeImageView();
    VBox recipeDetailsSection = createRecipeDetailsSection();
    HBox imageAndDetails = new HBox(20, recipeImage, recipeDetailsSection);
    imageAndDetails.setAlignment(Pos.CENTER_LEFT);
    return imageAndDetails;
  }



  /**
   * Creates the layout for the bottom section of the recipe detail view.
   *
   * @return the layout for the bottom section of the recipe detail view
   */
  private HBox createBottomSection() {
    VBox ingredientsSection = createIngredientsSection();
    VBox instructionsSection = createInstructionsSection();
    HBox bottomSection = new HBox(20, ingredientsSection, instructionsSection);
    bottomSection.setAlignment(Pos.CENTER);
    return bottomSection;
  }




  /**
   * Creates the layout for the button section of the recipe detail view.
   *
   * @return the layout for the button section of the recipe detail view
   */
  private HBox createButtonLayout() {
    // Using ButtonFactory class to create buttons

    Button favoriteButton = recipeDetailViewController.getFavoriteButton();
    Button generateShoppingListButton = recipeDetailViewController.getGenerateShoppingListButton();
    Button downloadRecipeButton = recipeDetailViewController.getDownloadRecipeButton();

    HBox buttonLayout =
        new HBox(10, favoriteButton, generateShoppingListButton, downloadRecipeButton);
    buttonLayout.setAlignment(Pos.CENTER);
    buttonLayout.setPadding(new Insets(15, 0, 15, 0));
    return buttonLayout;
  }




  /**
   * Creates a button with the specified text and icon.
   *
   * @param buttonText the text of the button
   * @param iconPath the path to the icon of the button
   * @return the button with the specified text and icon
   */
  private Button createButton(String buttonText, String iconPath) {
    Button button = new Button(buttonText);
    ImageView icon = new ImageView(new Image(iconPath));
    icon.setFitHeight(30);
    icon.setFitWidth(30);
    button.setGraphic(icon);
    button.setContentDisplay(ContentDisplay.LEFT);
    return button;
  }




  /**
   * Creates the layout for the header section of the recipe detail view.
   *
   * @return the layout for the header section of the recipe detail view
   */
  private HBox createHeaderSection() {
    Label titleLabel = new Label(recipe.getRecipeName());

    titleLabel.setFont(Font.font(ARIAL_FONT, FontWeight.BOLD, 24));

    titleLabel.setWrapText(true);

    titleLabel.setTextAlignment(TextAlignment.CENTER);
    titleLabel.setMaxWidth(Double.MAX_VALUE);
    HBox.setHgrow(titleLabel, Priority.ALWAYS);

    HBox headerSection = new HBox(10, titleLabel, createRatingBox());
    headerSection.setAlignment(Pos.CENTER_LEFT);

    headerSection.setPadding(new Insets(10, 0, 10, 0));

    return headerSection;
  }



  /**
   * Creates the layout for the recipe details section of the recipe detail view.
   *
   * @return the layout for the recipe details section of the recipe detail view
   */
  private VBox createRecipeDetailsSection() {
    Label prepTimeLabel = new Label("Prep Time: " + recipe.getPrepTime());
    Label cookTimeLabel = new Label("Cook Time: " + recipe.getCookTime());
    VBox detailsSection = new VBox(5, prepTimeLabel, cookTimeLabel);
    detailsSection.setAlignment(Pos.TOP_CENTER);
    return detailsSection;
  }



  /**
   * Creates an ImageView for the recipe image.
   *
   * @return an ImageView for the recipe image
   */
  private ImageView createRecipeImageView() {
    ImageView recipeImage = new ImageView();

    String imagePath =
        getClass().getResource(
            recipe.getImagePath()) != null ? recipe.getImagePath() : "/images/default-image.png";

    recipeImage.setImage(
        new Image(Objects.requireNonNull(getClass().getResourceAsStream(imagePath)))
    );

    recipeImage.setFitWidth(200);
    recipeImage.setPreserveRatio(true);
    recipeImage.setOnMouseEntered(e -> recipeImage.setScaleX(1.15));
    recipeImage.setOnMouseExited(e -> recipeImage.setScaleX(1.0));
    return recipeImage;
  }



  /**
   * Creates the layout for the ingredients section of the recipe detail view.
   *
   * @return the layout for the ingredients section of the recipe detail view
   */
  private VBox createIngredientsSection() {
    ListView<String> ingredientsListView = new ListView<>();

    recipe.getIngredientDescriptions().forEach(ingredientsListView.getItems()::add);

    Label ingredientsLabel = new Label("Ingredients:");
    ingredientsLabel.setFont(Font.font(ARIAL_FONT, FontWeight.NORMAL, 18));

    VBox ingredientsSection = new VBox(5, ingredientsLabel, ingredientsListView);
    ingredientsSection.setPadding(new Insets(0, 10, 10, 10));

    return ingredientsSection;
  }



  /**
   * Creates the layout for the instructions section of the recipe detail view.
   *
   * @return the layout for the instructions section of the recipe detail view
   */
  private VBox createInstructionsSection() {
    TextArea instructionArea = new TextArea(recipe.getInstructions());

    instructionArea.setEditable(false);
    instructionArea.setWrapText(true);
    instructionArea.setPrefHeight(400);
    instructionArea.setPrefWidth(400);

    Label instructionsLabel = new Label("Instructions:");
    instructionsLabel.setFont(Font.font(ARIAL_FONT, FontWeight.NORMAL, 18));
    VBox instructionsSection = new VBox(5, instructionsLabel, instructionArea);

    instructionsSection.setPadding(new Insets(0, 10, 10, 10));
    return instructionsSection;
  }



  /**
   * Creates an HBox for displaying the rating of the recipe.
   *
   * @return a HBox for displaying the rating of the recipe
   */
  private HBox createRatingBox() {
    Label ratingPlaceholder = new Label("★★★★★");
    return new HBox(ratingPlaceholder);
  }

}
