package idatg1005.gruppe10.matvettapp.frontend.guiclasses;

import idatg1005.gruppe10.matvettapp.backend.food.Recipe;
import idatg1005.gruppe10.matvettapp.backend.managerclasses.RecipeManager;
import idatg1005.gruppe10.matvettapp.backend.storageclasses.ShoppingList;
import idatg1005.gruppe10.matvettapp.frontend.controllers.RecipeController;
import idatg1005.gruppe10.matvettapp.frontend.controllers.ShoppingListController;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.geometry.Insets;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.geometry.Pos;
import javafx.stage.Stage;




/**
 * Represents the GUI for the recipe section of the application.
 * This class provides a visual interface for displaying recipes, categorized recipe options,
 * and a list of recently viewed recipes. It features a grid layout for recipes, a list for
 * recently viewed recipes, and a panel for recipe categories.
 */
public class RecipeGui extends BorderPane {

  private TilePane recipeGrid;  // Grid layout for displaying recipes
  private VBox categoryPanel;  // Panel for recipe categories
  private Stage currentDetailStage;
  private VBox rightPanel;
  private Label recentlyViewedLabel;
  private Label favoritesLabel;
  private TextField searchField;


  private final RecipeController controller;
  private final RecipeManager recipeManager;
  private final ShoppingList sharedShoppingList;
  private final ShoppingListController shoppingListController;



  private final Set<Recipe> favoriteRecipes = new HashSet<>();
  private ListView<Recipe> favoriteRecipesListView;
  private ListView<String> recentlyViewedList;  // List of recently viewed recipes


  private static final String ALL_RECIPES = "All Recipes";
  private static final String ARIAL_FONT = "Arial";






  /**
   * Constructor for RecipeGUI.
   * Initializes the GUI components and sets up the layout for recipe management.
   *
   * @param recipeManager The RecipeManager instance for managing recipes.
   * @param sharedShoppingList The ShoppingList used for ingredient management.
   * @param shoppingListController for controlling shopping list interactions.
   */
  public RecipeGui(RecipeManager recipeManager, ShoppingList sharedShoppingList,
      ShoppingListController shoppingListController) {
    this.recipeManager = recipeManager;
    this.sharedShoppingList = sharedShoppingList;
    this.shoppingListController = shoppingListController;
    this.controller = new RecipeController(this, recipeManager);

    // Initializing rightPanel before using it
    rightPanel = new VBox();

    createRecipeGrid(recipeManager.getAllRecipes());
    createCategoryPanel();
    setUpTopPanel();
    styleComponents();
    setupRightPanel();
    this.setRight(rightPanel);
    setLayout();
  }




  /**
   * Sets the layout of the RecipeGUI by adding the category panel, recipe grid, and recently viewed
   * list to the BorderPane.
   */
  private void setLayout() {
    // recipe Grid wrapped in a scrollable view:
    recipeGrid = createRecipeGrid(recipeManager.getAllRecipes());
    ScrollPane scrollableRecipeGrid = new ScrollPane(recipeGrid);
    scrollableRecipeGrid.setFitToWidth(true);
    scrollableRecipeGrid.setFitToHeight(true);
    scrollableRecipeGrid.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
    scrollableRecipeGrid.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);

    // binding the height of the recipeGrid to the height of the scene:
    recipeGrid.prefHeightProperty().bind(this.heightProperty());

    // category panel setup
    createCategoryPanel();   // 20% of the width ('0.2'):
    categoryPanel.prefWidthProperty().bind(this.widthProperty().multiply(0.2));

    setupRightPanel();  // favorites and recently Viewed panel setup

    // adding components to the BorderPane:
    this.setLeft(categoryPanel);
    this.setCenter(scrollableRecipeGrid);

    // CSS styles:
    this.getStylesheets().add(Objects.requireNonNull(
        getClass().getResource("/css/recipe-gui.css")).toExternalForm());

  }





  /**
   * Sets up the top panel of the RecipeGUI, which includes a search field and a button to perform
   * the search.
   */
  private void setUpTopPanel() {
    HBox searchPanel = new HBox();
    searchPanel.setPadding(new Insets(10, 10, 10, 10));
    searchPanel.setAlignment(Pos.CENTER);

    searchField = new TextField();
    searchField.setPromptText("Search recipes...");
    searchField.setMinWidth(200);

    Button searchButton = new Button();
    ImageView searchIcon = new ImageView(new Image(
        Objects.requireNonNull(getClass()
            .getResourceAsStream("/images/button-icons/search-icon.png"))));
    searchIcon.setFitHeight(20);
    searchIcon.setFitWidth(20);
    searchButton.setGraphic(searchIcon);
    searchButton.setOnAction(e -> performSearch());

    // go back button: (Instead loads all recipes, until history functionality is implemented)
    Button backButton = new Button("Go Back");
    ImageView backIcon = new ImageView(new Image(
        Objects.requireNonNull(getClass()
            .getResourceAsStream("/images/button-icons/refresh-icon.png"))));
    backIcon.setFitHeight(20);
    backIcon.setFitWidth(20);
    backButton.setGraphic(backIcon);
    backButton.setAlignment(Pos.CENTER_LEFT);
    backButton.setOnAction(e -> controller.updateRecipeViewByCategory(ALL_RECIPES));

    searchPanel.getChildren().addAll(backButton, searchField, searchButton);
    this.setTop(searchPanel);
  }




  /**
   * Performs a search based on the text entered the search field.
   */
  private void performSearch() {
    String searchText = searchField.getText();
    controller.performSearch(searchText);
  }





  /**
   * Initializes the tile pane used to display recipe cards. The method populates the grid with
   * placeholder recipe cards for demonstration purposes.
   */
  private TilePane createRecipeGrid(Collection<Recipe> recipes) {
    TilePane grid = new TilePane();
    grid.setPadding(new Insets(15));
    grid.setHgap(15);
    grid.setVgap(15);

    for (Recipe recipe : recipes) {
      grid.getChildren().add(createRecipeCard(recipe));
    }

    return grid;
  }



  /**
   * Creates a visual card for a recipe, including an image and a title.
   *
   * @param recipe The recipe to create a card for.
   * @return A VBox containing the recipe's image and title.
   */
  private VBox createRecipeCard(Recipe recipe) {
    VBox recipeCard = new VBox(10);
    recipeCard.setAlignment(Pos.CENTER);
    recipeCard.setPadding(new Insets(10));
    recipeCard.getStyleClass().add("recipe-card");

    ImageView recipeImage = new ImageView(new Image(
        Objects.requireNonNull(getClass().getResourceAsStream(recipe.getImagePath()))));
    recipeImage.setFitWidth(150);
    recipeImage.setFitHeight(150);
    recipeImage.setPreserveRatio(true);

    Label recipeLabel = new Label(recipe.getRecipeName());
    recipeLabel.setFont(Font.font(ARIAL_FONT, FontWeight.BOLD, 12));

    recipeCard.getChildren().addAll(recipeImage, recipeLabel);
    recipeCard.setOnMouseClicked(e -> {
      controller.addRecipeToRecentlyViewed(recipe);
      openRecipeDetailView(recipe);
    });
    return recipeCard;
  }



  /**
   * Adds a recipe name to the recently viewed list if it is not already present.
   *
   * @param recipeName The recipe name to add.
   */
  public void addToRecentlyViewedList(String recipeName) {
    Platform.runLater(() -> {
      if (!recentlyViewedList.getItems().contains(recipeName)) {
        recentlyViewedList.getItems().addFirst(recipeName); // Add to the top
        // Make sure this is saved
        Recipe recipe = recipeManager.getRecipeByName(recipeName);
        if (recipe != null) {
          controller.addRecipeToRecentlyViewed(recipe);
        }
      }
    });
  }





  /**
   * Initializes the category panel for recipe categories. This method sets up a vertical box
   * containing buttons for different recipe categories.
   */
  private void createCategoryPanel() {
    categoryPanel = new VBox(10);
    categoryPanel.setPadding(new Insets(20, 0, 0, 0));


    allRecipesButton(); // button for all recipes in the category panel (top of the list)

    Map<String, String> categoryIcons = new HashMap<>();
    categoryIcons.put("Appetizer", "appetizer-icon.png");
    categoryIcons.put("Main Course", "main-course-icon.png");
    categoryIcons.put("Dessert", "desert-icon.png");

    categoryIcons.keySet().forEach(category -> {
      Button categoryButton = new Button(category);
      categoryButton.setFont(Font.font(ARIAL_FONT, FontWeight.BOLD, 14));
      categoryButton.setMaxWidth(215);
      categoryButton.setAlignment(Pos.CENTER);

      // the image path from the map
      String imagePath = "/images/button-icons/category-icons/" + categoryIcons.get(category);
      // getting the resource as a stream
      InputStream resourceStream = getClass().getResourceAsStream(imagePath);
      if (resourceStream == null) {
        throw new IllegalArgumentException("Resource not found: " + imagePath);
      }

      // ImageView for the button's graphic:
      ImageView imageView = new ImageView(new Image(resourceStream));
      imageView.setFitHeight(25);
      imageView.setFitWidth(25);

      categoryButton.setGraphic(imageView);
      categoryButton.setContentDisplay(ContentDisplay.TOP);
      categoryButton.setOnAction(e -> controller.updateRecipeViewByCategory(category));
      categoryPanel.getChildren().add(categoryButton);
    });

    ComboBox<String> categoryComboBox = new ComboBox<>();
    categoryComboBox.getStyleClass().add("custom-combobox");
    categoryComboBox.getItems().addAll(
        "Side Dish", "Pasta", "Meat", "Seafood",
        "Vegetarian", "Vegan", "Gluten-Free", "Salad", "Soup",
        "Marinade", "Baking", "Drinks",
        "International/Cuisines", "Quick & Easy", "Budget Meals"
    );
    categoryComboBox.setValue("Other Recipes");
    categoryComboBox.setOnAction(e ->
        controller.updateRecipeViewByCategory(categoryComboBox.getValue()));

    categoryPanel.getChildren().add(categoryComboBox);
    categoryPanel.setAlignment(Pos.CENTER);

    ScrollPane categoryScrollPane = new ScrollPane(categoryPanel);
    categoryScrollPane.setFitToWidth(true);
    categoryScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);

    this.setLeft(categoryScrollPane);
  }




  /**
   * Adds a button to the category panel for displaying all recipes.
   */
  private void allRecipesButton() {
    Button allRecipesButton = new Button(ALL_RECIPES);
    allRecipesButton.setMaxWidth(215);
    allRecipesButton.setAlignment(Pos.CENTER);
    ImageView icon = new ImageView(new Image(
        Objects.requireNonNull(getClass().getResourceAsStream(
            "/images/button-icons/category-icons/all-recipes-icon.png"))));
    allRecipesButton.setGraphic(icon);
    allRecipesButton.setContentDisplay(ContentDisplay.TOP);
    allRecipesButton.setFont(Font.font(ARIAL_FONT, FontWeight.BOLD, 14));
    icon.setFitHeight(25);
    icon.setFitWidth(25);


    allRecipesButton.setOnAction(e -> controller.updateRecipeViewByCategory(ALL_RECIPES));
    categoryPanel.getChildren().add(allRecipesButton);
  }





  /**
   * Opens a detailed view of the specified recipe.
   *
   * @param recipe The recipe to display in detail.
   */
  public void openRecipeDetailView(Recipe recipe) {
    if (currentDetailStage != null) {
      currentDetailStage.close();
    }

    RecipeDetailView recipeDetailView =
        new RecipeDetailView(recipe, sharedShoppingList, shoppingListController, this);
    Scene scene = new Scene(recipeDetailView, 700, 600);
    Stage stage = new Stage();
    stage.setScene(scene);
    stage.setResizable(false);
    stage.setOnCloseRequest(e -> currentDetailStage = null); // reset the current detail stage
    stage.show();
    currentDetailStage = stage; // saving the current stage

    controller.addRecipeToRecentlyViewed(recipe);
  }




  /**
   * Loads and displays the specified recipes in the recipe grid.
   *
   * @param recipes The recipes to display.
   */
  public void loadAndDisplayRecipes(Collection<Recipe> recipes) {
    recipeGrid.getChildren().clear(); // clearing existing recipe cards
    if (recipes.isEmpty()) {
      // When no recipes are found
      Label noRecipesLabel = new Label("No recipes found. Try another search term.");
      noRecipesLabel.getStyleClass().add("no-recipes-label");
      recipeGrid.getChildren().add(noRecipesLabel);
    } else {
      for (Recipe recipe : recipes) {
        VBox recipeCard = createRecipeCard(recipe);
        recipeGrid.getChildren().add(recipeCard);
      }
    }
  }





  /**
   * Adds a recipe to the user's list of favorites and saves the updated list.
   *
   * @param recipe The recipe to be added to favorites.
   */
  public void addRecipeToFavorites(Recipe recipe) {
    if (!favoriteRecipes.contains(recipe)) {
      favoriteRecipes.add(recipe);
      controller.addRecipeToFavorites(recipe);
      refreshFavoriteRecipesDisplay();
    }
  }




  // TODO: Denne metoden eksisteres allerede i RecipeManager
  /**
   * Removes a recipe from the user's list of favorites and saves the updated list.
   *
   * @param recipe The recipe to be removed from favorites.
   */
  public void removeRecipeFromFavorites(Recipe recipe) {
    if (favoriteRecipes.remove(recipe)) {
      recipeManager.removeRecipeFromFavorites(recipe);
      recipeManager.saveFavorites(); // this method will save the updated favorites
      refreshFavoriteRecipesDisplay(); // this method will update the GUI
    }
  }



  /**
   * Refreshes the display of favorite recipes in the GUI.
   */
  public void refreshFavoriteRecipesDisplay() {
    favoriteRecipesListView.getItems().clear();
    favoriteRecipesListView.getItems().addAll(recipeManager.getFavoriteRecipes());
  }



  /**
   * Refreshes the display of recently viewed recipes in the GUI.
   */
  public void refreshRecentlyViewedListDisplay() {
    recentlyViewedList.getItems().clear();
    recentlyViewedList.getItems().addAll(recipeManager.getRecentlyViewed());
  }



  /**
   * Refreshes the display of favorite recipes and recently viewed recipes in the GUI.
   */
  public void refreshDataDisplay() {
    refreshFavoriteRecipesDisplay();
    refreshRecentlyViewedListDisplay();
  }




  /**
   * Applies CSS styles to the GUI components.
   * This method is called after all components have been initialized and added to the layout.
   */
  private void styleComponents() {
    // the global style sheet
    this.getStylesheets().add(Objects.requireNonNull(
        getClass().getResource("/css/recipe-gui.css")).toExternalForm());

    // styles for category buttons and combobox
    categoryPanel.getChildren().stream()
        .filter(Button.class::isInstance)
        .map(Button.class::cast)
        .forEach(button -> button.getStyleClass().add("category-button"));

    // styles for search panel elements
    searchField.getStyleClass().add("search-field");
    Platform.runLater(() -> {

      // now the scene is rendered, and lookup should find the node
      Node searchButton = searchField.lookup(".button");
      if (searchButton != null) {
        searchButton.getStyleClass().add("search-button");
      }
    });
  }




  /**
   * Sets up the right panel of the GUI, which includes a list of recently viewed recipes and a list
   * of favorite recipes.
   */
  private void setupRightPanel() {
    if (rightPanel == null) {
      rightPanel = new VBox();
    }

    GridPane listsGrid = new GridPane();
    listsGrid.setAlignment(Pos.CENTER);
    listsGrid.setVgap(10);

    setupRecentlyViewedPanel();
    setupFavoritesPanel();

    // setting GridPane constraints for alignment
    GridPane.setHalignment(recentlyViewedLabel, HPos.CENTER);
    GridPane.setHalignment(favoritesLabel, HPos.CENTER);

    listsGrid.add(recentlyViewedLabel, 0, 0);
    listsGrid.add(recentlyViewedList, 0, 1);
    listsGrid.add(favoritesLabel, 0, 2);
    listsGrid.add(favoriteRecipesListView, 0, 3);

    // ensuring the list views expand to fill the available space
    ColumnConstraints columnConstraints = new ColumnConstraints();
    columnConstraints.setHgrow(Priority.ALWAYS);
    listsGrid.getColumnConstraints().add(columnConstraints);

    // grid pane wrapped in a VBox container
    VBox container = new VBox(listsGrid);
    container.setPadding(new Insets(10));
    container.setAlignment(Pos.TOP_CENTER);

    recentlyViewedLabel.getStyleClass().add("heading-label");
    favoritesLabel.getStyleClass().add("heading-label");
    this.setRight(container);
  }





  /**
   * Sets up the panel for displaying recently viewed recipes.
   */
  private void setupRecentlyViewedPanel() {
    recentlyViewedList = new ListView<>();
    ImageView recentlyViewedIcon = new ImageView(new Image(
        Objects.requireNonNull(getClass().getResourceAsStream(
            "/images/button-icons/recently-viewed-icon.png"))));
    recentlyViewedIcon.setFitHeight(30);
    recentlyViewedIcon.setFitWidth(35);

    recentlyViewedLabel = new Label("Recently Viewed", recentlyViewedIcon);
    recentlyViewedLabel.setContentDisplay(ContentDisplay.LEFT);
    recentlyViewedLabel.setGraphicTextGap(10);
    recentlyViewedLabel.setFont(Font.font(ARIAL_FONT, FontWeight.BOLD, 14));

    recentlyViewedList.setCellFactory(lv -> new ListCell<>() {
      @Override
      protected void updateItem(String recipeName, boolean empty) {
        super.updateItem(recipeName, empty);
        if (empty || recipeName == null) {
          setText(null);
        } else {
          setText(recipeName);
          setOnMouseClicked(event -> {
            Recipe recipe = recipeManager.getRecipeByName(recipeName);
            if (recipe != null) {
              openRecipeDetailView(recipe);
            }
          });
        }
      }
    });

    rightPanel.getChildren().add(recentlyViewedLabel);
    rightPanel.getChildren().add(recentlyViewedList);
  }




  /**
   * Sets up the panel for displaying favorite recipes.
   */
  private void setupFavoritesPanel() {
    favoriteRecipesListView = new ListView<>();
    ImageView favoritesIcon = new ImageView(new Image(
        Objects.requireNonNull(getClass()
            .getResourceAsStream("/images/button-icons/heart-icon.png"))));
    favoritesIcon.setFitHeight(16);
    favoritesIcon.setFitWidth(16);

    // Label for the favorite list
    favoritesLabel = new Label("My Favorites", favoritesIcon);
    favoritesLabel.setContentDisplay(ContentDisplay.LEFT);
    favoritesLabel.setGraphicTextGap(10);
    favoritesLabel.setFont(Font.font(ARIAL_FONT, FontWeight.BOLD, 14));


    // Custom cell factory for the favorite recipe list view:
    favoriteRecipesListView.setCellFactory(lv -> new ListCell<>() {
      private final Button removeButton = new Button();
      private final ImageView removeIcon = new ImageView(new Image(Objects.requireNonNull(
          getClass().getResourceAsStream("/images/button-icons/edit-icon.png"))));

      {
        setContentDisplay(ContentDisplay.RIGHT);
        removeIcon.setFitWidth(15);
        removeIcon.setFitHeight(15);
        removeButton.setGraphic(removeIcon);
        removeButton.getStyleClass().add("remove-button");
        removeButton.setOnAction(e -> {
          Recipe item = getItem();
          if (item != null) {
            confirmAndRemoveRecipe(item);
          }
          e.consume(); // preventing the cell from being clicked, only button should be clickable
          recipeManager.saveFavorites(); // saving the updated favorites

        });
        setOnMouseEntered(e -> setGraphic(removeButton)); // button hover effect
        setOnMouseExited(e -> setGraphic(null)); // hiding the button when not hovered

        // here we open the recipe detail view when the cell is clicked
        setOnMouseClicked(e -> {
          if (getItem() != null && !e.isConsumed()) {
            openRecipeDetailView(getItem());
          }
        });
      }

      // custom updateItem method to set the text and graphic of the cell
      @Override
      protected void updateItem(Recipe item, boolean empty) {
        super.updateItem(item, empty);
        if (empty || item == null) {
          setText(null);
          setGraphic(null);
        } else {
          setText(item.getRecipeName());
        }
      }
    });

    rightPanel.getChildren().add(favoritesLabel);  // adding the label to the right panel
    rightPanel.getChildren().add(favoriteRecipesListView); // also the list view
  }




  /**
   * Prompts the user to confirm the removal of a recipe from favorites.
   *
   * @param recipe The recipe to remove from favorites.
   */
  public void confirmAndRemoveRecipe(Recipe recipe) {
    Alert confirmationAlert =
        new Alert(Alert.AlertType.CONFIRMATION,
            "Are you sure you want to remove this recipe from favorites?",
            ButtonType.YES, ButtonType.NO);

    // handling the user's response to the confirmation dialog
    confirmationAlert.showAndWait().ifPresent(response -> {
      if (response == ButtonType.YES) {
        removeRecipeFromFavorites(recipe); // remove the recipe if the user confirms
      }
    });
  }




}
