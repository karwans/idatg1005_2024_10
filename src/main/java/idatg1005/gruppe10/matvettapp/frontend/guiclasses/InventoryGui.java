package idatg1005.gruppe10.matvettapp.frontend.guiclasses;

import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import idatg1005.gruppe10.matvettapp.backend.storageclasses.Inventory;
import idatg1005.gruppe10.matvettapp.frontend.controllers.InventoryController;
import idatg1005.gruppe10.matvettapp.frontend.graphicalutilities.CategoryAccordion;
import idatg1005.gruppe10.matvettapp.frontend.graphicalutilities.CommonLayouts;
import idatg1005.gruppe10.matvettapp.frontend.graphicalutilities.EditableTableCell;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.util.Pair;
import javafx.util.converter.DefaultStringConverter;
import idatg1005.gruppe10.matvettapp.commonutilities.errorhandler.ErrorHandler;
import idatg1005.gruppe10.matvettapp.commonutilities.filehandler.GroceryFileHandler;
import java.util.Objects;
import java.util.Optional;



/**
 * GUI class for displaying and interacting with the inventory of the user.
 * This class handles the layout and visual interaction components for managing inventory items.
 */
public class InventoryGui extends BorderPane {

  private static final Logger LOGGER =
      Logger.getLogger("idatg1005.gruppe10.matvettapp.guiclasses.frontend.InventoryGui");


  private final Inventory inventory;
  private final InventoryController inventoryController;
  private TableView<Grocery> inventoryTableView;
  private CategoryAccordion categoryAccordion;
  private TextField searchField;
  private Map<String, List<Grocery>> categorizedGroceries; // Categorized grocery


  public static final String DEFAULT_CATEGORY = "Uncategorized";
  private static final String SEARCH_ICON_PATH = "/images/button-icons/search-icon.png";
  private static final String INVENTORY_TABLE_NOT_SET = "Inventory table view is not set.";




  /**
   * Constructs the Inventory GUI using the given inventory and controller.
   *
   * @param inventory The inventory data model.
   * @param inventoryController The controller for inventory operations.
   */
  public InventoryGui(Inventory inventory, InventoryController inventoryController) {
    this.inventory = inventory;
    this.inventoryController = inventoryController;
    initializeLayout();
    this.inventoryController.setInventoryGui(this);
    updateTableView();
    updateAccordion();
  }




  /**
   * Initializes the layout of the inventory GUI, setting up styles and main layout.
   */
  private void initializeLayout() {
    setUpStyles();
    setCenter(createMainLayout());
    updateAccordion();
  }




  /**
   * Sets up the CSS styles used in the GUI.
   */
  private void setUpStyles() {
    this.getStylesheets().add(loadStyleSheet("/css/shopping-list-gui.css"));
    this.getStylesheets().add(loadStyleSheet("/css/accordian-style.css"));
  }




  /**
   * Loads a CSS stylesheet from a given path.
   *
   * @param path the path to the CSS file.
   * @return the external form URL of the CSS file.
   */
  private String loadStyleSheet(String path) {
    return Objects.requireNonNull(getClass().getResource(path)).toExternalForm();
  }




  /**
   * Creates the main layout containing the left, middle, and right sections of the GUI.
   *
   * @return the HBox containing all three sections.
   */
  private HBox createMainLayout() {
    VBox leftSideLayout = createLeftSideLayout();
    ScrollPane leftScrollPane = CommonLayouts.wrapInScrollPane(leftSideLayout, true, true);
    VBox middleLayout = createMiddleLayout();
    VBox rightSideLayout = createRightSideLayout();
    ScrollPane rightScrollPane = CommonLayouts.wrapInScrollPane(rightSideLayout, true, true);

    HBox mainLayout = CommonLayouts.createHboxWithSpacing(20);
    mainLayout.setAlignment(Pos.CENTER_LEFT);
    mainLayout.setPadding(new Insets(10));
    mainLayout.getChildren().addAll(leftScrollPane, middleLayout, rightScrollPane);
    HBox.setHgrow(rightScrollPane, Priority.ALWAYS);

    return mainLayout;
  }





  /**
   * Creates the left side layout containing the search and button layouts.
   *
   * @return the VBox containing the search and button layouts.
   */
  private VBox createLeftSideLayout() {
    HBox searchLayout = createSearchField();
    VBox buttonLayout = createButtonLayout();
    VBox leftSideLayout = CommonLayouts.createVboxWithSpacing(10);
    leftSideLayout.getChildren().addAll(searchLayout, buttonLayout);
    return leftSideLayout;
  }




  /**
   * Creates the middle layout containing the inventory table view.
   *
   * @return the VBox containing the table header and the table view.
   */
  private VBox createMiddleLayout() {
    Label tableHeader = new Label("My Inventory");
    tableHeader.setFont(new Font("Arial", 24));

    inventoryTableView = createConfiguredTableView();
    inventoryTableView
        .prefHeightProperty()
        .bind(this.heightProperty()
            .subtract(tableHeader.heightProperty()).subtract(100));

    VBox middleLayout = CommonLayouts.createVboxWithSpacing(5);
    middleLayout.getChildren().addAll(tableHeader, inventoryTableView);
    return middleLayout;
  }




  /**
   * Creates the right side layout containing the category accordion.
   *
   * @return the VBox containing the category accordion.
   */
  private VBox createRightSideLayout() {
    this.categoryAccordion = new CategoryAccordion(inventoryController,
        inventoryController.getAddItemWithQuantityAction());

    VBox rightSideLayout = new VBox(this.categoryAccordion);
    rightSideLayout.setPadding(new Insets(10));
    rightSideLayout.setSpacing(10);

    return rightSideLayout;
  }




  /**
   * Creates and configures a TableView for displaying Grocery items.
   * This method encapsulates the entire setup of the TableView including editable cells,
   * column configuration, and placeholder setup.
   *
   * @return a fully configured TableView<Grocery>
   */
  private TableView<Grocery> createConfiguredTableView() {
    TableView<Grocery> tableView = new TableView<>();
    tableView.setEditable(true); // allows users to edit the contents directly in the table
    configureColumns(tableView);
    setTableViewPlaceholder(tableView); // placeholder for an empty table
    tableView.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY); // columns resized freely
    return tableView;
  }

  /**
   * Configures the columns for the TableView.
   * This includes setting up the data properties, cell factories, and column widths.
   *
   * @param tableView the TableView to which columns will be added
   */
  private void configureColumns(TableView<Grocery> tableView) {
    TableColumn<Grocery, String> nameColumn = new TableColumn<>("Item");

    // binding the column to the grocery item names:
    nameColumn.setCellValueFactory(new PropertyValueFactory<>("groceryItemName"));

    // setting the cell factory to allow editing of the cell:
    nameColumn.setCellFactory(column -> new EditableTableCell<>(new DefaultStringConverter()));

    // setting the column width to be 50% of the table width:
    nameColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.5));

    // the same configuration for the quantity column:
    TableColumn<Grocery, String> quantityColumn = new TableColumn<>("Quantity");
    quantityColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));
    quantityColumn.setCellFactory(column -> new EditableTableCell<>(new DefaultStringConverter()));
    quantityColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.5));

    // adding both columns to the TableView
    tableView.getColumns().addAll(Arrays.asList(nameColumn, quantityColumn));
  }


  /**
   * Sets the placeholder for the TableView.
   * This is displayed when the TableView has no items to show.
   *
   * @param tableView the TableView whose placeholder will be set
   */
  private void setTableViewPlaceholder(TableView<Grocery> tableView) {
    if (inventory.getInventoryItems().isEmpty()) {
      tableView.setPlaceholder(new Label("No items in inventory."));//when the inventory is empty
    } else {
      tableView.getItems().setAll(inventory.getInventoryItems());
    }
  }







  /**
   * Creates the button layout for the left side of the GUI.
   *
   * @return the VBox containing add, remove, and save buttons.
   */
  private VBox createButtonLayout() {
    Button addItemButton = new Button("Add Item");
    addItemButton.setOnAction(e -> handleAddItem());

    Button removeItemButton = new Button("Remove Selected Item");
    removeItemButton.setOnAction(e -> handleRemoveSelectedItem());

    Button saveChangesButton = new Button("Save Changes");
    saveChangesButton.setOnAction(e -> inventoryController.saveChanges());  // save changes action

    VBox buttonLayout = CommonLayouts.createVboxWithSpacing(10);
    buttonLayout.getChildren().addAll(addItemButton, removeItemButton, saveChangesButton);
    return buttonLayout;
  }




  /**
   * Creates the search field and search button for inventory search functionality.
   *
   * @return the HBox containing the search field and button.
   */
  private HBox createSearchField() {
    searchField = new TextField();
    searchField.setPromptText("Search in inventory...");

    ImageView searchIcon = new ImageView(new Image(Objects.requireNonNull(
        getClass().getResourceAsStream(SEARCH_ICON_PATH))));
    searchIcon.setFitHeight(23);
    searchIcon.setFitWidth(23);

    Button searchButton = new Button("", searchIcon); // button with search icon
    searchButton.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);  // displaying only the icon
    searchButton.setOnAction(e -> performSearch());
    // listener to perform search when the user presses enter field:
    searchField.textProperty().addListener((observable, oldValue, newValue) -> performSearch());

    HBox searchLayout = CommonLayouts.createHboxWithSpacing(10);
    searchLayout.getChildren().addAll(searchField, searchButton);
    return searchLayout;
  }




  /**
   * Performs a search in the inventory based on the text in the search field.
   */
  private void performSearch() {
    String searchQuery = searchField.getText().toLowerCase();

    if (!searchQuery.trim().isEmpty()) {
      List<Grocery> filteredList = new ArrayList<>();  // list to store filtered items

      for (Grocery grocery : inventory.getInventoryItems()) {
        if (grocery.getGroceryItemName().toLowerCase().contains(searchQuery)) {
          filteredList.add(grocery);
        }
      }
      inventoryTableView.getItems().setAll(filteredList);

    } else {
      updateTableView();
    }

  }




  /**
   * Handles the removal of a selected item from the inventory via the GUI.
   * Removes the selected item from the GUI table view and from the inventory model.
   */

  private void handleRemoveSelectedItem() {
    Grocery selected = inventoryTableView.getSelectionModel().getSelectedItem();
    if (selected != null) {
      // Request to controller to remove the item from the inventory:
      inventoryController.removeItem(selected.getGroceryItemName());
      inventoryTableView.getItems().remove(selected); // removes item from GUI view
      updateTableView();
      updateAccordion();
    } else {
      LOGGER.warning("No item selected to remove."); // warning if no item is selected
      ErrorHandler.handleException(new IllegalArgumentException("No item selected to remove."),
          "No item selected to remove");
    }

  }




  /**
   * Updates the table view in the GUI to reflect the current inventory items.
   * This method refreshes the display whenever items are added, removed,
   * or the inventory is otherwise modified.
   */
//  public void updateTableView() {
//    if (inventoryTableView != null) {
//      Platform.runLater(() ->
//          inventoryTableView.getItems().setAll(inventoryController.getInventoryItems()));
//    } else {
//      LOGGER.warning(INVENTORY_TABLE_NOT_SET);
//      ErrorHandler.handleException(new IllegalStateException(INVENTORY_TABLE_NOT_SET),
//          "Inventory table view is not set");
//      throw new IllegalStateException(INVENTORY_TABLE_NOT_SET);
//    }
//  }
  public void updateTableView() {
    Platform.runLater(() -> {
      List<Grocery> items = inventory.getInventoryItems();  // getting the latest items from inventory
      if (inventoryTableView != null) {
        inventoryTableView.getItems().setAll(items);  // updating the table view
      }
    });
  }







  /**
   * Updates the category accordion in the GUI to reflect the current inventory categories.
   * This method refreshes the accordion display whenever categories are added, removed,
   * or the inventory is otherwise modified.
   */
  public void updateAccordion() {
    if (categoryAccordion != null) {
      categoryAccordion.populateCategories();
    }
  }





  /**
   * Retrieves the TableView used in the inventory GUI.
   * For testing purposes.
   *
   * @return the TableView used in the inventory GUI.
   */
  public TableView<Grocery> getTableView() {
    return this.inventoryTableView;
  }






  /**
   * Initializes and shows a dialog for adding a new item to the inventory.
   */
  private void handleAddItem() {
    Dialog<Pair<String, String>> dialog = prepareAddItemDialog();
    configureAddItemDialogEvents(dialog);
    processAddItemDialogResult(dialog);
  }




  /**
   * Prepares the dialog used for adding new items, including ComboBoxes for items and units.
   *
   * @return A configured dialog ready to be displayed.
   */
  private Dialog<Pair<String, String>> prepareAddItemDialog() {
    loadCategorizedGroceriesIfNeeded();

    Dialog<Pair<String, String>> dialog = createItemAdditionDialog();
    ComboBox<Grocery> groceryComboBox = createGroceryComboBox();

    GridPane grid = new GridPane();
    grid.add(new Label("Item:"), 0, 0);
    grid.add(groceryComboBox, 1, 0);
    dialog.getDialogPane().setContent(grid);

    ButtonType addButton = new ButtonType("Add", ButtonBar.ButtonData.OK_DONE);
    dialog.getDialogPane().getButtonTypes().addAll(addButton, ButtonType.CANCEL);
    Node addButtonNode = dialog.getDialogPane().lookupButton(addButton);
    addButtonNode.setDisable(true);

    groceryComboBox.valueProperty().addListener((observable, oldValue, newVal) ->
        addButtonNode.setDisable(newVal == null));

    return dialog;
  }





  /**
   * Configures event handling for the add item dialog.
   *
   * @param dialog The dialog to which event handling will be added.
   */
  private void configureAddItemDialogEvents(Dialog<Pair<String, String>> dialog) {
    ComboBox<Grocery> groceryComboBox = getGroceryComboBox(dialog);

    dialog.setResultConverter(dialogButton -> {
      if (dialogButton.getButtonData() == ButtonBar.ButtonData.OK_DONE) {
        Grocery selectedGrocery = groceryComboBox.getValue();
        return selectedGrocery != null
            ? new Pair<>(selectedGrocery.getGroceryItemName(), null) // Temporarily null for unit
            : null;
      }
      return null;
    });
  }




  /**
   * Retrieves the ComboBox<Grocery> from the dialog.
   *
   * @param dialog The dialog from which to retrieve the ComboBox.
   * @return The ComboBox<Grocery> from the dialog.
   */
  private static ComboBox<Grocery> getGroceryComboBox(Dialog<Pair<String, String>> dialog) {
    Node node = ((GridPane) dialog.getDialogPane().getContent()).getChildren().get(1);

    if (node instanceof ComboBox<?> comboBox) {
      // Check if the ComboBox contains items of type Grocery
      if (!comboBox.getItems().isEmpty() && comboBox.getItems().getFirst() instanceof Grocery) {
        // We are sure of the type, so we can suppress the unchecked warning
        @SuppressWarnings("unchecked")
        ComboBox<Grocery> groceryComboBox = (ComboBox<Grocery>) comboBox;
        return groceryComboBox;
      }
    }
    LOGGER.warning("ComboBox<Grocery> not found in dialog.");
    throw new IllegalStateException("ComboBox<Grocery> not found in dialog.");
  }




  /**
   * Processes the result from the add item dialog, asking for quantity and adding the item.
   *
   * @param dialog The dialog whose result will be processed.
   */
  private void processAddItemDialogResult(Dialog<Pair<String, String>> dialog) {
    Optional<Pair<String, String>> result = dialog.showAndWait();
    result.ifPresent(pair -> {
      TextInputDialog quantityDialog = new TextInputDialog("1");
      quantityDialog.setTitle("Quantity Input");
      quantityDialog.setHeaderText("Enter the quantity for " + pair.getKey());
      quantityDialog.setContentText("Quantity:");
      Optional<String> quantityResult = quantityDialog.showAndWait();

      quantityResult.ifPresent(quantity -> {
        List<String> possibleUnits = Arrays.asList("kg", "g", "L", "pieces");
        ChoiceDialog<String> unitDialog = new ChoiceDialog<>("kg", possibleUnits);
        unitDialog.setTitle("Unit Input");
        unitDialog.setHeaderText(null);
        unitDialog.setContentText("Unit:");
        Optional<String> unitResult = unitDialog.showAndWait();

        unitResult.ifPresent(unit -> {
          String quantityWithUnit = quantity + " " + unit;
          Grocery newItem = new Grocery(pair.getKey(), quantityWithUnit);

          inventoryController.addNewItem(newItem, DEFAULT_CATEGORY);
          inventoryTableView.getItems().add(newItem);
          updateTableView();
          updateAccordion();
        });
      });
    });
  }


  /**
   * Loads categorized groceries from file if not already loaded.
   * This method is used to ensure that the categorized groceries are loaded
   */
  private void loadCategorizedGroceriesIfNeeded() {
    if (categorizedGroceries == null || categorizedGroceries.isEmpty()) {
      GroceryFileHandler fileHandler = new GroceryFileHandler();
      categorizedGroceries = fileHandler
          .loadCategoriesWithItems("src/main/resources/grocery-items.txt");
    }
  }



  /**
   * Creates a dialog for adding items to the inventory.
   *
   * @return a dialog for adding items to the inventory.
   */
  private Dialog<Pair<String, String>> createItemAdditionDialog() {
    Dialog<Pair<String, String>> dialog = new Dialog<>();
    dialog.setTitle("Add New Item");
    dialog.setHeaderText("Select an item and its quantity unit.");
    return dialog;
  }



  /**
   * Creates a ComboBox for selecting grocery items.
   *
   *
   * @return a ComboBox for selecting grocery items.
   */
  private ComboBox<Grocery> createGroceryComboBox() {
    ComboBox<Grocery> comboBox = new ComboBox<>();
    comboBox.setPromptText("Select an item");
    List<Grocery> allGroceries = categorizedGroceries.values().stream()
        .flatMap(List::stream)
        .collect(Collectors.toList());
    comboBox.setItems(FXCollections.observableArrayList(allGroceries));
    return comboBox;
  }

}
