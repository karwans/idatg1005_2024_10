package idatg1005.gruppe10.matvettapp.frontend.guiclasses;

import idatg1005.gruppe10.matvettapp.backend.storageclasses.ShoppingList;
import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import idatg1005.gruppe10.matvettapp.frontend.graphicalutilities.CategoryAccordion;
import idatg1005.gruppe10.matvettapp.frontend.graphicalutilities.CommonLayouts;
import idatg1005.gruppe10.matvettapp.frontend.graphicalutilities.EditableTableCell;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.geometry.Insets;
import idatg1005.gruppe10.matvettapp.frontend.controllers.ShoppingListController;
import javafx.scene.text.Font;
import javafx.util.converter.DefaultStringConverter;




/**
 * GUI representation of the shopping list.
 * This class is responsible for displaying the shopping list
 * and providing interactive elements to manage it.
 */
public class ShoppingListGui extends BorderPane {

  private final ShoppingList shoppingList;
  private final ShoppingListController shoppingListController;


  private ComboBox<String> savedListsComboBox;
  private TableView<Grocery> groceryTableView;
  private TextField searchField;


  /**
   * Constructor for ShoppingListGUI.
   * Initializes the GUI with a given shopping list and its controller.
   *
   * @param shoppingList The shopping list to be managed.
   * @param shoppingListController The controller for shopping list operations.
   */
  public ShoppingListGui(ShoppingList shoppingList, ShoppingListController shoppingListController) {
    this.shoppingList = shoppingList;
    this.shoppingListController = shoppingListController;
    this.shoppingListController.setShoppingListGUI(this);
    initializeLayout();
    setStyleSheets();
  }




  /**
   * Updates the UI components to reflect the current state of the shopping list.
   */
  public void updateShoppingListsUI() {
    // Clear ComboBox and repopulate
    savedListsComboBox.getItems().clear();
    List<String> savedLists = shoppingListController.getSavedShoppingLists();
    savedListsComboBox.getItems().addAll(savedLists);

    // Reset the groceryTableView to reflect any changes
    updateTableView();

    // Clear any selection in the ListView or other components
    groceryTableView.getSelectionModel().clearSelection();
  }

  /**
   * Initializes and arranges components in the layout.
   */
  private void initializeLayout() {
    // Initialize the grocery table view
    groceryTableView = createGroceryTableView();
    groceryTableView.setItems(FXCollections.observableArrayList(shoppingList.getGroceryItems()));

    // Create search layout and place it at the top
    HBox searchLayout = createSearchField();

    // Create a label as a title for the table
    Label tableHeader = new Label("My Shopping List");
    tableHeader.setFont(new Font("Arial", 24));

    // Place searchLayout and tableHeader at the top of the table
    VBox middleLayout = CommonLayouts.createVboxWithSpacing(5);
    middleLayout.getChildren().addAll(searchLayout, tableHeader, groceryTableView);
    middleLayout.setAlignment(Pos.TOP_CENTER);

    // Category label and accordion for the right side layout
    Label categoryLabel = new Label("Categories");
    categoryLabel.setFont(new Font("Arial", 18));
    CategoryAccordion categoriesAccordion = new CategoryAccordion(
        shoppingListController, shoppingListController.getAddItemWithQuantityAction());
    categoriesAccordion.setMinWidth(250); // the minimum width as needed
    VBox rightSideLayout = CommonLayouts.createVboxWithSpacing(10);
    rightSideLayout.getChildren().addAll(categoryLabel, categoriesAccordion);
    rightSideLayout.setAlignment(Pos.TOP_CENTER);
    rightSideLayout.setPadding(new Insets(10));
    rightSideLayout.setPrefWidth(250); // the preferred width as needed

    // the left side layout which includes the saved lists ComboBox
    VBox leftSideLayout = createLeftSideLayout();

    // main layout composition
    HBox mainLayout = new HBox();
    mainLayout.setSpacing(20);
    mainLayout.setAlignment(Pos.CENTER_LEFT);
    mainLayout.setPadding(new Insets(10));
    mainLayout.getChildren().addAll(leftSideLayout, middleLayout, rightSideLayout);

    // growth policies for resizable elements
    HBox.setHgrow(middleLayout, Priority.ALWAYS);
    HBox.setHgrow(rightSideLayout, Priority.SOMETIMES);

    // main layout as the center of the BorderPane
    this.setCenter(mainLayout);


    setStyleSheets(); // styles
  }





  /**
   * Creates a ComboBox for selecting saved shopping lists.
   *
   * @return The ComboBox for saved shopping lists.
   */
  private ComboBox<String> createSavedListsComboBox() {
    ComboBox<String> comboBox = new ComboBox<>();
    comboBox.setPromptText("My Saved Shopping Lists");
    comboBox.getItems().addAll(shoppingListController.getSavedShoppingLists());
    comboBox.setOnAction(e -> showMyShoppingList(comboBox.getValue()));
    return comboBox;
  }




  /**
   * Handles the saving of the current shopping list with a specified name.
   */
  private void handleSaveList() {
    TextInputDialog dialog = new TextInputDialog("New List");
    dialog.setHeaderText("Enter a name for the shopping list:");

    // saving the list
    Optional<String> result = dialog.showAndWait();
    result.ifPresent(shoppingListController::saveCurrentShoppingList);

    clearTableView();   // clearing the table view after saving the list
    updateShoppingListsUI();  // updating the UI components
  }




  /**
   * Shows the shopping list with the specified name in a dialog.
   *
   * @param listName The name of the shopping list to show.
   */
  private void showMyShoppingList(String listName) {
    if (listName != null && !listName.isEmpty()) {
      // Loading the shopping list:
      shoppingListController.loadShoppingList(listName);
      ShoppingList listToShow = shoppingListController.getShoppingList();

      // Converting the shopping list to an editable string:
      TextArea listTextArea = new TextArea(convertShoppingListToString(listToShow));
      listTextArea.setEditable(true);

      // Dialog:
      Dialog<String> dialog = new Dialog<>();
      dialog.setTitle("Shopping List: " + listName);
      dialog.setHeaderText("Edit items in " + listName);

      // Save and Delete buttons for the dialog:
      ButtonType saveButtonType = new ButtonType("Save", ButtonBar.ButtonData.OK_DONE);
      ButtonType deleteButtonType = new ButtonType("Delete", ButtonBar.ButtonData.LEFT);
      dialog.getDialogPane().getButtonTypes()
          .addAll(saveButtonType, deleteButtonType, ButtonType.CANCEL);


      dialog.getDialogPane().setContent(listTextArea); // TextArea as the content of the dialog

      // Logic to handle saving changes or deleting the list:
      dialog.setResultConverter(dialogButton -> {
        if (dialogButton == saveButtonType) {
          return listTextArea.getText();
        } else if (dialogButton == deleteButtonType) {
          shoppingListController.deleteShoppingList(listName);
          updateShoppingListsUI(); // updates ComboBox and other GUI components if necessary
          return null; // return null as the dialog outcome if the list is deleted
        }
        return null; // return null if cancel or close is pressed
      });

      // Dialog and handling the result:
      Optional<String> result = dialog.showAndWait();
      result.ifPresent(updatedText -> {
        // converting the text back to a shopping list and saving an updated list
        ShoppingList updatedList = convertStringToShoppingList(updatedText);
        shoppingListController.saveMyShoppingList(listName, updatedList);
        updateShoppingListsUI(); // update UI components to reflect the new data
      });

      if (result.isEmpty() || dialog.getResult() == null) {
        updateShoppingListsUI();
      }
    }
  }





  /**
   * Converts a string representation of a shopping list to a ShoppingList object.
   *
   * @param listContent The string representation of the shopping list.
   * @return The ShoppingList object created from the string.
   */
  private ShoppingList convertStringToShoppingList(String listContent) {
    // Converting the string to a ShoppingList object:
    ShoppingList newShoppingList = new ShoppingList();
    String[] lines = listContent.split("\n"); // splitting the string by new lines

    for (String line : lines) {   // iterating over the lines
      String[] parts = line.split(" - Quantity: ");   // splitting the line by the separator

      if (parts.length == 2) {   // the line has two parts, the item name and the quantity
        String itemName = parts[0].trim();     // trimming the item name
        String quantity = parts[1].trim();     // trimming the quantity

        newShoppingList.addGroceryItem(new Grocery(itemName, quantity));
      }

    }

    return newShoppingList;

  }





  /**
   * Converts a ShoppingList object to a string representation.
   *
   * @param shoppingList The ShoppingList object to convert.
   * @return The string representation of the ShoppingList.
   */
  private String convertShoppingListToString(ShoppingList shoppingList) {
    StringBuilder sb = new StringBuilder();
    for (Grocery item : shoppingList.getGroceryItems()) {
      sb.append(item.getGroceryItemName())
          .append(" - Quantity: ")
          .append(item.getQuantity())
          .append("\n");
    }
    return sb.toString();
  }





  /**
   * Creates the left side layout with buttons and a ComboBox.
   *
   * @return The VBox layout for the left side.
   */
  private VBox createLeftSideLayout() {
    Button addItemButton = new Button("Add Item");
    addItemButton.setOnAction(e -> handleAddItem());

    Button removeItemButton = new Button("Remove Selected Item");
    removeItemButton.setOnAction(e -> handleRemoveSelectedItem());

    Button saveListButton = new Button("Save this Shopping List");
    saveListButton.setOnAction(e -> handleSaveList());

    Button printListButton = new Button("Print Shopping List");
    printListButton.setOnAction(e -> handlePrintList());

    // saving a ComboBox for later reference and initialization
    savedListsComboBox = createSavedListsComboBox();

    // initializing the VBox for the buttons
    VBox buttonLayout = new VBox(10);
    buttonLayout.getChildren().addAll(
        addItemButton,
        removeItemButton,
        saveListButton,
        printListButton
    );
    buttonLayout.setAlignment(Pos.TOP_CENTER);
    VBox.setVgrow(buttonLayout, Priority.ALWAYS); // button layout to grow vertically if needed

    // initializing the VBox for the left side layout
    VBox leftSideLayout = new VBox(10);
    leftSideLayout.getChildren().addAll(buttonLayout, savedListsComboBox); // saving the ComboBox
    leftSideLayout.setAlignment(Pos.TOP_CENTER);
    leftSideLayout.setPadding(new Insets(10));

    return leftSideLayout;
  }




  /**
   * Creates the button layout for the GUI.
   *
   * @return The VBox layout for the buttons.
   */
  private VBox createButtonLayout() {
    Button addItemButton = new Button("Add Item");
    addItemButton.setOnAction(e -> handleAddItem());

    Button removeItemButton = new Button("Remove Selected Item");
    removeItemButton.setOnAction(e -> handleRemoveSelectedItem());

    Button saveListButton = new Button("Save this Shopping List");
    saveListButton.setOnAction(e -> handleSaveList());

    Button printListButton = new Button("Print Shopping List");
    printListButton.setOnAction(e -> handlePrintList());

    VBox buttonLayout = new VBox(10);
    buttonLayout.getChildren()
        .addAll(addItemButton, removeItemButton, saveListButton, printListButton);
    buttonLayout.setAlignment(Pos.TOP_CENTER);
    buttonLayout.setPrefWidth(250); // width for the button layout

    return buttonLayout;
  }




  /**
   * Sets the style sheets for the GUI.
   */
  private void setStyleSheets() {
    this.getStylesheets()
        .add(Objects.requireNonNull(getClass()
            .getResource("/css/shopping-list-gui.css")).toExternalForm());
    this.getStylesheets().add(
        Objects.requireNonNull(
            getClass().getResource("/css/accordian-style.css")).toExternalForm());
  }





  /**
   * Sets up the table view for the grocery items with appropriate columns and properties.
   *
   * @return The configured TableView for grocery items.
   */
  private TableView<Grocery> createGroceryTableView() {
    TableView<Grocery> tableView = new TableView<>();
    tableView.setEditable(true);

    TableColumn<Grocery, String> nameColumn = new TableColumn<>("Item");
    nameColumn.setCellValueFactory(new PropertyValueFactory<>("groceryItemName"));
    nameColumn.setCellFactory(column -> new EditableTableCell<>(new DefaultStringConverter()));

    TableColumn<Grocery, String> quantityColumn = new TableColumn<>("Quantity");
    quantityColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));
    quantityColumn.setCellFactory(column -> new EditableTableCell<>(new DefaultStringConverter()));

    tableView.getColumns().add(nameColumn);
    tableView.getColumns().add(quantityColumn);
    tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

    return tableView;
  }






  /**
   * Creates the search field layout.
   *
   * @return The HBox layout for the search field.
   */
  private HBox createSearchField() {
    searchField = new TextField();
    searchField.setPromptText("Search for an item");

    ImageView searchIcon = new ImageView(new Image(Objects.requireNonNull(
        getClass().getResourceAsStream("/images/button-icons/search-icon.png"))));
    searchIcon.setFitHeight(23);
    searchIcon.setFitWidth(23);

    Button searchButton = new Button();
    searchButton.setGraphic(searchIcon);
    searchButton.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

    searchButton.setOnAction(e -> performSearch());
    searchField.textProperty().addListener((observable, oldValue, newValue) -> performSearch());

    HBox searchLayout = new HBox(10, searchField, searchButton);
    searchLayout.setAlignment(Pos.CENTER_LEFT);
    return searchLayout;
  }




  /**
   * Creates a ComboBox for selecting saved shopping lists.
   *
   * @return The ComboBox for saved shopping lists.
   */
  private ComboBox<String> createMyShoppingListsComboBox() {
    ComboBox<String> comboBox = new ComboBox<>();
    comboBox.getItems().addAll(shoppingListController.getSavedShoppingLists());
    comboBox.setOnAction(e -> shoppingListController.loadShoppingList(comboBox.getValue()));
    return comboBox;
  }



  /**
   * Handles the printing of the shopping list.
   */
  private void handlePrintList() {
    // TODO: Implement print functionality
    System.out.println("Print functionality not implemented");
  }



  /**
   * Performs a search based on the text in the search field.
   */
  private void performSearch() {
    String searchQuery = searchField.getText().toLowerCase();

    // filtering the list based on the search query
    List<Grocery> filteredList = shoppingList.getGroceryItems().stream()
        .filter(grocery -> grocery.getGroceryItemName().toLowerCase().contains(searchQuery))
        .collect(Collectors.toList());

    groceryTableView.getItems().setAll(filteredList); // setting the filtered list to the table view

    if (searchQuery.isEmpty()) {  // if the search query is empty, update the table view
      updateTableView();
    }
  }



  /**
   * Handles the removal of the selected item from the shopping list.
   */
  private void handleRemoveSelectedItem() {
    Grocery selected = groceryTableView.getSelectionModel().getSelectedItem();
    if (selected != null) {
      shoppingListController.removeItem(selected.getGroceryItemName());
      groceryTableView.getItems().remove(selected);
    }
  }



  /**
   * Handles the addition of a new item to the shopping list.
   */
  private void handleAddItem() {
    TextInputDialog dialog = new TextInputDialog();

    dialog.setTitle("Add New Item");
    dialog.setHeaderText("Add a new item to the shopping list");
    dialog.setContentText("Item name:");

    // Adding the new item to the shopping list
    Optional<String> result = dialog.showAndWait();
    result.ifPresent(itemName -> {
      Grocery newItem = new Grocery(itemName, "1");
      shoppingListController.addNewItem(newItem);
    });

  }

  /**
   * Updates the display of the grocery table view
   * to reflect the current items in the shopping list.
   */
  public void updateTableView() {
    List<Grocery> items = shoppingList.getGroceryItems();
    if (items != null && !items.isEmpty()) {
      groceryTableView.setItems(FXCollections.observableArrayList(items));
    } else {
      groceryTableView.getItems().clear(); // clearing the table if no items
      groceryTableView.getSelectionModel().clearSelection();
    }
    groceryTableView.refresh();
  }



  /**
   * Clears the table view of all items.
   */
  public void clearTableView() {
    groceryTableView.getItems().clear();
  }

}
