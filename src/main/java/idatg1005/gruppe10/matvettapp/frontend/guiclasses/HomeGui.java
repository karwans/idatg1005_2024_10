package idatg1005.gruppe10.matvettapp.frontend.guiclasses;

import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import idatg1005.gruppe10.matvettapp.commonutilities.errorhandler.CustomExceptionUtility.CustomIoException;


/**
 * The HomeGUI class represents the home screen in the application's GUI.
 * It displays a welcome message to the user and
 * provides buttons to navigate to different parts of the application,
 * such as recipes, shopping list, and inventory management.
 */
public class HomeGui extends BorderPane {

  private static final Logger LOGGER = Logger.getLogger("frontend.guiclasses.HomeGUI");
  private static final double SPACING = 15; // spacing between navigation buttons
  private static final Insets PADDING = new Insets(20); // padding around navigation buttons

  private Text welcomeText;
  private Button recipeButton;
  private Button shoppingListButton;
  private Button inventoryButton;

  private final String backgroundImageUri; // URI for the background image
  private final String cssStylesheetUri;   // URI for the CSS stylesheet



  /**
   * Constructs a HomeGUI object,
   * initializes its components with provided image and stylesheet URIs.
   * The constructor sets up the GUI elements and applies the CSS styling.
   *
   * @param backgroundImageUri the relative path to the background image resource.
   * @param cssStylesheetUri the relative path to the CSS stylesheet resource.
   */
  public HomeGui(String backgroundImageUri, String cssStylesheetUri) {
    this.backgroundImageUri = backgroundImageUri;
    this.cssStylesheetUri = cssStylesheetUri;
    loadStyleSheet();         // Load and apply CSS stylesheet
    initializeComponents();   // Initialize GUI components
  }




  /**
   * Loads the CSS stylesheet from the specified URI and applies it to the home GUI.
   * If the resource cannot be found, it logs a warning and throws a custom I/O exception.
   *
   * @throws CustomIoException if the CSS stylesheet resource cannot be loaded.
   */
  private void loadStyleSheet() {
    try {
      getStylesheets()
          .add(Objects.requireNonNull(getClass()
              .getResource(cssStylesheetUri)).toExternalForm());

    } catch (NullPointerException e) {
      LOGGER.warning("Failed to load CSS stylesheet for HomeGUI.");
      throw new CustomIoException("Failed to load CSS stylesheet for HomeGUI.", e);
    }
  }




  /**
   * Initializes the components of the GUI.
   * This method sets up the welcome text, background, and navigation buttons.
   */
  private void initializeComponents() {
    setupWelcomeText();
    setupBackground();
    setupNavigationButtons();
    layoutComponents();
  }




  /**
   * Sets up the welcome text with predefined properties.
   */
  private void setupWelcomeText() {
    welcomeText = new Text("Welcome to MatVett");    // The welcome message to the user

    welcomeText.setFont(Font.font(null, FontWeight.BOLD, 60));
    welcomeText.setFill(Color.LIGHTBLUE);
    welcomeText.setStroke(Color.BLACK);

    welcomeText.setStrokeWidth(1);       // stroke width for the text
    welcomeText.setId("welcome-text");   // ID for styling with CSS
  }



  /**
   * Sets up the background of the GUI using an image loaded from the specified URI.
   * If the image cannot be loaded, logs an error and throws a custom I/O exception.
   *
   * @throws CustomIoException if the background image resource cannot be loaded.
   */
  private void setupBackground() {
    try {
      Image image = new Image(
          Objects.requireNonNull(getClass().getResourceAsStream(backgroundImageUri)));

      // Adding a background image with the specified properties:
      BackgroundImage backgroundImage = new BackgroundImage(image,
          BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
          BackgroundPosition.CENTER,
          new BackgroundSize(
              BackgroundSize.AUTO,
              BackgroundSize.AUTO, true, true, true, true)
      );
      setBackground(new Background(backgroundImage));

    } catch (NullPointerException e) {
      LOGGER.log(Level.SEVERE, "Failed to load background image for HomeGUI.");

      throw new CustomIoException("Failed to load background image for HomeGUI.", e);
    }
  }




  /**
   * Initializes and styles the navigation buttons for the home screen.
   */
  private void setupNavigationButtons() {
    recipeButton = new Button("Recipes");
    shoppingListButton = new Button("Shopping List");
    inventoryButton = new Button("Inventory");

    // Add CSS classes to style the buttons
    String buttonStyle = "menu-button";
    recipeButton.getStyleClass().add(buttonStyle);
    shoppingListButton.getStyleClass().add(buttonStyle);
    inventoryButton.getStyleClass().add(buttonStyle);
  }




  /**
   * Arranges the layout components within the BorderPane.
   */
  private void layoutComponents() {
    VBox menuBox = new VBox(SPACING, recipeButton, shoppingListButton, inventoryButton);

    menuBox.setAlignment(Pos.CENTER);    // Center the navigation buttons
    menuBox.setPadding(PADDING);         // Add padding around the buttons
    menuBox.getStyleClass().add("menu-box");

    setTop(welcomeText);
    setBottom(menuBox);

    // Align the welcome text and navigation buttons in the center of the screen:
    setAlignment(welcomeText, Pos.CENTER);
    setAlignment(menuBox, Pos.CENTER);
  }




  // Getters for the navigation buttons:



  /**
   * Gets the recipe button.
   *
   * @return The button for navigating to the recipe section.
   */
  public Button getRecipeButton() {
    return recipeButton;
  }


  /**
   * Gets the shopping list button.
   *
   * @return The button for navigating to the shopping list section.
   */
  public Button getShoppingListButton() {
    return shoppingListButton;
  }


  /**
   * Gets the inventory button.
   *
   * @return The button for navigating to the inventory management section.
   */
  public Button getInventoryButton() {
    return inventoryButton;
  }
}
