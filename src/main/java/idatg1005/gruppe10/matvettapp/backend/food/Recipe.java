package idatg1005.gruppe10.matvettapp.backend.food;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * {@code Recipe} represents a food recipe,
 * encapsulating the essential elements required to prepare a dish.
 * This class provides structured details about ingredients,
 * instructions, and relevant categorization to aid users in cooking processes.
 * It is designed to offer step-by-step instructions along with a comprehensive
 * list of ingredients.
 */
public class Recipe {
  private String recipeName;
  private final ArrayList<Grocery> ingredients;
  private String category;
  private String imagePath;
  private String prepTime;
  private String cookTime;
  private String instructions;  // Step-by-step instructions for preparing the dish
  private List<String> ingredientDescriptions; // List of ingredient descriptions


  /**
   * Constructs a new Recipe with specified details.
   * Each parameter is crucial to defining a complete recipe.
   *
   * @param name         the name of the recipe
   *                     must be non-null
   *                     and contain only alphabetic characters and special characters like '-'
   *                     and space.
   * @param category     the category of the recipe, to help in sorting and filtering recipes.
   * @param prepTime     the estimated preparation time required before cooking.
   * @param cookTime     the estimated cooking time.
   * @param imagePath    the file path to an image representing the recipe,
   *                     could be relative or absolute.
   * @param instructions detailed step-by-step instructions for preparing the meal.
   */
  public Recipe(String name, String category, String prepTime, String cookTime,
      String imagePath, String instructions) {

    setRecipeName(name);
    this.category = category;
    this.ingredients = new ArrayList<>();
    this.imagePath = imagePath;
    this.prepTime = prepTime;
    this.cookTime = cookTime;
    this.instructions = instructions;

  }




  /**
   * Adds an ingredient to the recipe. Ingredients are fundamental for the recipe construction.
   *
   * @param ingredient the ingredient to add, cannot be null.
   * @throws IllegalArgumentException if the ingredient is null.
   */
  public void addIngredient(Grocery ingredient) {
    if (ingredient == null) {
      throw new IllegalArgumentException("Ingredient cannot be null.");
    }
    ingredients.add(ingredient);
  }






  /**
   * Sets the name of the recipe with validation
   * to ensure it conforms to acceptable character rules.
   *
   * @param recipeName the name of the recipe,
   *                   it should contain only alphabets, special characters like '-', and spaces.
   * @throws IllegalArgumentException if the recipeName is null or contains invalid characters.
   */
  public void setRecipeName(String recipeName) {
    if (recipeName == null || !recipeName.matches("[a-zA-ZæøåÆØÅ' -]+")) {
      throw new
          IllegalArgumentException("Recipe name must contain only alphabets and cannot be null.");
    }
    this.recipeName = recipeName;
  }

  /**
   * Returns the name of the recipe.
   *
   * @return the name of the recipe.
   */
  public String getRecipeName() {
    return recipeName;
  }




  /**
   * Returns an iterator for the ingredient list.
   * This provides a way to sequentially access ingredients.
   *
   * @return an iterator for the ingredients.
   */
  public Iterator<Grocery> getIngredientsIterator() {
    return ingredients.iterator();
  }

  /**
   * Returns a list of all ingredients. This ensures encapsulation by returning a copy of the list.
   *
   * @return a copy of the ingredient list.
   */
  public List<Grocery> getIngredients() {
    return new ArrayList<>(ingredients);
  }




  /**
   * Sets the category of the recipe.
   *
   * @param category the category of the recipe.
   */
  public void setCategory(String category) {
    validateForNonNull(category);
    this.category = category;
  }

  /**
   * Returns the category of the recipe.
   *
   * @return the category of the recipe.
   */
  public String getCategory() {
    return category;
  }




  /**
   * Returns the image path of the recipe.
   *
   * @return the image path.
   */
  public String getImagePath() {
    return imagePath;
  }

  /**
   * Sets the image path for the recipe.
   *
   * @param imagePath the path to the image file.
   */
  public void setImagePath(String imagePath) {
    validateForNonNull(imagePath);
    this.imagePath = imagePath;
  }




  /**
   * Returns the preparation time of the recipe.
   *
   * @return the preparation time.
   */
  public String getPrepTime() {
    return prepTime;
  }

  /**
   * Sets the preparation time of the recipe.
   *
   * @param prepTime the preparation time to set.
   */
  public void setPrepTime(String prepTime) {
    validateForNonNull(prepTime);
    this.prepTime = prepTime;
  }





  /**
   * Returns the cooking time of the recipe.
   *
   * @return the cooking time.
   */
  public String getCookTime() {
    return cookTime;
  }

  /**
   * Sets the cooking time of the recipe.
   *
   * @param cookTime the cooking time to set.
   */
  public void setCookTime(String cookTime) {
    validateForNonNull(cookTime);
    this.cookTime = cookTime;
  }



  /**
   * Returns the step-by-step instructions for preparing the dish.
   *
   * @return the instructions.
   */
  public String getInstructions() {
    return instructions;
  }





  /**
   * Sets the instructions for preparing the dish.
   *
   * @param instructions the detailed instructions to set.
   */
  public void setInstructions(String instructions) {
    validateForNonNull(instructions);
    this.instructions = instructions;
  }




  /**
   * Sets the list of ingredient descriptions.
   *
   * @param ingredientDescriptions the list of ingredient descriptions.
   */
  public void setIngredientDescriptions(List<String> ingredientDescriptions) {
    this.ingredientDescriptions = ingredientDescriptions;
  }



  /**
   * Returns the list of ingredient descriptions.
   *
   * @return the list of ingredient descriptions.
   */
  public List<String> getIngredientDescriptions() {
    return ingredientDescriptions;
  }


  /**
   * Validates that the provided object is not null.
   *
   * @param object the object to validate
   * @throws IllegalArgumentException if the object is null
   */
  private void validateForNonNull(Object object) {
    if (object == null) {
      throw new IllegalArgumentException("Object cannot be null.");
    }
  }

}