package idatg1005.gruppe10.matvettapp.backend.food;


import java.util.Objects;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * {@code Grocery} represents a grocery item in the grocery manager application.
 * Each item has a name, quantity, and category, and can be marked as selected.
 */
public class Grocery {


  private String groceryItemName;
  private String quantity;
  private final BooleanProperty selected;
  private final StringProperty category = new SimpleStringProperty(this, "category");



  /**
   * Constructs a new Grocery item with the specified name and quantity.
   * Both the name and quantity must not be null.
   *
   * @param name     the name of the grocery item and
   * @param quantity the quantity of the grocery item
   * @throws IllegalArgumentException if name or quantity is null or invalids
   */
  public Grocery(String name, String quantity) {
    setGroceryName(name);
    setQuantity(quantity);
    this.selected = new SimpleBooleanProperty(false); // selected as false by default
  }





  /**
   * Sets the name of the grocery item after validating against a pattern.
   *
   * @param groceryItemName The name to be set, must not be null & must-match the specified pattern
   * @throws IllegalArgumentException if the name is null or does not match the required pattern
   */
  public void setGroceryName(String groceryItemName) {
    String validPattern = "[a-zA-ZæøåÆØÅ0-9\\s\\-()]+";

    if (groceryItemName == null /*|| !groceryItemName.matches(validPattern)*/) {
      throw new
          IllegalArgumentException(
              "Name must contain only allowed characters and cannot be null.");
    }
    this.groceryItemName = groceryItemName;
  }



  /**
   * Returns the name of the grocery item.
   *
   * @return the current name of the grocery item
   */
  public String getGroceryItemName() {
    return groceryItemName;
  }





  /**
   * Sets the quantity of the grocery item.
   *
   * @param quantity The quantity to be set, must not be null
   * @throws IllegalArgumentException if the quantity is null
   */
  public void setQuantity(String quantity) {
    if (quantity == null) {
      throw new IllegalArgumentException("Quantity cannot be null.");
    }
    this.quantity = quantity;
  }

  /**
   * Returns the quantity of the grocery item.
   *
   * @return the current quantity of the grocery item
   */
  public String getQuantity() {
    return quantity;
  }




  /**
   * Sets the category of the grocery item after validating against a pattern.
   *
   * @param value The category to be set, must not be null and must-match specified pattern
   * @throws IllegalArgumentException if the category is null, empty,
   *                                  or does not match the required pattern
   *
   */
  public void setCategory(String value) {
    if (value == null || value.isEmpty()) {  // Explicit check for null or empty string
      throw new IllegalArgumentException("Category must not be null or empty.");
    }

    String validPattern = "[a-zA-ZæøåÆØÅ]+";
    if (!value.matches(validPattern)) {
      throw new IllegalArgumentException("Category must contain only allowed characters.");
    }

    this.category.set(value);
  }




  /**
   * Returns the category of the grocery item.
   *
   * @return The category of the grocery item
   */
  public StringProperty getCategory() {
    return category;
  }




  /**
   * Sets the selected property of the grocery item as selected or not selected.
   * If the grocery item is selected, the value is true.
   * Otherwise, the value is false.
   */
  public final void setSelected(boolean value) {
    selected.set(value);
  }

  /**
   * Returns the selected property of the grocery item.
   *
   * @return The selected property of the grocery item
   */
  public BooleanProperty selectedProperty() {
    return selected;
  }





  /**
   * Provides a string representation of the grocery item, including name and quantity.
   *
   * @return a formatted string containing the name and quantity of the grocery item
   */
  @Override
  public String toString() {
    return String.format("%s: %s", this.getGroceryItemName(), this.getQuantity());
  }


  /**
   * Checks if this grocery item is equal to another object based on the grocery
   * item's name and quantity.
   *
   * @param obj the object to compare with this instance
   * @return true if both objects are grocery items with the same name and quantity
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    Grocery grocery = (Grocery) obj;
    return Objects.equals(groceryItemName, grocery.groceryItemName)
        && Objects.equals(quantity, grocery.quantity);
  }


  /**
   * Returns a hash code value for the grocery item, derived from the name and quantity.
   *
   * @return a hash code value for the grocery item
   */
  @Override
  public int hashCode() {
    return Objects.hash(groceryItemName, quantity);
  }

}
