package idatg1005.gruppe10.matvettapp.backend.accountmanager;


/**
 * The {@code EndUser} class represents an end user account type.
 * It extends the {@code UserAccount} class and has a full name.
 */
public class EndUser extends UserAccount {

  private String fullName;  // The full name of the end user

  /**
   * Constructor for the {@code EndUser} class. The account type, username, and password are set.
   *
   * @param userName The username of the end user.
   * @param password The password of the end user.
   * @param email The email of the end user.
   * @param address The address of the end user.
   * @param fullName The full name of the end user.
   */
  public EndUser(String userName, String password, String email, String address, String fullName) {
    super(userName, userName, password, email, address);
    this.userName = "End User";
    setFullName(fullName);
  }


  /**
   * Sets the full name of the end user.
   *
   * @param fullName The full name to be set.
   * @throws IllegalArgumentException if the full name is invalid.
   */
  public void setFullName(String fullName) {
    if (fullName != null) {
      this.fullName = fullName;
    } else {
      throw new IllegalArgumentException("Invalid full name");
    }
  }


  /**
   * Returns the full name of the end user.
   *
   * @return The full name of the end user.
   */
  public String getFullName() {
    return fullName;
  }

}