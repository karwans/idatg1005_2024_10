package idatg1005.gruppe10.matvettapp.backend.accountmanager;

public class AccountType {
  protected String accountType; // Admin, User or ThirdParty
  protected String userName;
  protected String password;
  private LoginManager loginManager;

  /**
   * Constructor for {@code AccountType} class. The account type, username, and password are set.
   *
   * @param accountType The account type of the user (admin, user, and third party).
   * @param userName The username of the user.
   * @param password The password of the user.
   */
  public AccountType(String accountType, String userName, String password) {
    setUserType(accountType);
    setUserName(userName);
    setPassword(password);
  }

  /**
   * Sets the account type of the user. The admin, user, and third party are the only valid account.
   *
   * @param accountType The account type of the user
   * @throws IllegalArgumentException if the account type is invalid
   */
  public void setUserType(String accountType) {
    if (accountType.equals("Admin") || accountType.equals("User") || accountType.equals("ThirdParty")) {
      this.accountType = accountType;
    } else {
      throw new IllegalArgumentException("Invalid account type");
    }
  }


  /**
   * Sets the username of the user. The username must be unique and alphanumeric.
   *
   * @param userName The username to be set.
   * @throws IllegalArgumentException if the username is invalid or already taken.
   */
  public void setUserName (String userName) {
    if (userName != null && !userName.isEmpty() && userName.length() < 20 && userName.matches("^[a-zA-Z0-9]*$")) {
      if (loginManager.isUserNameAvailable(userName)) {
        this.userName = userName;
      } else {
        throw new IllegalArgumentException("Username " + userName + " is already taken");
      }
    } else {
      throw new IllegalArgumentException("Invalid username");
    }
  }


  /**
   * Sets the password of the user. The password must be alphanumeric and less than 20 characters.
   *
   * @param password The password to be set.
   * @throws IllegalArgumentException if the password is invalid.
   */
  public void setPassword (String password) {
    if (password != null && !password.isEmpty() && password.length() < 20 && password.matches("^[a-zA-Z0-9]*$")) {
      this.password = password;
    } else {
      throw new IllegalArgumentException("Invalid password");
    }
  }


  /**
   * Returns the account type of the user.
   *
   * @return The account type of the user.
   */
  public String getAccountType() {
    return accountType;
  }


  /**
   * Returns the username of the user.
   *
   * @return The username of the user.
   */
  public String getUserName() {
    return userName;
  }


  /**
   * Returns the password of the user.
   *
   * @return The password of the user.
   */
  public String getPassword() {
    return password;
  }
}