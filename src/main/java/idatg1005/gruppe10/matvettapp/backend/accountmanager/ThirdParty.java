package idatg1005.gruppe10.matvettapp.backend.accountmanager;


/**
 * The {@code ThirdParty} class represents a third party account type.
 * Such as a company that uses the system.
 * It extends the {@code UserAccount} class and has a company name.
 */
public class ThirdParty extends UserAccount {

  private String companyName;

  public ThirdParty(String accountType, String userName, String password, String email, String address, String companyName) {
    super(accountType, userName, password, email, address);
    this.accountType = "ThirdParty";
    setCompanyName(companyName);
  }

  public void setCompanyName(String companyName) {
    if (companyName != null) {
      this.companyName = companyName;
    } else {
      throw new IllegalArgumentException("Invalid company name");
    }
  }

  public String getCompanyName() {
    return companyName;
  }

}