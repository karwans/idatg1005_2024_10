package idatg1005.gruppe10.matvettapp.backend.accountmanager;



/**
 * The {@code UserAccount} class represents a user account type.
 * It extends the {@code AccountType} class and has an email and address.
 */
public class UserAccount extends AccountType {
  protected String email;
  protected String address;

  /**
   * Constructor for the {@code UserAccount} class. The account type, username, and password are set.
   *
   * @param accountType The account type of the user.
   * @param userName The username of the user.
   * @param password The password of the user.
   * @param email The email of the user.
   * @param address The address of the user.
   */
  public UserAccount(String accountType, String userName, String password, String email, String address) {
    super(accountType, userName, password);
    setEmail(email);
    setAddress(address);
  }


  /**
   * Sets the address of the user.
   *
   * @param address The address to be set.
   * @throws IllegalArgumentException if the address is invalid.
   */
  public void setAddress(String address) {
    if (address != null) {
      this.address = address;
    } else {
      throw new IllegalArgumentException("Invalid address");
    }
  }


  /**
   * Sets the email of the user.
   *
   * @param email The email to be set.
   * @throws IllegalArgumentException if the email is invalid.
   */
  public void setEmail(String email) {
    if (email != null) {
      this.email = email;
    } else {
      throw new IllegalArgumentException("Invalid email");
    }
  }


  /**
   * Returns the address of the user.
   *
   * @return The address of the user.
   */
  public String getAddress() {
    return address;
  }


  /**
   * Returns the email of the user.
   *
   * @return The email of the user.
   */
  public String getEmail() {
    return email;
  }

  /*
  public static UserAccount createAccount() {

  }
   */
}