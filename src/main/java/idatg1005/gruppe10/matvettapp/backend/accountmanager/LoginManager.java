package idatg1005.gruppe10.matvettapp.backend.accountmanager;

import static java.lang.String.valueOf;

import java.util.HashMap;


/**
 * The {@code LoginManager} class manages the login process for the system.
 * It keeps track of the usernames that are taken and checks if a username is available.
 */
public class LoginManager {


  private HashMap<String, AccountType> takenUserNames;  // The usernames that are taken


  /**
   * Constructor for the {@code LoginManager} class. The taken usernames are initialized.
   */
  public LoginManager() {
    takenUserNames = new HashMap<>();
  }


  /**
   * Checks if a username is available.
   *
   * @param userName The username to be checked.
   * @return {@code true} if the username is available, {@code false} otherwise.
   */
  public boolean isUserNameAvailable(String userName) {
    return !takenUserNames.containsKey(userName);
  }


  /**
   * Marks a username as taken.
   *
   * @param userName The username to be marked as taken.
   * @param accountType The account type of the user.
   * @throws IllegalArgumentException if the username is already taken.
   */
  public void markUserNameAsTaken(String userName, AccountType accountType) {
    if (userName == null || (!isUserNameAvailable(userName) && takenUserNames.containsKey(valueOf(userName)))) {
      takenUserNames.put(userName, accountType);
    } else {
      throw new IllegalArgumentException("Username " + userName + " is already taken");
    }
  }
}
