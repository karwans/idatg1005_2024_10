package idatg1005.gruppe10.matvettapp.backend.accountmanager;

/**
 * The {@code Admin} class represents an admin account type.
 * It extends the {@code AccountType} class and has an admin level.
 */
public class Admin extends AccountType {

  private int adminLevel;  // The admin level of the admin account


  /**
   * Constructor for the {@code Admin} class. The account type, username, and password are set.
   *
   * @param userName The username of the admin.
   * @param password The password of the admin.
   */
  public Admin(String userName, String password) {
    super("Admin", userName, password);
    setAdminLevel(adminLevel);
  }


  /**
   * Sets the admin level of the admin account. The admin level must be between 0 and 10.
   *
   * @param adminLevel The admin level to be set.
   * @throws IllegalArgumentException if the admin level is invalid.
   */
  public void setAdminLevel(int adminLevel) {
    if (adminLevel >= 0 && adminLevel <= 10) {
      this.adminLevel = adminLevel;
    } else {
      throw new IllegalArgumentException("Invalid admin level");
    }
  }


  /**
   * Returns the admin level of the admin account.
   *
   * @return The admin level of the admin account.
   */
  public int getAdminLevel() {
    return adminLevel;
  }


}