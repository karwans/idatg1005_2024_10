package idatg1005.gruppe10.matvettapp.backend.storageclasses;

import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Manages the storage and retrieval of grocery items categorized by type.
 * Provides functionalities to add, remove, and query grocery items across categories.
 */
public class Inventory {
  private final Map<String, List<Grocery>> categorizedGroceries;
  public static final String CANNOT_BE_NULL_NOR_EMPTY = "Category cannot be null or empty.";




  /**
   * Constructs a new Inventory with an empty map to hold categorized groceries.
   */
  public Inventory() {
    this.categorizedGroceries = new HashMap<>();
  }



  /**
   * Adds a grocery item to the specified category in the inventory.
   * If the category does not exist, it is created automatically.
   *
   * @param item     the {@link Grocery} item to add; must not be null.
   * @param category the category under which the item should be added; must not be null or empty.
   * @throws IllegalArgumentException if the item or category is null, or if the category is empty.
   */
  public void addItem(Grocery item, String category) {
    if (item == null) {
      throw new IllegalArgumentException("Item cannot be null.");
    }
    if (category == null || category.trim().isEmpty()) {
      throw new IllegalArgumentException(CANNOT_BE_NULL_NOR_EMPTY);
    }
    this.categorizedGroceries.computeIfAbsent(category, k -> new ArrayList<>()).add(item);
  }


  /**
   * Removes a grocery item from the specified category.
   *
   * @param item     the {@link Grocery} item to remove.
   * @param category the category from which to remove the item; must not be null or empty.
   * @return true if the item was successfully removed, false if it was not found.
   * @throws IllegalArgumentException if the category is null or empty.
   */
  public boolean removeInventoryItem(Grocery item, String category) {
    if (category == null || category.trim().isEmpty()) {
      throw new IllegalArgumentException(CANNOT_BE_NULL_NOR_EMPTY);
    }
    List<Grocery> items = categorizedGroceries.get(category);
    return items != null && items.remove(item);
  }


  /**
   * Retrieves all grocery items categorized under a specific category.
   *
   * @param category the category to retrieve items from; must not be null or empty.
   * @return a list of grocery items in the specified category.
   * @throws IllegalArgumentException if the category is null or empty.
   */
  public List<Grocery> getGroceriesForCategory(String category) {
    if (category == null || category.trim().isEmpty()) {
      throw new IllegalArgumentException(CANNOT_BE_NULL_NOR_EMPTY);
    }
    return new ArrayList<>(categorizedGroceries.getOrDefault(category, new ArrayList<>()));
  }


  /**
   * Retrieves a list of all grocery items in all categories.
   *
   * @return a list of all {@link Grocery} items in the inventory.
   */
  public List<Grocery> getInventoryItems() {
    List<Grocery> allItems = new ArrayList<>();
    for (List<Grocery> items : categorizedGroceries.values()) {
      allItems.addAll(items);
    }
    return allItems;
  }


  /**
   * Gets a list of all categories that have been defined in the inventory.
   *
   * @return a list of category names.
   */
  public List<String> getCategories() {
    return new ArrayList<>(categorizedGroceries.keySet());
  }


  /**
   * Provides a safe copy of the categorized items map.
   *
   * @return a copy of the map containing all categories and their respective grocery items.
   */
  public Map<String, List<Grocery>> getCategorizedItems() {
    return new HashMap<>(categorizedGroceries);
  }

}


// TODO: This class is very similar to the GroceryManager class
