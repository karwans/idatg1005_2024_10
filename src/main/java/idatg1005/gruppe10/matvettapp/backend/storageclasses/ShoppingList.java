package idatg1005.gruppe10.matvettapp.backend.storageclasses;

import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import java.util.Iterator;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


/**
 * Manages a list of grocery items for shopping purposes.
 * Provides functionality to add, remove, and manipulate the list of groceries,
 * facilitating easy modifications and retrieval of items.
 */
public class ShoppingList {

  private ObservableList<Grocery> groceryItems;



  /**
   * Initializes a new ShoppingList with an empty list of grocery items.
   */
  public ShoppingList() {
    this.groceryItems = FXCollections.observableArrayList();
  }





  /**
   * Adds a grocery item to the shopping list.
   *
   * @param item The grocery item to be added; must not be null.
   */
  public void addGroceryItem(Grocery item) {
    if (item == null) {
      throw new IllegalArgumentException("Grocery item cannot be null.");
    }
    groceryItems.add(item);
  }



  /**
   * Removes a grocery item from the shopping list.
   *
   * @param item The grocery item to be removed; must not be null.
   */
  public void removeGroceryItem(Grocery item) {
    if (item == null) {
      throw new IllegalArgumentException("Grocery item cannot be null.");
    }
    groceryItems.remove(item);
  }



  /**
   * Sets the list of grocery items to a new list.
   * Replaces the current list with a new list of items.
   *
   * @param newItems The new list of grocery items to set; must not be null.
   */
  public void setGroceryItems(List<Grocery> newItems) {
    if (newItems == null) {
      throw new IllegalArgumentException("List of new items cannot be null.");
    }
    this.groceryItems = FXCollections.observableList(newItems);
  }



  /**
   * Replaces the current list of grocery items with a new list.
   * Clears the existing list and adds all items from the provided list.
   *
   * @param newItems A list of grocery items to replace the existing items.
   */
  public void replaceItems(List<Grocery> newItems) {
    if (newItems == null) {
      throw new IllegalArgumentException("List of new items cannot be null.");
    }
    groceryItems.clear();
    groceryItems.addAll(newItems);
  }



  /**
   * Returns an iterator for the grocery items in the shopping list.
   * This allows for iterating over the items without exposing the internal list structure.
   *
   * @return An iterator over the grocery items in the shopping list.
   */
  public Iterator<Grocery> getShoppingListItemsIterator() {
    return groceryItems.iterator();
  }




  /**
   * Returns an unmodifiable list of grocery items from the shopping list.
   * This method provides a snapshot of grocery items at the time of calling, ensuring immutability.
   * Attempts to modify the returned list will throw an {@link UnsupportedOperationException}.
   *
   * @return A read-only List of {@link Grocery} items.
   */
  public List<Grocery> getGroceryItems() {
    return List.copyOf(groceryItems);
  }

}