package idatg1005.gruppe10.matvettapp.backend.managerclasses;


import idatg1005.gruppe10.matvettapp.backend.food.Recipe;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import idatg1005.gruppe10.matvettapp.commonutilities.datastorage.RecipeDataStorage;
import idatg1005.gruppe10.matvettapp.commonutilities.filehandler.RecipeFileHandler;


/**
 * {@code RecipeManager} manages a collection of recipes,
 * providing functionalities to add, remove, and retrieve recipes,
 * as well as manage favorites and recently viewed recipes.
 * This class acts as a central hub for recipe organization within the application.
 */
public class RecipeManager {


  private final Map<String, Recipe> recipes;  // Maps recipe names to Recipe objects
  private Set<Recipe> favoriteRecipes; // Holds favorite recipes
  private Set<String> recentlyViewed; // Tracks names of recently viewed recipes
  private final RecipeDataStorage recipeDataStorage; // Handles persistence for recipe data



  /**
   * Constructor for RecipeManager that initializes the storage for recipes,
   * favorite recipes, and recently viewed recipes.
   */
  public RecipeManager() {
    this.recipes = new HashMap<>();
    this.favoriteRecipes = new HashSet<>();
    this.recentlyViewed = new HashSet<>();
    this.recipeDataStorage = new RecipeDataStorage(this);
  }



  /**
   * Saves the current set of favorite recipes using the data storage utility.
   */
  public void saveFavorites() {
    recipeDataStorage.saveFavorites(favoriteRecipes);
  }

  /**
   * Loads the set of favorite recipes from storage.
   */
  public void loadFavorites() {
    this.favoriteRecipes = recipeDataStorage.loadFavorites();
  }



  /**
   * Saves the list of recently viewed recipes.
   */
  public void saveRecentlyViewed() {
    recipeDataStorage.saveRecentlyViewed(recentlyViewed);
  }


  /**
   * Loads the list of recently viewed recipes from storage.
   */
  public void loadRecentlyViewed() {
    this.recentlyViewed = recipeDataStorage.loadRecentlyViewed();
  }


  /**
   * Retrieves the list of recently viewed recipe names.
   *
   * @return a set of strings representing the names of recently viewed recipes.
   */
  public Set<String> getRecentlyViewed() {
    return recentlyViewed;
  }


  /**
   * Retrieves the set of favorite recipes.
   *
   * @return a set containing the favorite recipes.
   */
  public Set<Recipe> getFavoriteRecipes() {
    return favoriteRecipes;
  }


  /**
   * Retrieves a recipe by its name.
   *
   * @param name The name of the recipe to find.
   * @return the Recipe object if found, or null otherwise.
   */
  public Recipe getRecipeByName(String name) {
    for (Recipe recipe : recipes.values()) {
      if (recipe.getRecipeName().equals(name)) {
        return recipe;
      }
    }
    return null;
  }






  /**
   * Adds a recipe to the set of favorite recipes and saves the favorites.
   *
   * @param recipe The recipe to add to favorites.
   * @throws IllegalArgumentException if the recipe is null.
   */
  public void addRecipeToFavorites(Recipe recipe) {
    if (recipe == null) {
      throw new IllegalArgumentException("Recipe cannot be null.");
    }
    favoriteRecipes.add(recipe);
    saveFavorites(); // Saving the data immediately after adding
  }



  /**
   * Adds a recipe name to the set of recently viewed recipes and saves the list.
   *
   * @param recipeName The name of the recipe to add to recently viewed.
   * @throws IllegalArgumentException if the recipe name is null or empty.
   */
  public void addRecipeToRecentlyViewed(String recipeName) {
    if (recipeName == null || recipeName.isEmpty()) {
      throw new IllegalArgumentException("Recipe name cannot be null or empty.");
    }
    recentlyViewed.add(recipeName);
    saveRecentlyViewed(); // Saving the data immediately after adding
  }


  /**
   * Removes a recipe from the set of favorite recipes.
   *
   * @param recipe The recipe to remove from favorites.
   * @throws IllegalArgumentException if the recipe is null or not in favorites.
   */
  public void removeRecipeFromFavorites(Recipe recipe) {
    if (recipe == null || !favoriteRecipes.contains(recipe)) {
      throw new IllegalArgumentException("Recipe is not in favorites or is null.");
    }
    favoriteRecipes.remove(recipe);
    recipeDataStorage.removeRecipeFromFavorites(recipe.getRecipeName());
  }



  /**
   * Adds a recipe to the collection managed by this RecipeManager.
   *
   * @param recipe the recipe to add.
   * @throws IllegalArgumentException if the recipe is invalid or the name is empty.
   */
  public void addRecipe(Recipe recipe) {
    if (recipe == null || recipe.getRecipeName() == null || recipe.getRecipeName().isEmpty()) {
      throw new IllegalArgumentException("Invalid recipe or recipe name.");
    }
    recipes.put(recipe.getRecipeName(), recipe);
  }



  /**
   * Retrieves all recipes managed by this RecipeManager.
   *
   * @return a Collection of all recipes
   */
  public Collection<Recipe> getAllRecipes() {
    return recipes.values();
  }




  /**
   * Loads all recipes from text files located in a specified directory.
   *
   * @param directoryPath The path to the directory containing recipe text files.
   */
  public void loadAllRecipes(String directoryPath) {
    RecipeFileHandler.loadAllRecipes(directoryPath, this::addRecipe);
  }



  /**
   * Retrieves all recipes that belong to a specified category.
   *
   * @param category The category to filter recipes by.
   * @return a Collection of recipes that belong to the specified category.
   */
  public Collection<Recipe> getRecipesByCategory(String category) {
    return recipes.values().stream()
        .filter(recipe -> recipe.getCategory().equalsIgnoreCase(category))
        .toList();
  }




  /**
   * Loads recipes from a directory.
   *
   * @param directoryPath The path to the directory containing the recipe text files.
   */
  public void loadRecipesFromDirectory(String directoryPath) {
    loadAllRecipes(directoryPath);
  }

}


