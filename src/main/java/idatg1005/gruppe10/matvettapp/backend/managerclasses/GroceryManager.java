package idatg1005.gruppe10.matvettapp.backend.managerclasses;

import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import idatg1005.gruppe10.matvettapp.commonutilities.filehandler.GroceryFileHandler;

/**
 * {@code GroceryManager} manages grocery items by allowing operations such as adding,
 * retrieving, and loading items from a file.
 * This class is responsible
 * for maintaining a collection of grocery items in memory,
 * ensuring synchronization with persistent file storage
 * to provide a consistent state across application sessions.
 *
 * <p>This class is designed
 * to interact directly with a {@link GroceryFileHandler} to offload the
 * responsibilities of file operations,
 * thereby following the single responsibility principle
 * by delegating file handling concerns.
 */
public class GroceryManager {

  private Map<String, List<Grocery>> categorizedGroceries; // groceries categorized by their types.
  private final String filePath;                        // Path to the file storing grocery data.
  private final GroceryFileHandler fileHandler;        // Handles loading groceries from the file.




  /**
   * Constructs a new GroceryManager instance, initializes the grocery item storage,
   * and loads existing grocery items from the specified file path
   * using a provided file handler.
   *
   * @param filePath The path to the file containing grocery items.
   * @param fileHandler An instance of {@link GroceryFileHandler} to load and read grocery data.
   */
  public GroceryManager(String filePath, GroceryFileHandler fileHandler) {
    this.filePath = filePath;
    this.fileHandler = fileHandler;
    this.categorizedGroceries = new HashMap<>();
    loadCategoriesWithItems();
  }




  /**
   * Loads grocery categories along with their items from the file.
   * This method ensures that all groceries are loaded into memory from the start,
   * and validates that the loaded data is not null.
   *
   * @throws NullPointerException if the loaded categories are null,
   *                              indicating a failure in file processing.
   */
  public void loadCategoriesWithItems() {
    Map<String, List<Grocery>> categories = fileHandler.loadCategoriesWithItems(filePath);
    if (categories == null) {
      throw new NullPointerException("Loaded categories cannot be null");
    }
    categorizedGroceries = categories;
  }





  /**
   * Retrieves a list of all grocery categories currently available in memory.
   *
   * @return A list of category names.
   * @throws IllegalStateException if no categories are available.
   */
  public List<String> getCategories() {
    Set<String> keys = categorizedGroceries.keySet();

    if (keys.isEmpty()) {  // if no categories are available
      throw new IllegalStateException(
          "No categories are available, which is required for the application.");
    }

    return new ArrayList<>(keys);
  }





  /**
   * Retrieves a list of groceries for a specific category.
   *
   * @param category The category name to retrieve groceries for.
   * @return A list of {@link Grocery} objects belonging to the specified category.
   * @throws NoSuchElementException if the specified category does not exist.
   */
  public List<Grocery> getGroceriesForCategory(String category) {
    List<Grocery> groceries = categorizedGroceries.get(category);

    if (groceries == null) {  // if the category does not exist
      throw new NoSuchElementException("The category '" + category + "' does not exist.");
    }

    return new ArrayList<>(groceries);
  }
//  public List<Grocery> getGroceriesForCategory(String category) {
//    List<Grocery> groceries = categorizedGroceries.get(category);
//    if (groceries == null) {  // if the category does not exist
//      return Collections.emptyList();  // return an empty list instead of throwing an exception
//    }
//    return new ArrayList<>(groceries);
//  }


}
