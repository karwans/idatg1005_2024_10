package idatg1005.gruppe10.matvettapp;

/**
 * The MainApplication class acts as the entry point for the application.
 * It simply calls the main method of the AppEnsemble class to start the application.
 * This separation allows for a clean organization of the application's main method.
 */
public class MainApplication {



  /**
   * Main method to launch the application.
   * This method is the entry point for the Java application.
   * It delegates the start of the application
   * to the AppEnsemble class
   * by calling its main method with the command-line arguments received.
   *
   * @param args Command line arguments passed to the application.
   */
  public static void main(String[] args) {
    AppEnsemble.main(args);
  }


}