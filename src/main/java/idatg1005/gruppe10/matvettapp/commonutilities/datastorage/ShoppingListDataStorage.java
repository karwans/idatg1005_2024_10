package idatg1005.gruppe10.matvettapp.commonutilities.datastorage;

import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import idatg1005.gruppe10.matvettapp.backend.storageclasses.ShoppingList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import idatg1005.gruppe10.matvettapp.commonutilities.errorhandler.CustomExceptionUtility.CustomIoException;


/**
 * Handles the storage and retrieval of shopping list data using JSON serialization.
 * This class provides methods to save and load shopping lists and individual shopping list items,
 * as well as manage multiple shopping lists identified by unique names. It ensures data persistence
 * by storing information in files located in the user's home directory.
 */
public class ShoppingListDataStorage {



  // Logger for debugging and error messages:
  private static final Logger LOGGER =
      Logger.getLogger("idatg1005.gruppe10.matvettapp.datastorage.utilities.ShoppingListDataStorage");



  // The path to the user's home director
  private static final String USER_HOME = System.getProperty("user.home");
  private static final Path DATASTORAGE_DIR = Paths.get(USER_HOME, ".MatVett", "datastorage");
  private static final Path SHOPPING_LIST_FILE_PATH =
      DATASTORAGE_DIR.resolve("shoppingList.json");
  private static final Path MY_SHOPPING_LISTS_FILE_PATH =
      DATASTORAGE_DIR.resolve("myShoppingLists.json");



  private final Gson gson; // Gson is a library for converting Java objects to and from JSON format




  /**
   * Constructs a ShoppingListDataStorage object and initializes Gson with custom serializers and
   * deserializers for handling complex objects such as ShoppingList and Grocery.
   */
  public ShoppingListDataStorage() {

    // Custom deserializer for ShoppingList objects:
    JsonDeserializer<ShoppingList> shoppingListDeserializer = (json, typeOfT, context) -> {
      // deserializing the list of grocery items:
      Type listType = new TypeToken<List<Grocery>>(){}.getType();
      List<Grocery> groceryList =
          context.deserialize(json.getAsJsonObject().get("groceryItems"), listType);

      ObservableList<Grocery> observableGroceryList =   // converting the list to an ObservableList
          FXCollections.observableArrayList(groceryList);
      ShoppingList shoppingList = new ShoppingList();
      shoppingList.setGroceryItems(observableGroceryList);

      return shoppingList;    // return the ShoppingList object
    };

    // custom serializer for Grocery objects: (only serializes the grocery item name and quantity)
    gson = new GsonBuilder()
        .registerTypeAdapter(Grocery.class, (JsonSerializer<Grocery>) (src, typeOfSrc, context) -> {
          JsonObject jsonObject = new JsonObject();
          jsonObject.addProperty("groceryItemName", src.getGroceryItemName());
          jsonObject.addProperty("quantity", src.getQuantity());
          return jsonObject;
        })
        .registerTypeAdapter(ShoppingList.class, shoppingListDeserializer)
        .create();

    try {
      Files.createDirectories(DATASTORAGE_DIR);
    } catch (IOException e) {
      LOGGER.log(Level.WARNING, "Could not save initialize data storage directories: ");
      throw new CustomIoException("Failed to initialize: " + DATASTORAGE_DIR, e);
    }
  }




  /**
   * Saves a single shopping list to the file system.
   *
   * @param shoppingList The ShoppingList object to be saved.
   */
  public void saveShoppingList(ShoppingList shoppingList) {
    List<Grocery> groceries = new ArrayList<>(shoppingList.getGroceryItems());

    String json = gson.toJson(groceries);  // converting the list of Grocery objects to JSON
    try {
      Files.write(SHOPPING_LIST_FILE_PATH, json.getBytes()); // writing the JSON data to the file

    } catch (IOException e) {
      LOGGER.log(Level.WARNING, "Could not save shopping list to file.");
      throw new CustomIoException("Failed to save shopping list: " + SHOPPING_LIST_FILE_PATH, e);
    }

  }





  /**
   * Loads a shopping list from the file system.
   *
   * @return ShoppingList The loaded ShoppingList object, or a new empty list if no data was found.
   */
  public ShoppingList loadShoppingList() {
    try {
      if (Files.exists(SHOPPING_LIST_FILE_PATH)) {  // check if the file exists
        String json = new String(Files.readAllBytes(SHOPPING_LIST_FILE_PATH));
        Type listType = new TypeToken<List<Grocery>>(){}.getType();
        List<Grocery> groceries = gson.fromJson(json, listType);
        ShoppingList shoppingList = new ShoppingList();
        groceries.forEach(shoppingList::addGroceryItem);  // adding each grocery item to the list
        return shoppingList;  // return the ShoppingList object
      }
    } catch (IOException e) {
      LOGGER.log(Level.WARNING, "Could not load shopping list from file.");
      throw new CustomIoException("Failed to load shopping list: " + SHOPPING_LIST_FILE_PATH, e);
    }
    return new ShoppingList();
  }




  /**
   * Saves multiple named shopping lists to the file system.
   *
   * @param shoppingLists A map of shopping list names to ShoppingList objects.
   */
  public void saveMyShoppingLists(Map<String, ShoppingList> shoppingLists) {
    String json = gson.toJson(shoppingLists);

    try {
      Files.write(MY_SHOPPING_LISTS_FILE_PATH, json.getBytes());

    } catch (IOException e) {
      LOGGER.log(Level.WARNING, "Could not save personal shopping lists to file.");
      throw new CustomIoException(
          "Failed to save shopping lists: " + MY_SHOPPING_LISTS_FILE_PATH, e);
    }

  }





  /**
   * Saves a single shopping list to the file system, identified by a unique name.
   *
   * @param listName The name of the shopping list.
   * @param shoppingList The ShoppingList object to be saved.
   */
  public void saveMyShoppingListByName(String listName, ShoppingList shoppingList) {
    Map<String, ShoppingList> shoppingLists = loadMyShoppingLists();
    shoppingLists.put(listName, shoppingList);
    saveMyShoppingLists(shoppingLists);
  }



  /**
   * Loads all named shopping lists from the file system.
   *
   * @return Map<String, ShoppingList> A map of shopping list names to their
   * corresponding ShoppingList objects.
   */
  public Map<String, ShoppingList> loadMyShoppingLists() {
    try {
      if (Files.exists(MY_SHOPPING_LISTS_FILE_PATH)) {
        String json = new String(Files.readAllBytes(MY_SHOPPING_LISTS_FILE_PATH));
        Type type = new TypeToken<Map<String, ShoppingList>>(){}.getType();
        return gson.fromJson(json, type);
      }
    } catch (IOException e) {
      LOGGER.log(Level.WARNING, "Could not load personal shopping lists from file.");
      throw new CustomIoException(
          "Failed to load shopping lists: " + MY_SHOPPING_LISTS_FILE_PATH, e);
    }
    return new HashMap<>();
  }


}
