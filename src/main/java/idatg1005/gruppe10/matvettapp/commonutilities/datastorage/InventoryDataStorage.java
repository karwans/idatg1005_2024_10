package idatg1005.gruppe10.matvettapp.commonutilities.datastorage;

import idatg1005.gruppe10.matvettapp.backend.storageclasses.Inventory;
import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import idatg1005.gruppe10.matvettapp.commonutilities.errorhandler.CustomExceptionUtility.CustomIoException;


/**
 * Provides functionality to manage the persistent storage and
 * retrieval of an inventory of groceries.
 * This class uses JSON format to serialize and deserialize the inventory data stored in a file
 * within the user's home directory under `.MatVett/data-storage`.
 * It supports operations to load and save inventory data,
 * ensuring data integrity and ease of access through high-level abstractions.
 */
public class InventoryDataStorage {

  // Logger for debugging and error messages:
  private static final Logger LOGGER =
      Logger.getLogger("idatg1005.gruppe10.matvettapp.datastorage.utilities.InventoryDataStorage");


  // the path to the user's home directory
  private static final String USER_HOME = System.getProperty("user.home");
  private static final Path DATA_STORAGE_DIR = Paths.get(USER_HOME, ".MatVett", "datastorage");
  private static final Path INVENTORY_FILE_PATH = DATA_STORAGE_DIR.resolve("inventory.json");


  // Gson is a library for converting Java objects to and from JSON format
  private final Gson gson;
  private final Inventory inventory;


  // The categories of groceries that are available in the inventory
  private static final String[] CATEGORIES = {
      "Vegetables",
      "Fruits",
      "Sauces",
      "Canned foods",
      "Spices & herbs",
      "Dairy",
      "Cheese",
      "Meat",
      "Seafood",
      "Beverages",
      "Baking",
      "Various groceries"
  };




  /**
   * Constructs an InventoryDataStorage object.
   * Initializes the Gson parser, checks and creates the necessary directories and files,
   * and loads existing inventory from the storage or initializes a new empty inventory.
   */
  public InventoryDataStorage() {
    gson = new GsonBuilder().create();
    this.inventory = loadInventory();  // loads user inventory from JSON file
    try {
      Files.createDirectories(DATA_STORAGE_DIR);
      if (!Files.exists(INVENTORY_FILE_PATH)) {

        // If the inventory.json file does not exist, this will create it with empty categories:
        createEmptyInventoryFile();
      }
    } catch (IOException e) {
      LOGGER.log(Level.WARNING, "Failed to create necessary storage directories. ");
      throw new CustomIoException("Failed to create necessary storage directories. ", e);
    }


    loadInventory();
  }




  /**
   * Loads the inventory data from the storage file.
   * If the file does not exist or is unreadable, it initializes an empty inventory.
   *
   * @return Inventory the loaded or newly created inventory object
   */
  public Inventory loadInventory() {
    try {
      if (Files.exists(INVENTORY_FILE_PATH)) {
        String json = new String(Files.readAllBytes(INVENTORY_FILE_PATH));

        // Deserialize the JSON into a raw structure
        Type inventoryType = new TypeToken<Map<String, List<Map<String, String>>>>(){}.getType();
        Map<String, List<Map<String, String>>> rawInventory = gson.fromJson(json, inventoryType);

        // converting the rawInventory into Inventory structure
        Inventory loadedInventory = new Inventory();
        for (String category : rawInventory.keySet()) {

          List<Map<String, String>> items = rawInventory.get(category);
          for (Map<String, String> item : items) {

            String name = item.get("name");
            String quantity = item.get("quantity");
            loadedInventory.addItem(new Grocery(name, quantity), category);

          }

        }
        return loadedInventory;
      }

    } catch (IOException e) {
      LOGGER.log(Level.WARNING, "Could not read inventory file. ");
      throw new CustomIoException("Error reading file: " + INVENTORY_FILE_PATH, e);

    }


    return new Inventory(); // empty inventory if file does not exist or an error occurs
  }




  /**
   * Saves the current state of the inventory to the storage file.
   * Serializes the inventory data into JSON format and writes it to the file system.
   */
  public void saveInventory() {
    // converting the Inventory object back to the JSON structure:
    Map<String, List<Map<String, String>>> rawInventory = new HashMap<>();
    for (String category : inventory.getCategories()) {       // iterating over categories
      List<Grocery> groceries = inventory.getGroceriesForCategory(category);
      List<Map<String, String>> itemList = new ArrayList<>();

      for (Grocery grocery : groceries) {    // converting Grocery objects to Map<String, String>
        Map<String, String> itemData = new HashMap<>();
        itemData.put("name", grocery.getGroceryItemName());
        itemData.put("quantity", grocery.getQuantity());
        itemList.add(itemData);
      }

      rawInventory.put(category, itemList); // adding the category and its items to the rawInventory
    }

    // serializing the rawInventory to JSON and writing it to the file
    String json =
        gson.toJson(rawInventory,
            new TypeToken<Map<String, List<Map<String, String>>>>(){}.getType()
        );


    try {
      Files.write(INVENTORY_FILE_PATH, json.getBytes());
    } catch (IOException e) {
      LOGGER.log(Level.WARNING, "Could not save inventory to file.");
      throw new CustomIoException("Failed to save inventory: " + INVENTORY_FILE_PATH, e);
    }
  }





  /**
   * Creates an empty inventory file.
   * Initializes the inventory file with empty lists for each predefined category.
   *
   * @throws IOException if there is an error writing to the file
   */
  private void createEmptyInventoryFile() throws IOException {
    Map<String, List<Map<String, String>>> emptyInventoryStructure = new HashMap<>();
    // adding empty lists for each category
    for (String category : CATEGORIES) {
      emptyInventoryStructure.put(category, new ArrayList<>());
    }

    // serializing the empty inventory structure to JSON and writing it to the file:
    String json =
        gson.toJson(
            emptyInventoryStructure,
            new TypeToken<Map<String, List<Map<String, String>>>>(){}.getType()
        );

    Files.write(INVENTORY_FILE_PATH, json.getBytes()); // writing the JSON to the file
  }





  /**
   * Returns the current inventory.
   *
   * @return Inventory the current inventory object
   */
  public Inventory getInventory() {
    return inventory;
  }





  /**
   * Provides the list of categories for which inventory can be tracked.
   * Returns the default categories if no categories are currently available in the inventory.
   *
   * @return List<String> a list of inventory categories
   */
  public List<String> getCategories() {
    return inventory
        .getCategories()
        .isEmpty() ? Arrays.asList(CATEGORIES) : inventory.getCategories();
  }

}
