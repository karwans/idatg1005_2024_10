package idatg1005.gruppe10.matvettapp.commonutilities.datastorage;

import idatg1005.gruppe10.matvettapp.backend.food.Recipe;
import idatg1005.gruppe10.matvettapp.backend.managerclasses.RecipeManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.lang.reflect.Type;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import idatg1005.gruppe10.matvettapp.commonutilities.errorhandler.CustomExceptionUtility.CustomIoException;


/**
 * Manages the storage and retrieval of recipe-related data,
 * specifically favorites and recently viewed recipes.
 * This class leverages JSON for serialization and deserialization of recipe data,
 * and stores this data within the user's home directory under `.MatVett/data-storage`.
 */
public class RecipeDataStorage {


  // Logger for debugging and error messages:
  private static final Logger LOGGER =
      Logger.getLogger("idatg1005.gruppe10.matvettapp.datastorage.utilities.RecipeDataStorage");



  // The path to the user's home directory
  private static final String USER_HOME = System.getProperty("user.home");
  private static final Path DATA_STORAGE_DIR = Paths.get(USER_HOME, ".MatVett", "datastorage");
  private static final Path FAVORITES_FILE_PATH = DATA_STORAGE_DIR.resolve("favorites.json");
  private static final Path RECENTLY_VIEWED_FILE_PATH =
      DATA_STORAGE_DIR.resolve("recentlyViewed.json");



  // Gson is a library for converting Java objects to and from JSON format
  private final Gson gson = new Gson();
  private final RecipeManager recipeManager;




  /**
   * Constructor that initializes a new RecipeDataStorage object with a specified RecipeManager.
   * Ensures the necessary directories exist for storing recipe data files.
   *
   * @param recipeManager A RecipeManager object to manage recipe data.
   */
  public RecipeDataStorage(RecipeManager recipeManager) {
    this.recipeManager = recipeManager;

    try {
      Files.createDirectories(DATA_STORAGE_DIR);

    } catch (IOException e) {
      LOGGER.log(Level.WARNING, "Failed to create data storage directories: ");
      throw new CustomIoException("Failed to create data storage directories: ", e);
    }

  }





  /**
   * Saves a set of favorite recipes to the storage.
   * Serializes the set of recipe names and writes this data to the 'favorites.json' file.
   *
   * @param favorites A set of Recipe objects to be saved as favorites.
   */
  public void saveFavorites(Set<Recipe> favorites) {
    // extracting the recipe names from the Recipe objects
    Set<String> favoriteNames = favorites.stream()
        .map(Recipe::getRecipeName)
        .collect(Collectors.toSet());

    String json = gson.toJson(favoriteNames);

    try {
      Files.write(FAVORITES_FILE_PATH, json.getBytes());  // writing the JSON data to the file
    } catch (IOException e) {
      LOGGER.log(Level.WARNING, "Could not save 'Favorites' to file.");
      throw new CustomIoException("Failed to save inventory: " + FAVORITES_FILE_PATH, e);
    }
  }






  /**
   * Loads the set of favorite recipes from storage.
   * Deserializes the stored JSON data into recipe names and
   * constructs Recipe objects from these names.
   *
   * @return Set<Recipe> A set of Recipe objects that are marked as favorites.
   */
  public Set<Recipe> loadFavorites() {
    try {


      if (Files.exists(FAVORITES_FILE_PATH)) {   // check if the file exists
        // reading the file contents
        String json = new String(Files.readAllBytes(FAVORITES_FILE_PATH));
        Type setType = new TypeToken<Set<String>>(){}.getType();  // type token for deserialization
        Set<String> favoriteNames = gson.fromJson(json, setType);  // deserialize the JSON data

        // map the recipe names to Recipe objects:
        return favoriteNames.stream()
            .map(recipeManager::getRecipeByName)
            .filter(Objects::nonNull)
            .collect(Collectors.toSet());
      }


    } catch (IOException e) {
      LOGGER.log(Level.WARNING, "Could not load 'Favorites' file. ");
      throw new CustomIoException("Error reading file: " + FAVORITES_FILE_PATH, e);
    }
    return new HashSet<>();
  }






  /**
   * Removes a specific recipe from the list of favorites.
   *
   * @param recipeName The name of the recipe to be removed from favorites.
   * @throws CustomIoException If an error occurs while saving the updated favorites.
   */
  public void removeRecipeFromFavorites(String recipeName) {
    Set<Recipe> favorites = loadFavorites();  // Load existing favorites
    boolean isRemoved = favorites.removeIf(recipe -> recipe.getRecipeName().equals(recipeName));

    LOGGER.log(Level.INFO, "Attempting to remove recipe. Success: {0}", isRemoved);

    if (isRemoved) {
      LOGGER.log(Level.INFO, "Saving updated favorites after removal.");
      saveFavorites(favorites);  // Save the updated favorites back to the file
    } else {
      LOGGER.log(Level.WARNING, "No recipe found to remove.");
    }
  }







  /**
   * Saves the names of recently viewed recipes.
   * Serializes a set of recipe names and stores this data in 'recentlyViewed.json'.
   *
   * @param recentlyViewed A set of recipe names that have been recently viewed.
   */
  public void saveRecentlyViewed(Set<String> recentlyViewed) {
    String json = gson.toJson(recentlyViewed);
    try {
      Files.write(RECENTLY_VIEWED_FILE_PATH, json.getBytes());
    } catch (IOException e) {
      LOGGER.log(Level.WARNING, "Could not save 'Recently Viewed' to file. ");
      throw new CustomIoException("Error saving file: " + RECENTLY_VIEWED_FILE_PATH, e);
    }
  }





  /**
   * Loads the set of recently viewed recipes from storage.
   * Deserializes the stored JSON data to retrieve a set of recipe names.
   * @return Set<String> A set of recipe names that have been recently viewed.
   */
  public Set<String> loadRecentlyViewed() {
    try {
      if (Files.exists(RECENTLY_VIEWED_FILE_PATH)) {
        String json = new String(Files.readAllBytes(RECENTLY_VIEWED_FILE_PATH));
        Type setType = new TypeToken<Set<String>>(){}.getType();
        return gson.fromJson(json, setType);
      }
    } catch (IOException e) {
      LOGGER.log(Level.WARNING, "Could not load 'Recently Viewed' file. ");
      throw new CustomIoException("Error loading/reading file: " + RECENTLY_VIEWED_FILE_PATH, e);
    }
    return new HashSet<>();
  }

}
