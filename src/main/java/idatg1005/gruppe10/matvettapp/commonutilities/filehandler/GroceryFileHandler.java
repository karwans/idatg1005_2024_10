package idatg1005.gruppe10.matvettapp.commonutilities.filehandler;

import idatg1005.gruppe10.matvettapp.backend.food.Grocery;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import idatg1005.gruppe10.matvettapp.commonutilities.errorhandler.CustomExceptionUtility.CustomIoException;


/**
 * Handles loading of grocery data from a structured text file into a categorized map.
 * This class reads a file specified by a file path and constructs a map where each category of
 * groceries is a key, and each key holds a list of Grocery objects. It uses standard Java logging
 * to report errors during file operations.
 */
public class GroceryFileHandler {

  private static final Logger LOGGER =
      Logger.getLogger("idatg1005.gruppe10.matvettapp.managerclasses.backend.GroceryManager");



  /**
   * Loads groceries categorized by sections from a specified file.
   * The file is expected to have categories prefixed with "-"
   * and groceries listed under these categories.
   * Each grocery item is assumed to have a default quantity of "1".
   *
   * @param filePath The path to the file containing the grocery data.
   * @return Map<String, List<Grocery>>
   *   A map with categories as keys and lists of groceries as values.
   * @throws CustomIoException if the file cannot be read,
   *                           encapsulating the underlying IOException.
   */
   public Map<String, List<Grocery>> loadCategoriesWithItems(String filePath) {
    // a map to store the categories and their corresponding groceries
    Map<String, List<Grocery>> categorizedGroceries = new HashMap<>();

    // try-with-resources block to read the file with BufferedReader
    try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
      String line;                    // variable to store the current line being read
      String currentCategory = null;  // variable to store the current category being read

      while ((line = br.readLine()) != null) {
        if (line.startsWith("-")) {  // checking if the line is a category indicator
          // Adjust to account for the colon and possible space after the category name
          currentCategory = line.substring(1).trim(); // Remove dash
          if (currentCategory.endsWith(":")) {
            currentCategory = currentCategory.substring(0, currentCategory.length() - 1).trim(); // Remove colon
          }

          // creating a new list for this category
          categorizedGroceries.put(currentCategory, new ArrayList<>());
        } else if (!line.isEmpty() && currentCategory != null) {
          Grocery item = new Grocery(line.trim(), "1"); // default quantity of "1"
          // adding the item to the current category's list
          categorizedGroceries.get(currentCategory).add(item);
        }
      }
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, "Failed to load groceries from file: {0}", filePath);
      throw new CustomIoException("Failed to read file", e);
    }

    // returning the map, which will be empty if the file is empty or has no valid data
    return categorizedGroceries;
  }




}
