package idatg1005.gruppe10.matvettapp.commonutilities.filehandler;

import idatg1005.gruppe10.matvettapp.backend.food.Recipe;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import idatg1005.gruppe10.matvettapp.commonutilities.errorhandler.CustomExceptionUtility.CustomIoException;



/**
 * Facilitates reading and writing of recipe data to and from text files.
 * This class provides methods to handle recipe files within a specified directory, including
 * loading all recipes and saving individual recipes to file.
 * It uses a functional interface to allow custom processing of each loaded recipe.
 */
public class RecipeFileHandler {

  private static final Logger LOGGER =
      Logger.getLogger("idatg1005.gruppe10.matvettapp.managerclasses.backend.RecipeManager");




  /**
   * A functional interface for processing recipes.
   */
  @FunctionalInterface
  public interface RecipeHandler {
    void handle(Recipe recipe);
  }




  /**
   * Loads all recipes from text files located in a specified directory
   * and processes them using a provided handler.
   *
   * @param directoryPath The path to the directory containing recipe text files.
   * @param recipeHandler A RecipeHandler to process each loaded recipe.
   * @throws CustomIoException if there is an issue accessing the directory or reading files.
   */
  public static void loadAllRecipes(String directoryPath, RecipeHandler recipeHandler) {
    try (Stream<Path> paths = Files.walk(Paths.get(directoryPath))) {
      paths.filter(Files::isRegularFile)                     // filter out directories
          .filter(path -> path.toString().endsWith(".txt"))  // filter out non-text files
          .forEach(path -> {                                 // process each file
            try {
              Recipe recipe = readRecipeFromFile(path.toString());
              if (recipe != null) {
                recipeHandler.handle(recipe);
              }
            } catch (CustomIoException e) {
              LOGGER.log(Level.SEVERE, "Error processing file: {0}", path);
            }
          });
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, "Failed to load recipes from directory: {0}", directoryPath);
      throw new CustomIoException("Failed to load recipes from directory: " + directoryPath, e);
    }

  }

  /**
   * Reads a single recipe from a file.
   *
   * @param filePath Path to the recipe file.
   * @return Recipe The loaded recipe object.
   * @throws CustomIoException if the file cannot be read.
   */
  public static Recipe readRecipeFromFile(String filePath) throws CustomIoException {
    List<String> lines = readFileLines(filePath);  // read all lines from the file
    return parseRecipe(lines);
  }





  /**
   * Reads all lines from a recipe file.
   *
   * @param filePath Path to the file.
   * @return List<String> All lines from the file.
   * @throws CustomIoException if the file cannot be read.
   */
  public static List<String> readFileLines(String filePath) throws CustomIoException {
    List<String> lines = new ArrayList<>();  // list to store all lines from the file
    try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
      String line;
      while ((line = br.readLine()) != null) {
        lines.add(line);  // add each line to the list
      }
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, () -> "Error reading file: " + filePath);
      throw new CustomIoException("Error reading file: " + filePath, e);
    }
    return lines;
  }





  /**
   * Parses a list of strings into a Recipe object.
   *
   * @param lines Lines representing components of a recipe.
   * @return Recipe The parsed Recipe object, or null if required data is missing.
   */
  public static Recipe parseRecipe(List<String> lines) {
    // Initialize variables to store recipe data
    String name = "";
    String category = "";
    String imagePath = "";
    String prepTime = "";
    String cookTime = "";
    // List to store ingredient descriptions
    List<String> ingredientDescriptions = new ArrayList<>();
    StringBuilder instructionsBuilder = new StringBuilder(); // StringBuilder to store instructions

    Iterator<String> lineIterator = lines.iterator();  // iterator to traverse the lines
    while (lineIterator.hasNext()) {     // iterate through the lines and parse the data
      String line = lineIterator.next().trim();   // getting the next line and trimming it
      if (line.startsWith("Name:")) {
        name = line.substring(5).trim();
      } else if (line.startsWith("Category:")) {
        category = line.substring(9).trim();
      } else if (line.startsWith("ImagePath:")) {
        imagePath = line.substring(10).trim();
      } else if (line.startsWith("PrepTime:")) {
        prepTime = line.substring(9).trim();
      } else if (line.startsWith("CookTime:")) {
        cookTime = line.substring(9).trim();
      } else if (line.startsWith("Ingredients:")) {

        // Read ingredient descriptions until the next section starts
        while (lineIterator.hasNext() && !(line = lineIterator.next().trim()).startsWith("Instructions:")) {
          ingredientDescriptions.add(line);  // add each ingredient description to the list
        }
        instructionsBuilder.append(line.substring(13).trim());
      } else if (!line.isEmpty()) {
        instructionsBuilder.append(line).append("\n");
      }
    }

    // If the name and instructions are not empty, create a new Recipe object
    if (!name.isEmpty() && !instructionsBuilder.toString().isEmpty()) {
      Recipe recipe =
          new Recipe(
              name, category, prepTime, cookTime, imagePath, instructionsBuilder.toString().trim());
      recipe.setIngredientDescriptions(ingredientDescriptions);
      return recipe;
    }
    return null;
  }




  /**
   * Saves a recipe to a file specified by the file path.
   *
   * @param recipe   The recipe to save.
   * @param filePath The file path where the recipe should be saved.
   * @throws CustomIoException if there is an issue saving the recipe to the file.
   */
  public static void saveRecipeToFile(Recipe recipe, String filePath) {
    Path path = Paths.get(filePath);
    try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(path))) {
      writer.println("Name: " + recipe.getRecipeName());
      writer.println("Category: " + recipe.getCategory());
      writer.println("ImagePath: " + recipe.getImagePath());
      writer.println("PrepTime: " + recipe.getPrepTime());
      writer.println("CookTime: " + recipe.getCookTime());
      writer.println("Ingredients:");
      recipe.getIngredientDescriptions().forEach(ingredient -> writer.println("- " + ingredient));
      writer.println("Instructions:");
      writer.println(recipe.getInstructions());
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, "Error saving recipe to file: {0}", filePath);
      throw new CustomIoException("Error saving recipe to file: " + filePath, e);
    }
  }
}