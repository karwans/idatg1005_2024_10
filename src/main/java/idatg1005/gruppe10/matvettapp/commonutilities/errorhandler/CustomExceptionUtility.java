package idatg1005.gruppe10.matvettapp.commonutilities.errorhandler;


/**
 * Provides a collection of custom exceptions to handle various types of errors
 * throughout the application.
 * This utility class defines specialized exceptions for I/O operations,
 * database access, and configuration issues.
 */
public class CustomExceptionUtility {


  CustomExceptionUtility() {
    // This constructor is intentionally empty. Nothing special is needed here.
  }


  /**
   * Static inner class for handling custom I/O exceptions.
   */
  public static class CustomIoException extends RuntimeException {


    /**
     * Custom unchecked exception for handling I/O related errors.
     * This exception is thrown when an I/O operation fails,
     * indicating that the error should be handled at runtime.
     *
     * @param message Detailed message about the error cause.
     * @param cause The underlying cause of the exception.
     */
    public CustomIoException(String message, Throwable cause) {
      super(message, cause);
    }


  }



}