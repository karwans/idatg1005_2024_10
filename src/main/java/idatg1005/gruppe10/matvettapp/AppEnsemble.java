package idatg1005.gruppe10.matvettapp;

import idatg1005.gruppe10.matvettapp.backend.managerclasses.RecipeManager;
import idatg1005.gruppe10.matvettapp.backend.storageclasses.Inventory;
import idatg1005.gruppe10.matvettapp.backend.storageclasses.ShoppingList;
import idatg1005.gruppe10.matvettapp.frontend.controllers.HomeController;
import idatg1005.gruppe10.matvettapp.frontend.controllers.InventoryController;
import idatg1005.gruppe10.matvettapp.frontend.controllers.ShoppingListController;
import idatg1005.gruppe10.matvettapp.frontend.guiclasses.HomeGui;
import idatg1005.gruppe10.matvettapp.frontend.guiclasses.InventoryGui;
import idatg1005.gruppe10.matvettapp.frontend.guiclasses.RecipeGui;
import idatg1005.gruppe10.matvettapp.frontend.guiclasses.ShoppingListGui;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import idatg1005.gruppe10.matvettapp.commonutilities.datastorage.ShoppingListDataStorage;


/**
 * Main application class for the MatVett system.
 * Sets up the GUI and initializes the data layers.
 * Manages the lifecycle of the application and
 * orchestrates the interaction between the GUI components and data management.
 */
public class AppEnsemble extends Application {

  private TabPane tabPane;
  private RecipeManager recipeManager;
  private ShoppingListDataStorage shoppingListDataStorage;
  private ShoppingListController shoppingListController;


  private static final String BACKGROUND_IMAGE_PATH = "/images/home.png";
  private static final String CSS_STYLESHEET_PATH = "/css/home-style.css";


  /**
   * Initializes and starts the primary stage of the application.
   * Sets up tabs, initializes data managers, and loads necessary data.
   *
   * @param primaryStage The primary stage for this application.
   */
  @Override
  public void start(Stage primaryStage) {
    tabPane = new TabPane();
    String recipeDirectory = "src/main/resources/recipes";
    shoppingListDataStorage = new ShoppingListDataStorage();

    // Initialize the managers
    recipeManager = new RecipeManager();
    recipeManager.loadRecipesFromDirectory(recipeDirectory);
    loadData(); // Load favorites and recently viewed recipes

    // Shared shopping list:
    ShoppingList sharedShoppingList = shoppingListDataStorage.loadShoppingList();
    shoppingListController = new ShoppingListController(sharedShoppingList);
    ShoppingListGui shoppingListGui =
        new ShoppingListGui(sharedShoppingList, shoppingListController);

    // Recipes GUI and controller:
    RecipeGui recipeGui = new RecipeGui(recipeManager, sharedShoppingList, shoppingListController);
    recipeGui.refreshDataDisplay();  // Refresh the GUI after data is loaded

    // Create tabs for the GUI components:
    createTabs(shoppingListGui, recipeGui);

    // Set up the primary stage and show the application window.
    Scene scene = new Scene(tabPane, 1200, 600);
    primaryStage.setScene(scene);
    primaryStage.setTitle("MatVett");
    primaryStage.show();
  }




  /**
   * Loads favorites and recently viewed recipes, and shopping lists.
   */
  private void loadData() {
    recipeManager.loadFavorites();
    recipeManager.loadRecentlyViewed();
    shoppingListDataStorage.loadShoppingList();
  }




  /**
   * Creates and configures tabs for the application's GUI.
   *
   * @param shoppingListGui GUI component for managing shopping lists.
   * @param recipeGui GUI component for managing recipes.
   */
  private void createTabs(ShoppingListGui shoppingListGui, RecipeGui recipeGui) {
    // Shopping List Tab:
    ImageView shoppingListIcon =
        new ImageView(
            new Image(
                "/images/button-icons/shopping-list-page.png", 25, 25, true, true));
    Tab shoppingListTab = new Tab("Shopping List", shoppingListGui);
    shoppingListTab.setGraphic(shoppingListIcon);
    shoppingListTab.setClosable(false);

    // Recipes Tab:
    ImageView recipeIcon = new ImageView(
        new Image(
            "/images/button-icons/recipe-page.png", 25, 25, true, true));
    Tab recipeTab = new Tab("Recipes", recipeGui);
    recipeTab.setGraphic(recipeIcon);
    recipeTab.setClosable(false);

    // Inventory Tab:
    Inventory inventory = new Inventory();
    InventoryController inventoryController = new InventoryController(inventory);
    InventoryGui inventoryGui = new InventoryGui(inventory, inventoryController);

    // Set the GUI for the controller, which will also trigger the updates
    inventoryController.setInventoryGui(inventoryGui);

    ImageView inventoryIcon =
        new ImageView(
            new Image(
                "/images/button-icons/inventory-page.png", 25, 25, true, true));
    Tab inventoryTab = new Tab("Inventory", inventoryGui);
    inventoryTab.setGraphic(inventoryIcon);
    inventoryTab.setClosable(false);

    // Home Tab:
    HomeGui homeGui = new HomeGui(BACKGROUND_IMAGE_PATH, CSS_STYLESHEET_PATH);
    HomeController homeController = new HomeController(homeGui);
    ImageView homeIcon = new ImageView(
        new Image(
            "/images/button-icons/home-page.png", 25, 25, true, true));
    Tab homeTab = new Tab("Home", homeGui);
    homeTab.setGraphic(homeIcon);
    homeTab.setClosable(false);
    homeController.setOnTabSwitchRequested(this::switchTab);

    // Adding all initialized tabs to the TabPane:
    tabPane.getTabs().addAll(homeTab, recipeTab, shoppingListTab, inventoryTab);
  }




  /**
   * Switches to a specified tab when requested.
   *
   * @param tabName The name of the tab to switch to.
   */
  private void switchTab(String tabName) {
    for (Tab tab : tabPane.getTabs()) {
      if (tab.getText().equalsIgnoreCase(tabName)) {
        tabPane.getSelectionModel().select(tab);
        break;
      }
    }
  }





  /**
   * Saves all necessary data before the application stops.
   * Persists favorites, recently viewed recipes, and the current shopping list.
   */
  @Override
  public void stop() {
    recipeManager.saveFavorites();
    recipeManager.saveRecentlyViewed();
    shoppingListDataStorage.saveShoppingList(shoppingListController.getShoppingList());
  }



  /**
   * Main method to launch the application.
   *
   * @param args Command-line arguments.
   */
  public static void main(String[] args) {
    launch(args);
  }


}
