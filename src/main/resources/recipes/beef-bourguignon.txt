Name: Beef Bourguignon
Category: Meat
ImagePath: /images/recipe-images/beef-bourguignon.png
PrepTime: 20 minutes
CookTime: 3 hours

Ingredients:
- Beef Chuck (cut into 2-inch pieces): 1 kg
- Bacon (chopped): 100g
- Carrots (sliced): 3
- Onions (chopped): 2
- Garlic (minced): 3 cloves
- Red Wine: 750 ml
- Beef Broth: 2 cups
- Tomato Paste: 2 tbsp
- Fresh Thyme: 2 sprigs
- Bay Leaves: 2
- Salt and Pepper: to taste
- Button Mushrooms (quartered): 300g

Instructions:
- Preheat oven to 325°F (160°C).
- In a large Dutch oven, cook bacon over medium heat until crisp. Remove bacon and set aside. Brown the beef pieces in the bacon fat in batches; set aside with bacon.
- In the same pot, add onions and carrots and cook until onions are translucent. Add garlic and cook for 1 minute.
- Return the bacon and beef to the pot. Stir in red wine, beef broth, tomato paste, thyme, and bay leaves. Season with salt and pepper.
- Bring to a simmer, cover, and transfer to the oven. Cook for about 2.5 hours, or until the beef is tender.
- About 30 minutes before the stew is ready, sauté mushrooms in a bit of butter and add them to the stew.
- Remove bay leaves and thyme sprigs, adjust seasoning, and serve hot.