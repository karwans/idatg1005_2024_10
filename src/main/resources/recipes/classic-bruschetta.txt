Name: Classic Bruschetta
Category: Appetizer
ImagePath: /images/recipe-images/classic-bruschetta.png
PrepTime: 10 minutes
CookTime: 5 minutes

Ingredients:

- Ripe Tomatoes (diced): 3
- Garlic Clove (minced): 1
- Fresh Basil Leaves (chopped): 10
- Olive Oil: 2 tbsp
- Balsamic Vinegar: 1 tbsp
- Salt and Pepper: to taste
- Baguette (sliced and toasted): 1

Instructions:
- In a mixing bowl, combine diced tomatoes, minced garlic, chopped basil, olive oil, and balsamic vinegar. Season with salt and pepper to taste.
- Let the mixture sit for about 10 minutes to allow flavors to meld.
- Spoon the tomato mixture generously onto the toasted baguette slices just before serving.
- Serve immediately to keep the bread from getting soggy.