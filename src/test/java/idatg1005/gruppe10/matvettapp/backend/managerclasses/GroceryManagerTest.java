package idatg1005.gruppe10.matvettapp.backend.managerclasses;

import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import idatg1005.gruppe10.matvettapp.commonutilities.filehandler.GroceryFileHandler;

import java.util.NoSuchElementException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


/**
 * Test cases for the GroceryManager class.
 * Using Mockito to mock the file functionality.
 */
class GroceryManagerTest {

  private GroceryManager groceryManager;
  private GroceryFileHandler fileHandlerMock;
  private final String testFilePath = "test/path/to/file.txt";





  @BeforeEach
  void setUp() {
    fileHandlerMock = mock(GroceryFileHandler.class);
    groceryManager = new GroceryManager(testFilePath, fileHandlerMock);
  }






  // Positive and negative test cases for loadCategoriesWithItems method
  @Test
  void loadCategoriesWithItems_ShouldLoadCategories_WhenFileExists() {
    Map<String, List<Grocery>> expected = new HashMap<>();
    expected.put("Fruits", Arrays.asList(new Grocery("Apple", "1"), new Grocery("Banana", "2")));
    when(fileHandlerMock.loadCategoriesWithItems(testFilePath)).thenReturn(expected);

    groceryManager.loadCategoriesWithItems();
    assertEquals(2, groceryManager.getGroceriesForCategory("Fruits").size());
    assertTrue(groceryManager.getCategories().contains("Fruits"));
  }


  @Test
  void loadCategoriesWithItems_ShouldHandleNullCategoriesGracefully() {
    when(fileHandlerMock.loadCategoriesWithItems(testFilePath)).thenReturn(null);

    assertThrows(NullPointerException.class, () -> {
      groceryManager.loadCategoriesWithItems();
    });
  }





  // Positive and negative test cases for getCategories and getGroceriesForCategory methods:
  @Test
  void getCategories_ShouldReturnAllCategories() {
    Map<String, List<Grocery>> categories = new HashMap<>();
    categories.put("Vegetables", List.of(new Grocery("Carrot", "3")));
    categories.put("Dairy", List.of(new Grocery("Milk", "1")));
    when(fileHandlerMock.loadCategoriesWithItems(any())).thenReturn(categories);

    groceryManager.loadCategoriesWithItems();
    List<String> retrievedCategories = groceryManager.getCategories();

    assertEquals(2, retrievedCategories.size());
    assertTrue(retrievedCategories.contains("Vegetables"));
    assertTrue(retrievedCategories.contains("Dairy"));
  }






  @Test
  void getGroceriesForCategory_ShouldThrowException_WhenCategoryDoesNotExist() {
    Map<String, List<Grocery>> categories = new HashMap<>();
    categories.put("Snacks", List.of(new Grocery("Chips", "5")));
    when(fileHandlerMock.loadCategoriesWithItems(any())).thenReturn(categories);

    groceryManager.loadCategoriesWithItems();

    assertThrows(NoSuchElementException.class, () -> {
      groceryManager.getGroceriesForCategory("NonExistent");
    });
  }





  @Test
  void getGroceriesForCategory_ShouldReturnGroceries_WhenCategoryExists() {
    Map<String, List<Grocery>> categories = new HashMap<>();
    List<Grocery> expectedGroceries = List.of(new Grocery("Cheese", "2"));
    categories.put("Dairy", expectedGroceries);
    when(fileHandlerMock.loadCategoriesWithItems(any())).thenReturn(categories);

    groceryManager.loadCategoriesWithItems();
    List<Grocery> groceries = groceryManager.getGroceriesForCategory("Dairy");

    assertFalse(groceries.isEmpty());
    assertEquals("Cheese", groceries.getFirst().getGroceryItemName());
    assertEquals("2", groceries.getFirst().getQuantity());
  }






  @Test
  void getGroceriesForCategory_ShouldHandleNullCategoriesGracefully() {
    when(fileHandlerMock.loadCategoriesWithItems(any())).thenReturn(null);

    assertThrows(NullPointerException.class, () -> {
      groceryManager.loadCategoriesWithItems();
    });
  }


}
