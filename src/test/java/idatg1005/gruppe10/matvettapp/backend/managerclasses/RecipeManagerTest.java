package idatg1005.gruppe10.matvettapp.backend.managerclasses;

import idatg1005.gruppe10.matvettapp.backend.food.Recipe;
import java.util.Collection;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import idatg1005.gruppe10.matvettapp.commonutilities.datastorage.RecipeDataStorage;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class RecipeManagerTest {

  private RecipeManager recipeManager;


  @BeforeEach
  void setUp() {
    RecipeDataStorage mockStorage = mock(RecipeDataStorage.class);
    recipeManager = new RecipeManager();
  }






  // Positive and negative test case for addRecipe method:
  @Test
  void addRecipe_ShouldAddRecipe_WhenRecipeIsValid() {
    Recipe validRecipe = new Recipe("Pasta", "Main", "10 min", "15 min", "/images/pasta.jpg", "Boil pasta");
    recipeManager.addRecipe(validRecipe);
    assertEquals(validRecipe, recipeManager.getRecipeByName("Pasta"));
  }

  @Test
  void addRecipe_ShouldThrowException_WhenRecipeIsNull() {
    assertThrows(IllegalArgumentException.class, () -> recipeManager.addRecipe(null));
  }






  // Positive and negative test case for removeRecipe method:
  @Test
  void addRecipeToFavorites_ShouldAddToFavorites_WhenRecipeIsValid() {
    Recipe favoriteRecipe = new Recipe("Salad", "Side", "5 min", "0 min", "/images/salad.jpg", "Mix ingredients");
    recipeManager.addRecipeToFavorites(favoriteRecipe);
    assertTrue(recipeManager.getFavoriteRecipes().contains(favoriteRecipe));
  }

  @Test
  void addRecipeToFavorites_ShouldThrowException_WhenRecipeIsNull() {
    assertThrows(IllegalArgumentException.class, () -> recipeManager.addRecipeToFavorites(null));
  }






  // Positive and negative test case for removeRecipeFromFavorites method:
  @Test
  void removeRecipeFromFavorites_ShouldRemoveFromFavorites_WhenRecipeIsInFavorites() {
    Recipe favoriteRecipe = new Recipe("Soup", "Appetizer", "10 min", "20 min", "/images/soup.jpg", "Cook ingredients");
    recipeManager.addRecipeToFavorites(favoriteRecipe);
    recipeManager.removeRecipeFromFavorites(favoriteRecipe);
    assertFalse(recipeManager.getFavoriteRecipes().contains(favoriteRecipe));
  }

  @Test
  void removeRecipeFromFavorites_ShouldThrowException_WhenRecipeIsNotInFavorites() {
    Recipe nonFavoriteRecipe = new Recipe("Burger", "Main", "5 min", "10 min", "/images/burger.jpg", "Grill burger");
    assertThrows(IllegalArgumentException.class, () -> recipeManager.removeRecipeFromFavorites(nonFavoriteRecipe));
  }






  // Positive and negative test case for addRecipeToRecentlyViewed method:
  @Test
  void addRecipeToRecentlyViewed_ShouldAddToRecentlyViewed_WhenNameIsValid() {
    recipeManager.addRecipeToRecentlyViewed("Ice Cream");
    assertTrue(recipeManager.getRecentlyViewed().contains("Ice Cream"));
  }

  @Test
  void addRecipeToRecentlyViewed_ShouldThrowException_WhenNameIsEmpty() {
    assertThrows(IllegalArgumentException.class, () -> recipeManager.addRecipeToRecentlyViewed(""));
  }






  // Positive and negative test case for getRecipeByName method:
  @Test
  void getRecipesByCategory_ShouldReturnRecipesInCategory_WhenCategoryIsValid() {
    Recipe dessert = new Recipe("Cake", "Dessert", "15 min", "30 min", "/images/cake.jpg", "Bake cake");
    recipeManager.addRecipe(dessert);
    Collection<Recipe> results = recipeManager.getRecipesByCategory("Dessert");
    assertTrue(results.contains(dessert));
  }


  @Test
  void getRecipesByCategory_ShouldReturnEmptyCollection_WhenCategoryDoesNotExist() {
    assertTrue(recipeManager.getRecipesByCategory("NonExistent").isEmpty());
  }
}
