package idatg1005.gruppe10.matvettapp.backend.food;

import static org.junit.jupiter.api.Assertions.*;

import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for Grocery.
 * Tests both positive and negative scenarios for each method to ensure robustness and correctness.
 */
class GroceryTest {
  private Grocery grocery;


  // Initialization of a grocery item with valid parameters before each test
  @BeforeEach
  void setUp() {
    grocery = new Grocery("Apples", "10 kg");
  }




  // ----------------------- Tests for the constructor:
  @Test
  void testConstructorPositive() {
    assertAll(
        () -> assertEquals("Apples", grocery.getGroceryItemName()),
        () -> assertEquals("10 kg", grocery.getQuantity())
    );
  }

  @Test
  void testConstructorNegative() {
    assertThrows(IllegalArgumentException.class, () -> new Grocery(null, "10 kg"));
    assertThrows(IllegalArgumentException.class, () -> new Grocery("Apples", null));
  }




  // ----------------------- Tests for the setter methods:
  @Test
  void testSetGroceryNamePositive() {
    grocery.setGroceryName("Bananas");
    assertEquals("Bananas", grocery.getGroceryItemName());
  }

  @Test
  void testSetGroceryNameNegative() {
    assertThrows(IllegalArgumentException.class, () -> grocery.setGroceryName(null));
  }

  @Test
  void testSetQuantityPositive() {
    grocery.setQuantity("5 kg");
    assertEquals("5 kg", grocery.getQuantity());
  }

  @Test
  void testSetQuantityNegative() {
    assertThrows(IllegalArgumentException.class, () -> grocery.setQuantity(null));
  }

  @Test
  void testSetCategoryPositive() {
    grocery.setCategory("Fruits");
    assertEquals("Fruits", grocery.getCategory().get());
  }

  @Test
  void testSetCategoryNegative() {
    assertThrows(IllegalArgumentException.class, () -> grocery.setCategory(""));
    assertThrows(IllegalArgumentException.class, () -> grocery.setCategory(null));
  }

  @Test
  void testSelectedProperty() {
    assertFalse(grocery.selectedProperty().get());
    grocery.setSelected(true);
    assertTrue(grocery.selectedProperty().get());
  }




  // ----------------------- Tests for the toString, equals, and hashCode methods:
  @Test
  void testToString() {
    assertEquals("Apples: 10 kg", grocery.toString());
  }

  @Test
  void testEqualsAndHashCode() {
    Grocery anotherGrocery = new Grocery("Apples", "10 kg");
    assertEquals(grocery, anotherGrocery);
    assertEquals(grocery.hashCode(), anotherGrocery.hashCode());

    Grocery differentGrocery = new Grocery("Oranges", "5 kg");
    assertNotEquals(grocery, differentGrocery);
  }
}
