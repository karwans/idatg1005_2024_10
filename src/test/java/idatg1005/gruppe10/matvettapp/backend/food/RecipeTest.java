package idatg1005.gruppe10.matvettapp.backend.food;

import static org.junit.jupiter.api.Assertions.*;

import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import idatg1005.gruppe10.matvettapp.backend.food.Recipe;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for the Recipe class.
 * This class tests all functionalities of the Recipe class,
 * including positive and negative test cases
 * to ensure robust handling of edge cases and normal operations.
 */
class RecipeTest {
  private Recipe recipe;




  /**
   * Sets up a Recipe object before each test.
   */
  @BeforeEach
  void setUp() {
    recipe =
        new Recipe(
            "Pasta Carbonara", "Pasta",
            "10 min", "20 min",
            "path/to/image.jpg", "Instructions to cook Pasta Carbonara.");
  }





  /**
   * Tests the constructor of the Recipe class with positive test cases.
   */
  @Test
  void testRecipeConstructorPositive() {
    assertAll(
        () -> assertEquals("Pasta Carbonara", recipe.getRecipeName()),
        () -> assertEquals("Pasta", recipe.getCategory()),
        () -> assertEquals("10 min", recipe.getPrepTime()),
        () -> assertEquals("20 min", recipe.getCookTime()),
        () -> assertEquals("path/to/image.jpg", recipe.getImagePath()),
        () -> assertEquals("Instructions to cook Pasta Carbonara.", recipe.getInstructions())
    );
  }


  /**
   * Tests the constructor of the Recipe class with negative test cases.
   */
  @Test
  void testRecipeConstructorNegative() {
    Exception e = assertThrows(
        IllegalArgumentException.class, () ->
            new Recipe(
                null, "Pasta",
                "10 min", "20 min",
                "path/to/image.jpg", "Instructions")
    );

    assertEquals(
        "Recipe name must contain only alphabets and cannot be null.", e.getMessage());
  }




  // Test cases for the addIngredient method
  @Test
  void testAddIngredientPositive() {
    Grocery ingredient = new Grocery("Bread","Baked Goods");
    recipe.addIngredient(ingredient);
    assertTrue(recipe.getIngredients().contains(ingredient));
  }

  @Test
  void testAddIngredientNegative() {
    assertThrows(IllegalArgumentException.class, () -> recipe.addIngredient(null));
  }




  // Test cases for the setRecipeName method
  @Test
  void testSetRecipeNamePositive() {
    recipe.setRecipeName("New Name");
    assertEquals("New Name", recipe.getRecipeName());
  }

  @Test
  void testSetRecipeNameNegative() {
    assertThrows(IllegalArgumentException.class, () -> recipe.setRecipeName(null));
    assertThrows(IllegalArgumentException.class, () -> recipe.setRecipeName("123"));
  }



  // Tests for the setCategory, setImagePath, setPrepTime, setCookTime, and setInstructions methods
  @Test
  void testSetAndGetCategory() {
    recipe.setCategory("Dessert");
    assertEquals("Dessert", recipe.getCategory());
  }

  @Test
  void testSetAndGetImagePath() {
    recipe.setImagePath("new/path/to/image.jpg");
    assertEquals("new/path/to/image.jpg", recipe.getImagePath());
  }

  @Test
  void testSetAndGetPrepTime() {
    recipe.setPrepTime("15 min");
    assertEquals("15 min", recipe.getPrepTime());
  }

  @Test
  void testSetAndGetCookTime() {
    recipe.setCookTime("25 min");
    assertEquals("25 min", recipe.getCookTime());
  }

  @Test
  void testSetAndGetInstructions() {
    recipe.setInstructions("New Instructions");
    assertEquals("New Instructions", recipe.getInstructions());
  }
}
