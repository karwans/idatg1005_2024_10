package idatg1005.gruppe10.matvettapp.backend.storageclasses;

import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import idatg1005.gruppe10.matvettapp.backend.storageclasses.ShoppingList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class ShoppingListTest {
  private ShoppingList shoppingList;

  @BeforeEach
  void setUp() {
    shoppingList = new ShoppingList();
  }




  // Positive and Negative Test Cases for addGroceryItem method:
  @Test
  void addGroceryItem_ShouldAddItemSuccessfully_WhenItemIsValid() {
    Grocery milk = new Grocery("Milk", "1 liter");
    shoppingList.addGroceryItem(milk);
    assertTrue(shoppingList.getGroceryItems().contains(milk), "Shopping list should contain the added item.");
  }

  @Test
  void addGroceryItem_ShouldThrowIllegalArgumentException_WhenItemIsNull() {
    assertThrows(IllegalArgumentException.class, () -> shoppingList.addGroceryItem(null),
        "Adding a null item should throw IllegalArgumentException.");
  }





  // Positive and Negative Test Cases for removeGroceryItem method:
  @Test
  void removeGroceryItem_ShouldRemoveItemSuccessfully_WhenItemExists() {
    Grocery bread = new Grocery("Bread", "1 loaf");
    shoppingList.addGroceryItem(bread);
    shoppingList.removeGroceryItem(bread);
    assertFalse(shoppingList.getGroceryItems().contains(bread), "Shopping list should not contain the removed item.");
  }

  @Test
  void removeGroceryItem_ShouldThrowIllegalArgumentException_WhenItemIsNull() {
    assertThrows(IllegalArgumentException.class, () -> shoppingList.removeGroceryItem(null),
        "Removing a null item should throw IllegalArgumentException.");
  }





  // Positive and Negative Test Cases for setGroceryItems method:
  @Test
  void setGroceryItems_ShouldSetNewItemList_WhenListIsValid() {
    List<Grocery> newItems = Arrays.asList(new Grocery("Apples", "3 kg"), new Grocery("Oranges", "5 kg"));
    shoppingList.setGroceryItems(newItems);
    assertEquals(newItems, shoppingList.getGroceryItems(), "Shopping list items should match the newly set items.");
  }

  @Test
  void setGroceryItems_ShouldThrowIllegalArgumentException_WhenListIsNull() {
    assertThrows(IllegalArgumentException.class, () -> shoppingList.setGroceryItems(null),
        "Setting a null list should throw IllegalArgumentException.");
  }





  // Positive and Negative Test Cases for replaceItems method:
  @Test
  void replaceItems_ShouldReplaceItemsSuccessfully_WhenListIsValid() {
    shoppingList.addGroceryItem(new Grocery("Milk", "1 liter"));
    List<Grocery> newItems = Arrays.asList(new Grocery("Eggs", "12 pcs"), new Grocery("Cheese", "250g"));
    shoppingList.replaceItems(newItems);
    assertTrue(shoppingList.getGroceryItems().containsAll(newItems) && shoppingList.getGroceryItems().size() == 2,
        "Shopping list should only contain the new items.");
  }


  @Test
  void replaceItems_ShouldThrowIllegalArgumentException_WhenListIsNull() {
    assertThrows(IllegalArgumentException.class, () -> shoppingList.replaceItems(null),
        "Replacing with a null list should throw IllegalArgumentException.");
  }






  // Test Case for getShoppingListItemsIterator method:
  @Test
  void getShoppingListItemsIterator_ShouldIterateOverItemsSuccessfully() {
    Grocery eggs = new Grocery("Eggs", "12 pcs");
    Grocery cheese = new Grocery("Cheese", "250g");
    shoppingList.addGroceryItem(eggs);
    shoppingList.addGroceryItem(cheese);
    Iterator<Grocery> iterator = shoppingList.getShoppingListItemsIterator();
    assertTrue(iterator.hasNext(), "Iterator should have next item.");
    assertEquals(eggs, iterator.next(), "First item should be eggs.");
    assertEquals(cheese, iterator.next(), "Next item should be cheese.");
    assertThrows(NoSuchElementException.class, iterator::next, "Iterator should throw when no more elements.");
  }




  // Test case for getGroceryItems method:
  @Test
  void getGroceryItems_ShouldReturnCopyOfList_ThatIsNotModifiable() {
    shoppingList.addGroceryItem(new Grocery("Milk", "1 liter"));
    List<Grocery> items = shoppingList.getGroceryItems();

    assertThrows(UnsupportedOperationException.class, () ->
            items.add(new Grocery("Eggs", "12 pack")),"The returned list should be unmodifiable.");
  }


}
