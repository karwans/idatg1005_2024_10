package idatg1005.gruppe10.matvettapp.backend.storageclasses;

import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import idatg1005.gruppe10.matvettapp.backend.storageclasses.Inventory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit tests for the Inventory class.
 */
class InventoryTest {
  private Inventory inventory;

  @BeforeEach
  void setUp() {
    inventory = new Inventory();
  }



  // Positive and negative tests for addItem method:
  @Test
  void addItem_ShouldAddItem_WhenCategoryIsValid() {
    Grocery apple = new Grocery("Apple", "1 kg");

    inventory.addItem(apple, "Fruits");

    List<Grocery> fruits = inventory.getGroceriesForCategory("Fruits");

    assertTrue(fruits.contains(apple), "Inventory should contain the added item.");
  }

  @Test
  void addItem_ShouldThrowIllegalArgumentException_WhenItemIsNull() {
    assertThrows(IllegalArgumentException.class, () -> inventory.addItem(null, "Fruits"),
        "Adding null item should throw IllegalArgumentException.");
  }

  @Test
  void addItem_ShouldThrowIllegalArgumentException_WhenCategoryIsNull() {
    Grocery apple = new Grocery("Apple", "1 kg");
    assertThrows(IllegalArgumentException.class, () -> inventory.addItem(apple, null),
        "Adding item to a null category should throw IllegalArgumentException.");
  }

  @Test
  void addItem_ShouldThrowIllegalArgumentException_WhenCategoryIsEmpty() {
    Grocery apple = new Grocery("Apple", "1 kg");
    assertThrows(IllegalArgumentException.class, () -> inventory.addItem(apple, "  "),
        "Adding item to an empty category should throw IllegalArgumentException.");
  }





  // Positive and negative tests for removeInventoryItem method:
  @Test
  void removeInventoryItem_ShouldRemoveItem_WhenItemExists() {
    Grocery banana = new Grocery("Banana", "1 dozen");
    inventory.addItem(banana, "Fruits");
    boolean result = inventory.removeInventoryItem(banana, "Fruits");
    assertTrue(result, "Item should be removed successfully.");
    assertFalse(inventory.getGroceriesForCategory("Fruits").contains(banana),
        "Item should no longer be in the inventory.");
  }

  @Test
  void removeInventoryItem_ShouldReturnFalse_WhenItemDoesNotExist() {
    Grocery banana = new Grocery("Banana", "1 dozen");
    assertFalse(inventory.removeInventoryItem(banana, "Fruits"),
        "Removing a non-existing item should return false.");
  }





  // Positive and negative tests for getGroceriesForCategory method:
  @Test
  void getGroceriesForCategory_ShouldReturnEmptyList_WhenCategoryDoesNotExist() {
    assertTrue(inventory.getGroceriesForCategory("Vegetables").isEmpty(),
        "Querying a non-existing category should return an empty list.");
  }

  @Test
  void getGroceriesForCategory_ShouldThrowIllegalArgumentException_WhenCategoryIsNull() {
    assertThrows(IllegalArgumentException.class, () -> inventory.getGroceriesForCategory(null),
        "Querying with a null category should throw IllegalArgumentException.");
  }

  @Test
  void getGroceriesForCategory_ShouldThrowIllegalArgumentException_WhenCategoryIsEmpty() {
    assertThrows(IllegalArgumentException.class, () -> inventory.getGroceriesForCategory("  "),
        "Querying with an empty category should throw IllegalArgumentException.");
  }





  // Positive and negative tests for getInventoryItems method:
  @Test
  void getInventoryItems_ShouldReturnAllItems_WhenMultipleCategoriesExist() {
    inventory.addItem(new Grocery("Apple", "1 kg"), "Fruits");
    inventory.addItem(new Grocery("Carrot", "500 g"), "Vegetables");
    List<Grocery> items = inventory.getInventoryItems();
    assertEquals(2, items.size(), "Should return all items from all categories.");
  }

  @Test
  void getCategories_ShouldReturnAllCategories() {
    inventory.addItem(new Grocery("Apple", "1 kg"), "Fruits");
    inventory.addItem(new Grocery("Carrot", "500 g"), "Vegetables");
    List<String> categories = inventory.getCategories();
    assertEquals(2, categories.size(), "Should return all category names.");
    assertTrue(categories.contains("Fruits") && categories.contains("Vegetables"),
        "Should contain both 'Fruits' and 'Vegetables' categories.");
  }
}
