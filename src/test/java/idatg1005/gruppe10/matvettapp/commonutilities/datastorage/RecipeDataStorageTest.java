package idatg1005.gruppe10.matvettapp.commonutilities.datastorage;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import com.google.gson.reflect.TypeToken;
import idatg1005.gruppe10.matvettapp.backend.food.Recipe;
import idatg1005.gruppe10.matvettapp.backend.managerclasses.RecipeManager;
import idatg1005.gruppe10.matvettapp.commonutilities.errorhandler.CustomExceptionUtility.CustomIoException;

import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.Set;
import java.io.IOException;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.Mock;
import com.google.gson.Gson;

/**
 * Test class for RecipeDataStorage using Mockito to mock filesystem interactions and dependencies.
 * These tests ensure that RecipeDataStorage correctly handles file operations for saving and loading
 * recipe data, and properly reacts to file-related errors.
 */
@ExtendWith(MockitoExtension.class)
class RecipeDataStorageTest {



  @Mock     // Mock RecipeManager dependency
  private RecipeManager recipeManager;
  private RecipeDataStorage recipeDataStorage;




  // file paths for favorites and recently viewed recipes
  private static final Path DATA_STORAGE_DIR = Paths.get(System.getProperty("user.home"), ".MatVett", "datastorage");
  private static final Path FAVORITES_FILE_PATH = DATA_STORAGE_DIR.resolve("favorites.json");
  private static final Path RECENTLY_VIEWED_FILE_PATH = DATA_STORAGE_DIR.resolve("recentlyViewed.json");
  private MockedStatic<Files> mockedFiles;  // mock static Files methods




  // sample recipes for testing:
  Recipe cakeRecipe = new Recipe("Cake", "recipe", "ingredients", "instructions", "/path/to/image", "instructions");
  Recipe pastaRecipe = new Recipe("Pasta", "recipe", "ingredients", "instructions", "/path/to/image", "instructions");





  /**
   * Sets up the environment for each test.
   * Mocks the static Files methods to prevent actual file system access.
   */
  @BeforeEach
  void setUp() {
    mockedFiles = mockStatic(Files.class);
    recipeDataStorage = new RecipeDataStorage(recipeManager);
  }


  /**
   * Tears down the environment after each test.
   */
  @AfterEach
  void tearDown() {
    mockedFiles.close();
  }





  /**
   * Test successful creation of necessary directories during initialization of RecipeDataStorage.
   */
  @Test
  void testDirectoryCreation() {
    mockedFiles.verify(() -> Files.createDirectories(DATA_STORAGE_DIR), times(1));
  }





  /**
   * Tests the successful saving of favorite recipes to a file.
   */
  @Test
  void testSaveFavoritesSuccess() {
    Set<Recipe> favorites = Set.of(cakeRecipe, pastaRecipe);

    mockedFiles.when(() ->   // mocking the Files.write method to prevent actual file writing
        Files.write(eq(FAVORITES_FILE_PATH), any(byte[].class))).thenReturn(null);

    recipeDataStorage.saveFavorites(favorites);  // call the method to save favorites

    mockedFiles.verify(() ->   // to verify that the method was called with the correct arguments
        Files.write(eq(FAVORITES_FILE_PATH), any(byte[].class)), times(1));
  }





  /**
   * Tests the behavior when an IOException is encountered while saving favorites.
   */
  @Test
  void testSaveFavoritesFailure() {
    Set<Recipe> favorites = Set.of(cakeRecipe, pastaRecipe);


    mockedFiles.when(() ->     // mocking the Files.write method to throw an IOException
        Files.write(eq(FAVORITES_FILE_PATH), any(byte[].class)))  // any(byte[].class) to match the method signature
        .thenThrow(new IOException("Failed to write"));   // throw an IOException when the method is called

    // assert that the correct exception is thrown when an IOException occurs
    assertThrows(CustomIoException.class, () -> recipeDataStorage.saveFavorites(favorites));
  }





  /**
   * Tests the successful loading of favorite recipes from a file.
   */
  @Test
  void testLoadFavoritesSuccess() {
    // Initial mock setup for loading favorites:
    String json = new Gson().toJson(Set.of("Chocolate Cake", "Pasta Bolognese"));

    // mocking the Files.exists and Files.readAllBytes methods to return the expected JSON data
    mockedFiles.when(() -> Files.exists(FAVORITES_FILE_PATH)).thenReturn(true);
    mockedFiles.when(() -> Files.readAllBytes(FAVORITES_FILE_PATH)).thenReturn(json.getBytes());

    // mock the RecipeManager to return the correct Recipe objects when requested
    when(recipeManager.getRecipeByName("Chocolate Cake")).thenReturn(cakeRecipe);
    when(recipeManager.getRecipeByName("Pasta Bolognese")).thenReturn(pastaRecipe);

    Set<Recipe> loadedFavorites = recipeDataStorage.loadFavorites();  // call the method to load favorites

    assertFalse(loadedFavorites.isEmpty(), "Loaded favorites should not be empty");        // assert that the loaded set is not empty
    assertEquals(2, loadedFavorites.size(), "Should load two favorite recipes");  // assert that the correct number of recipes was loaded
  }






  /**
   * Tests the behavior when an IOException is encountered while loading favorites.
   */
  @Test
  void testLoadFavoritesFailure() {
    mockedFiles.when(() -> Files.exists(FAVORITES_FILE_PATH)).thenReturn(true);
    mockedFiles.when(() -> Files.readAllBytes(FAVORITES_FILE_PATH)).thenThrow(new IOException("Failed to read"));

    assertThrows(CustomIoException.class, () -> recipeDataStorage.loadFavorites());  // assert that the correct exception is thrown
  }






  /**
   * Tests the removal of a specific recipe from favorites.
   */
  // TODO: Implement this test
  // can not resolve the "org.mockito.exceptions.base.MockitoException:
  // For java.nio.file.Files, static mocking is already registered in the current thread" issue
  // but the loading and saving of favorites works fine
//  @Test
//  void testRemoveRecipeFromFavorites() {
//    // Define the response of your mocked methods
//    Gson gson = new Gson();
//    String initialJson = gson.toJson(Set.of("Chocolate Cake", "Pasta Bolognese"));
//
//    // Setup Recipe instances
//
//    // Using try-with-resources to ensure the mocked static Files class is closed properly
//    try (MockedStatic<Files> mockedFiles = mockStatic(Files.class)) {
//      mockedFiles.when(() -> Files.exists(FAVORITES_FILE_PATH)).thenReturn(true);
//      mockedFiles.when(() -> Files.readAllBytes(FAVORITES_FILE_PATH)).thenReturn(initialJson.getBytes());
//
//      when(recipeManager.getRecipeByName("Chocolate Cake")).thenReturn(cakeRecipe);
//      when(recipeManager.getRecipeByName("Pasta Bolognese")).thenReturn(pastaRecipe);
//
//      // Action: Attempt to remove "Chocolate Cake"
//      recipeDataStorage.removeRecipeFromFavorites("Chocolate Cake");
//
//      // Verification
//      ArgumentCaptor<byte[]> argumentCaptor = ArgumentCaptor.forClass(byte[].class);
//      mockedFiles.verify(() -> Files.write(eq(FAVORITES_FILE_PATH), argumentCaptor.capture()), times(1));
//
//      String capturedJson = new String(argumentCaptor.getValue(), StandardCharsets.UTF_8);
//      Set<String> updatedFavorites = gson.fromJson(capturedJson, new TypeToken<Set<String>>(){}.getType());
//
//      assertEquals(1, updatedFavorites.size(), "Only one favorite should remain after removal.");
//      assertTrue(updatedFavorites.contains("Pasta Bolognese"), "Updated favorites should contain 'Pasta Bolognese'.");
//      assertFalse(updatedFavorites.contains("Chocolate Cake"), "Updated favorites should not contain 'Chocolate Cake'.");
//    }
//  }











  /**
   * Tests the saving and loading of recently viewed recipes.
   */
  @Test
  void testRecentlyViewedRecipes() throws IOException {
    Set<String> recentlyViewed = Set.of("Chocolate Cake", "Pasta Bolognese");
    String json = new Gson().toJson(recentlyViewed);

    mockedFiles.when(() -> Files.write(eq(RECENTLY_VIEWED_FILE_PATH), eq(json.getBytes()))).thenReturn(null);  // first save operation, and then load operation
    recipeDataStorage.saveRecentlyViewed(recentlyViewed);   // save the recently viewed recipes

    // verify that the save operation was called with the correct arguments
    mockedFiles.verify(() -> Files.write(eq(RECENTLY_VIEWED_FILE_PATH), eq(json.getBytes())), times(1));

    mockedFiles.when(() -> Files.exists(RECENTLY_VIEWED_FILE_PATH)).thenReturn(true);
    mockedFiles.when(() -> Files.readAllBytes(RECENTLY_VIEWED_FILE_PATH)).thenReturn(json.getBytes());

    Set<String> loadedRecentlyViewed = recipeDataStorage.loadRecentlyViewed();
    assertEquals(recentlyViewed, loadedRecentlyViewed, "Recently viewed recipes should match saved ones");
  }

}
