package idatg1005.gruppe10.matvettapp.commonutilities.datastorage;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import com.google.gson.reflect.TypeToken;
import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import idatg1005.gruppe10.matvettapp.backend.storageclasses.ShoppingList;
import idatg1005.gruppe10.matvettapp.commonutilities.errorhandler.CustomExceptionUtility.CustomIoException;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;

import com.google.gson.Gson;



/**
 * Test class for ShoppingListDataStorage using Mockito to mock filesystem interactions.
 * These tests ensure that ShoppingListDataStorage handles file operations for saving and loading
 * shopping list data properly, and reacts appropriately to file-related errors.
 */
@ExtendWith(MockitoExtension.class)
class ShoppingListDataStorageTest {



  private ShoppingListDataStorage shoppingListDataStorage;   // ShoppingListDataStorage object to test
  private MockedStatic<Files> mockedFiles;     // mock static Files methods


  // file paths for shopping list data storage
  private static final Path DATA_STORAGE_DIR = Paths.get(System.getProperty("user.home"), ".MatVett", "datastorage");
  private static final Path SHOPPING_LIST_FILE_PATH = DATA_STORAGE_DIR.resolve("shoppingList.json");
  private static final Path MY_SHOPPING_LISTS_FILE_PATH = DATA_STORAGE_DIR.resolve("myShoppingLists.json");



  Grocery grocery1 = new Grocery("Apples", "2kg"); // sample grocery item for testing




  /**
   * Sets up the environment for each test.
   * Mocks the static Files methods to prevent actual file system access.
   */
  @BeforeEach
  void setUp() {
    mockedFiles = mockStatic(Files.class);
    shoppingListDataStorage = new ShoppingListDataStorage();
  }



  /**
   * Tears down the environment after each test.
   */
  @AfterEach
  void tearDown() {
    mockedFiles.close();
  }





  /**
   * Tests successful directory creation during initialization of ShoppingListDataStorage.
   */
  @Test
  void testDirectoryCreationOnInitialization() {
    mockedFiles.verify(() ->
        Files.createDirectories(DATA_STORAGE_DIR), times(1));
  }





  /**
   * Tests saving a single shopping list successfully to the file system.
   */
  @Test
  void testSaveShoppingListSuccess() {
    ShoppingList shoppingList = new ShoppingList();
    shoppingList.addGroceryItem(grocery1);

    mockedFiles.when(() -> Files.write(eq(SHOPPING_LIST_FILE_PATH), any(byte[].class))).thenReturn(null);

    shoppingListDataStorage.saveShoppingList(shoppingList);

    mockedFiles.verify(() -> Files.write(eq(SHOPPING_LIST_FILE_PATH), any(byte[].class)), times(1));
  }





  /**
   * Tests behavior when an IOException is encountered while saving a shopping list.
   */
  @Test
  void testSaveShoppingListFailure() {
    ShoppingList shoppingList = new ShoppingList();
    shoppingList.addGroceryItem(grocery1);

    mockedFiles.when(() -> Files.write(eq(SHOPPING_LIST_FILE_PATH), any(byte[].class)))
        .thenThrow(new IOException("Failed to write"));

    assertThrows(CustomIoException.class, () -> shoppingListDataStorage.saveShoppingList(shoppingList));
  }






  /**
   * Tests behavior when an IOException is encountered while loading a shopping list.
   */
  @Test
  void testLoadShoppingListFailure() {
    mockedFiles.when(() -> Files.exists(SHOPPING_LIST_FILE_PATH)).thenReturn(true);
    mockedFiles.when(() -> Files.readAllBytes(SHOPPING_LIST_FILE_PATH)).thenThrow(new IOException("Failed to read"));

    assertThrows(CustomIoException.class, () -> shoppingListDataStorage.loadShoppingList());
  }





  /**
   * Tests saving and loading multiple named shopping lists successfully.
   */
  @Test
  void testSaveAndLoadMyShoppingListsSuccess() {
    Map<String, ShoppingList> shoppingLists = new HashMap<>();
    shoppingLists.put("Weekly", new ShoppingList());
    shoppingLists.put("Party", new ShoppingList());

    String json = new Gson().toJson(shoppingLists);
    mockedFiles.when(() -> Files.write(eq(MY_SHOPPING_LISTS_FILE_PATH), eq(json.getBytes()))).thenReturn(null);
    shoppingListDataStorage.saveMyShoppingLists(shoppingLists);

    mockedFiles.verify(() -> Files.write(eq(MY_SHOPPING_LISTS_FILE_PATH), eq(json.getBytes())), times(1));

    mockedFiles.when(() -> Files.exists(MY_SHOPPING_LISTS_FILE_PATH)).thenReturn(true);
    mockedFiles.when(() -> Files.readAllBytes(MY_SHOPPING_LISTS_FILE_PATH)).thenReturn(json.getBytes(StandardCharsets.UTF_8));

    Map<String, ShoppingList> loadedLists = shoppingListDataStorage.loadMyShoppingLists();
    assertEquals(2, loadedLists.size(), "Should load two shopping lists");
  }






  /**
   * Tests deserialization of a basic grocery items.
   */
  @Test
  void testBasicGroceryDeserialization() {
    Gson gson = new Gson();  // create a new Gson object

    // JSON string representing a list of grocery items with empty quantities
    String json = "[{\"groceryItemName\":\"Olive Oil\",\"quantity\":\"\"},"
        + "{\"groceryItemName\":\"Onion\",\"quantity\":\"\"}]";

    Type listType = new TypeToken<List<Grocery>>(){}.getType();  // create a Type object for List<Grocery>
    List<Grocery> groceries = gson.fromJson(json, listType);  // deserialize the JSON string

    assertEquals(2, groceries.size());
    assertEquals("Olive Oil", groceries.getFirst().getGroceryItemName());
    assertEquals("", groceries.getFirst().getQuantity());
  }







  //TODO: StackOverflowError when running this test, investigate and fix
//  @Test
//  void testLoadShoppingListSuccess() {
//    String json = new Gson().toJson(Arrays.asList(
//        new Grocery("Olive Oil", ""),
//        new Grocery("Onion", ""),
//        new Grocery("Garlic Cloves", ""),
//        new Grocery("Crushed Tomatoes", ""),
//        new Grocery("Vegetable Broth", ""),
//        new Grocery("Fresh Basil", ""),
//        new Grocery("Salt and Pepper", ""),
//        new Grocery("Heavy Cream", "")
//    ));
//
//    mockedFiles.when(() -> Files.exists(SHOPPING_LIST_FILE_PATH)).thenReturn(true);
//    mockedFiles.when(() -> Files.readAllBytes(SHOPPING_LIST_FILE_PATH)).thenReturn(json.getBytes(StandardCharsets.UTF_8));
//
//    ShoppingList loadedList = shoppingListDataStorage.loadShoppingList();
//    assertFalse(loadedList.getGroceryItems().isEmpty(), "Loaded shopping list should not be empty");
//    assertTrue(loadedList.getGroceryItems().stream().anyMatch(item -> item.getGroceryItemName().equals("Olive Oil")), "Should contain 'Olive Oil'");
//  }


}
