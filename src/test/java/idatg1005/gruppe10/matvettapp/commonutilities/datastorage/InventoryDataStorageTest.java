package idatg1005.gruppe10.matvettapp.commonutilities.datastorage;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import idatg1005.gruppe10.matvettapp.backend.storageclasses.Inventory;
import idatg1005.gruppe10.matvettapp.commonutilities.errorhandler.CustomExceptionUtility.CustomIoException;
import java.nio.file.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.MockedStatic;
import java.io.IOException;

/**
 * Test class for InventoryDataStorage using Mockito to mock filesystem interactions.
 * This class ensures that the InventoryDataStorage handles file operations correctly
 * and throws the appropriate exceptions when errors occur.
 */
@ExtendWith(MockitoExtension.class)
class InventoryDataStorageTest {

  private MockedStatic<Files> mockedFiles;  // Mocked static block for Files class



  /**
   * Sets up the test environment before each test method is executed.
   * This includes mocking the Files static methods to prevent actual file system access.
   */
  @BeforeEach
  void setUp() {
    mockedFiles = mockStatic(Files.class);

    // mocking Files class methods to prevent actual file system access
    mockedFiles.when(() -> Files.createDirectories(any(Path.class))).thenReturn(null);
    mockedFiles.when(() -> Files.exists(any(Path.class))).thenReturn(true);
    mockedFiles.when(() -> Files.readAllBytes(any(Path.class))).thenReturn("{}".getBytes());
  }


  /**
   * Clean up after each test method by closing the mocked static block.
   */
  @AfterEach
  void tearDown() {
    mockedFiles.close();  // close the mocked static block
  }





  /**
   * Test to ensure that a new inventory file is created if it does not already exist.
   * This test simulates the file creation process when initializing the InventoryDataStorage class.
   */
  @Test
  void testCreateNewInventoryOnNonExistentFile() {
    mockedFiles.when(() -> Files.exists(any(Path.class))).thenReturn(false);
    new InventoryDataStorage(); // reinitialized to simulate file creation

    Path expectedPath = Paths.get(System.getProperty("user.home"), ".MatVett", "datastorage", "inventory.json").toAbsolutePath().normalize();
    mockedFiles.verify(() -> Files.write(eq(expectedPath), any(byte[].class))); // check for if the file is created
  }




  /**
   * Test to check successful loading of inventory data from a file.
   * This test simulates reading valid JSON data from the filesystem and checks if the inventory is loaded correctly.
   */
  @Test
  void testLoadInventorySuccess() {
    String sampleJson = "{\"Vegetables\": [{\"name\": \"Carrot\", \"quantity\": \"10\"}]}";  // sample JSON data

    mockedFiles.when(()   // mock the readAllBytes method to return the sample JSON data
        -> Files.readAllBytes(any(Path.class))).thenReturn(sampleJson.getBytes());

    // loading the inventory and verifying that it is not empty
    InventoryDataStorage storage = new InventoryDataStorage();
    Inventory loadedInventory = storage.loadInventory();

    // check if the inventory is not empty, and contains the expected grocery item from the sample JSON
    assertFalse(loadedInventory.getGroceriesForCategory("Vegetables").isEmpty(), "Loaded inventory should not be empty");
  }




  /**
   * Test to verify the behavior when an IOException is thrown during inventory loading.
   * This simulates a failure in reading the file, and checks if a CustomIoException is correctly thrown.
   */
  @Test
  void testLoadInventoryFailure() {
    mockedFiles.when(() -> Files.readAllBytes(any(Path.class))).thenThrow(new IOException("Failed to read"));

    // the constructor calls loadInventory, which should throw the CustomIoException
    assertThrows(CustomIoException.class,
        InventoryDataStorage::new,  // reinitializing to capture the exception during initialization
        "Expected CustomIoException to be thrown due to failed read");
  }




  /**
   * Test to ensure that inventory data can be successfully saved to a file.
   * This test adds an item to the inventory and saves it, verifying that the file write operation is called.
   */
  @Test
  void testSaveInventorySuccess() {
    InventoryDataStorage storage = new InventoryDataStorage();  // initialize the inventory
    storage.getInventory().addItem(new Grocery("Apple", "5 pcs"), "Fruits");  // add a grocery item to the inventory
    storage.saveInventory();  // save the inventory to file

    // verify that the write operation is called with the correct arguments
    mockedFiles.verify(() ->
        Files.write(any(Path.class), any(byte[].class))); // to ensure that data is written to file
  }




  /**
   * Test to verify the behavior when an IOException is thrown during inventory saving.
   * This simulates a failure in writing to the file, checking if a CustomIoException is correctly thrown.
   */
  @Test
  void testSaveInventoryFailure() {
    mockedFiles.when(() ->  // mock the write operation to throw an IOException
        Files.write(any(Path.class), any(byte[].class))).thenThrow(new IOException("Failed to write"));

    InventoryDataStorage storage = new InventoryDataStorage(); // initialize the inventory

    // add a grocery item to the inventory and attempt to save it
    assertThrows(CustomIoException.class,
        storage::saveInventory, // lambda to capture the exception during the save operation
        "Expected to throw, but it did not");
  }


}
