package idatg1005.gruppe10.matvettapp.commonutilities.filehandler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import idatg1005.gruppe10.matvettapp.commonutilities.errorhandler.CustomExceptionUtility.CustomIoException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;



/**
 * Tests the functionality of the GroceryFileHandler class.
 * This class checks the integrity and exception handling of loading grocery categories
 * and items from text files into a map structure.
 */
class GroceryFileHandlerTest {

  private GroceryFileHandler fileHandler;  // The file handler instance to be tested



  /**
   * Initializes a new instance of GroceryFileHandler before each test.
   */
  @BeforeEach
  void setUp() {
    fileHandler = new GroceryFileHandler();
  }






  /**
   * Test the correct loading of categories and items from a valid file.
   * Ensures that the map is not empty, contains specific categories, and correct number of items.
   *
   * @throws URISyntaxException if the URL to the test file is not properly formatted
   */
  @Test
  void testLoadCategoriesWithItems_ValidFile() throws URISyntaxException {
    // ClassLoader to get the resource URL
    URL resourceUrl = getClass().getClassLoader().getResource("txt_test_files/valid_test_file.txt");
    assertNotNull(resourceUrl, "The resource URL should be loaded");

    String filePath = Paths.get(resourceUrl.toURI()).toString();

    Map<String, List<Grocery>> groceries = fileHandler.loadCategoriesWithItems(filePath);
    assertFalse(groceries.isEmpty(), "Map should not be empty");
    assertTrue(groceries.containsKey("Fruits"), "Map should contain 'Fruits' category");
    assertEquals(3, groceries.get("Fruits").size(), "There should be 3 fruits listed under the 'Fruits' category");
  }





  /**
   * Test loading categories and items from an empty file.
   * Verifies that the map returned is empty, indicating no categories or items are loaded.
   *
   * @throws URISyntaxException if the URL to the test file is not properly formatted
   */
  @Test
  void testLoadCategoriesWithItems_EmptyFile() throws URISyntaxException {
    // ClassLoader to get the resource URL
    URL resourceUrl = getClass().getClassLoader().getResource("txt_test_files/empty_test_file.txt");
    assertNotNull(resourceUrl, "The resource URL should be loaded");

    String filePath = Paths.get(resourceUrl.toURI()).toString();

    Map<String, List<Grocery>> groceries = fileHandler.loadCategoriesWithItems(filePath);
    assertTrue(groceries.isEmpty(), "Map should be empty when file has no contents");
  }




  /**
   * Test the behavior when attempting to load categories and items from a non-existing file.
   */
  @Test
  void testLoadCategoriesWithItems_InvalidFile() {
    // ClassLoader to get the resource URL for a file that does not exist
    URL resourceUrl = getClass().getClassLoader().getResource("txt_test_files/invalid_test_file.txt");

    // if the URL is not null, it means the file unexpectedly exists.
    assertNull(resourceUrl, "The resource for the invalid file should not exist");

    String filePath = "non_existent_file.txt"; // a path that we know is invalid

    // expecting the CustomIoException to be thrown because the file does not exist
    assertThrows(CustomIoException.class, () -> fileHandler.loadCategoriesWithItems(filePath),
        "Expected CustomIoException to be thrown due to file read failure");
  }


}
