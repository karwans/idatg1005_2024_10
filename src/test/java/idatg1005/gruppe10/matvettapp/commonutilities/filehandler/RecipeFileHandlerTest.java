package idatg1005.gruppe10.matvettapp.commonutilities.filehandler;

import idatg1005.gruppe10.matvettapp.backend.food.Recipe;
import idatg1005.gruppe10.matvettapp.commonutilities.errorhandler.CustomExceptionUtility.CustomIoException;
import java.util.ArrayList;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Test class for RecipeFileHandler.
 * It includes both positive and negative tests to ensure that the file handling
 * and recipe processing functions operate as expected under various conditions.
 * The class uses a temporary directory for testing file operations without affecting the actual
 * file system outside the test environment.
 *
 */
class RecipeFileHandlerTest {



  private static final String TEST_DIR = "testRecipes"; // directory used for test files
  private static Path testDirectory; // path representation of the test directory





  /**
   * Sets up the environment before any tests are run.
   * This method creates a test directory and a test file that contains sample recipe data.
   * It is executed once before all tests.
   *
   * @throws IOException if there is an error creating the test directory or file
   */
  @BeforeAll
  static void setup() throws IOException {
    testDirectory = Paths.get(TEST_DIR); // creating the test directory
    Files.createDirectories(testDirectory);  // creating the directory if it does not exist
    Path file = testDirectory.resolve("testRecipe.txt");     // a sample recipe file
    Files.writeString(file,
        "Name: Chocolate Cake\nCategory: Dessert\nImagePath: cake.jpg\nPrepTime: 20 min\nCookTime: 30 min\nIngredients:\n- Sugar\n- Flour\n- Cocoa\n- Eggs\nInstructions:\nMix ingredients. Bake.");
  }




  /**
   * Cleans up the environment after all tests are completed.
   * This method deletes the test directory and any files it contains.
   * It ensures that the file system is clean and no test artifacts remain.
   *
   * @throws IOException if there is an error deleting the files or directories
   */
  @AfterAll
  static void teardown() throws IOException {
    // cleaning up the test directory
    Files.walk(testDirectory)
        .sorted((a, b) -> b.compareTo(a)) // sort in reverse, so files are deleted before the directory
        .forEach(path -> {
          try {
            Files.delete(path);
          } catch (IOException e) {
            fail("Could not clean up test directory");
          }
        });
  }






  /**
   * Tests the ability to load all recipes from a valid directory.
   * Verifies that the RecipeFileHandler correctly parses recipe data into Recipe objects and adds them to a list.
   * This test expects to successfully find a specific recipe ("Chocolate Cake") among the loaded recipes.
   */
  @Test
  void loadAllRecipes_ValidDirectory_SuccessfullyLoadsRecipes() {
    List<Recipe> loadedRecipes = new ArrayList<>();

    assertDoesNotThrow(() -> {
      RecipeFileHandler.loadAllRecipes(TEST_DIR, loadedRecipes::add);
    });

    // checking that the specific recipe "Chocolate Cake" is present
    assertTrue(loadedRecipes.stream().anyMatch(recipe -> "Chocolate Cake".equals(recipe.getRecipeName())),
        "Expected 'Chocolate Cake' not found in loaded recipes");
  }




  /**
   * Tests the handling of an invalid directory when attempting to load recipes.
   * This test expects the method to throw a CustomIoException, indicating an error due to the nonexistent directory.
   */
  @Test
  void loadAllRecipes_InvalidDirectory_ThrowsCustomIoException() {
    assertThrows(CustomIoException.class, () -> RecipeFileHandler.loadAllRecipes("invalidDir", recipe -> {}));
  }




  /**
   * Tests reading a recipe from a valid file.
   * Verifies that the RecipeFileHandler correctly reads and parses a single recipe file into a Recipe object.
   * This test ensures that the Recipe object's properties match the expected values.
   */
  @Test
  void readRecipeFromFile_ValidFile_SuccessfullyReadsRecipe() {
    try {
      Recipe recipe = RecipeFileHandler.readRecipeFromFile(TEST_DIR + "/testRecipe.txt");
      assertNotNull(recipe);
      assertEquals("Chocolate Cake", recipe.getRecipeName());
    } catch (CustomIoException e) {
      fail("Exception should not have been thrown");
    }
  }




  /**
   * Tests reading a recipe from an invalid file.
   * This test expects the method to throw a CustomIoException, indicating an error due to the nonexistent file.
   */
  @Test
  void readRecipeFromFile_InvalidFile_ThrowsCustomIoException() {
    assertThrows(CustomIoException.class, () -> RecipeFileHandler.readRecipeFromFile("nonExistentFile.txt"));
  }




  /**
   * Tests saving a recipe to a file with valid input parameters.
   * Verifies that the RecipeFileHandler correctly writes the recipe data into a file and that the file exists afterwards with correct content.
   */
  @Test
  void saveRecipeToFile_ValidInput_SuccessfullySavesRecipe() {
    Recipe recipe = new Recipe("Apple Pie", "Dessert", "15 min", "45 min", "pie.jpg", "Bake it");
    recipe.setIngredientDescriptions(List.of("Apples", "Sugar", "Pie crust"));
    String filePath = TEST_DIR + "/applePie.txt";
    assertDoesNotThrow(() -> RecipeFileHandler.saveRecipeToFile(recipe, filePath));

    // checking if file was created and contents are correct
    assertTrue(Files.exists(Paths.get(filePath)));
  }




  /**
   * Tests saving a recipe to an invalid file path.
   * This test expects the method to throw a CustomIoException, indicating an error due to the invalid file path.
   */
  @Test
  void saveRecipeToFile_InvalidPath_ThrowsCustomIoException() {
    Recipe recipe = new Recipe("Apple Pie", "Dessert", "15 min", "45 min", "pie.jpg", "Bake it");
    assertThrows(CustomIoException.class, () -> RecipeFileHandler.saveRecipeToFile(recipe, "/invalid/path/applePie.txt"));
  }
}
