package idatg1005.gruppe10.matvettapp.frontend.controllers;


import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.mockito.Mockito.*;

import idatg1005.gruppe10.matvettapp.frontend.controllers.HomeController;
import idatg1005.gruppe10.matvettapp.frontend.guiclasses.HomeGui;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.JFXPanel;
import javafx.scene.control.Button;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.function.Consumer;
import org.mockito.ArgumentCaptor;
import org.mockito.MockedStatic;



/**
 * Tests for the HomeController class focusing on the behavior during null scenarios for tab switcher.
 */
class HomeControllerTest {

  private Consumer mockTabSwitcher;
  private HomeController controller;

  @BeforeEach
  void setUp() {
    new JFXPanel(); // JavaFX toolkit to support JavaFX components like Buttons in tests

    // Mocking the HomeGui to simulate GUI interactions
    HomeGui mockHomeGui = mock(HomeGui.class);
    mockTabSwitcher = mock(Consumer.class);

    // Mock the buttons to ensure they are not null and prevent NullPointerException
    when(mockHomeGui.getRecipeButton()).thenReturn(new Button("Recipes"));
    when(mockHomeGui.getShoppingListButton()).thenReturn(new Button("Shopping List"));
    when(mockHomeGui.getInventoryButton()).thenReturn(new Button("Inventory"));

    // Initializing HomeController with the mocked HomeGui
    controller = new HomeController(mockHomeGui);
    controller.setOnTabSwitchRequested(mockTabSwitcher);
  }




  @Test
  void testHandleButtonAction_Success() {
    // Setup: no exception expected to be thrown
    doNothing().when(mockTabSwitcher).accept(anyString());
    controller.handleButtonAction("Recipes");
    verify(mockTabSwitcher, times(1)).accept("Recipes");
  }





  /**
   * Tests that the logger correctly logs a severe level message when the tab switcher is not set (null).
   * This simulates the scenario where an action is attempted with no tab switcher configured.
   */
  @Test
  void testHandleButtonAction_ForNull() {
    // Mock local HomeGui and Buttons to ensure the setup is isolated to this test:
    HomeGui localMockHomeGui = mock(HomeGui.class);
    Button mockButton = mock(Button.class);

    // Mocking the buttons to ensure they are not null:
    when(localMockHomeGui.getRecipeButton()).thenReturn(mockButton);
    when(localMockHomeGui.getShoppingListButton()).thenReturn(mockButton);
    when(localMockHomeGui.getInventoryButton()).thenReturn(mockButton);

    try (MockedStatic<Logger> mockedLogger = mockStatic(Logger.class)) {
      Logger mockLogger = mock(Logger.class);
      // Ensuring the logger is lockable and logs at SEVERE level:
      when(mockLogger.isLoggable(Level.SEVERE)).thenReturn(true);
      mockedLogger.when(() -> Logger.getLogger(anyString())).thenReturn(mockLogger);

      HomeController localController = new HomeController(localMockHomeGui);
      localController.setOnTabSwitchRequested(null);  // Explicitly setting tabSwitcher to null to test error handling


      localController.handleButtonAction(null); // Simulating the action with null tab name to trigger the logging

      // Capturing the exception to verify the correct type is passed to the logger:
      ArgumentCaptor<Exception> exceptionCaptor = ArgumentCaptor.forClass(Exception.class);
      verify(mockLogger).log(eq(Level.SEVERE),
          eq("Unexpected Error: Tab switcher consumer is not initialized."),
          exceptionCaptor.capture());

      // Asserting the captured exception is an instance of IllegalStateException
      assertInstanceOf(IllegalStateException.class, exceptionCaptor.getValue(),
          "The caught exception should be an IllegalStateException");
    }
  }


}
