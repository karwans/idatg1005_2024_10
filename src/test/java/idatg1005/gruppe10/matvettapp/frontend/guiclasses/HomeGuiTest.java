package idatg1005.gruppe10.matvettapp.frontend.guiclasses;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import idatg1005.gruppe10.matvettapp.frontend.guiclasses.HomeGui;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import javafx.scene.control.Button;
import javafx.embed.swing.JFXPanel; // Needed to initialize JavaFX toolkit

class HomeGuiTest {

  private HomeGui homeGui;



  @BeforeEach
  void setUp() {
    new JFXPanel(); // Initialization og JavaFX environment
    homeGui = new HomeGui("/images/home.png",
        "/css/home-style.css");
  }



  @Test
  void testButtonInitialization() {
    Button recipeButton = homeGui.getRecipeButton();
    assertNotNull(recipeButton);
    assertEquals("Recipes", recipeButton.getText());
  }
}
