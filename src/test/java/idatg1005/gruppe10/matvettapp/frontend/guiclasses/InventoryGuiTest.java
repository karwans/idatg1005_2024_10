package idatg1005.gruppe10.matvettapp.frontend.guiclasses;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import idatg1005.gruppe10.matvettapp.backend.food.Grocery;
import idatg1005.gruppe10.matvettapp.backend.storageclasses.Inventory;
import idatg1005.gruppe10.matvettapp.frontend.controllers.InventoryController;
import idatg1005.gruppe10.matvettapp.frontend.guiclasses.InventoryGui;
import javafx.application.Platform;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import javafx.embed.swing.JFXPanel;
import javafx.scene.control.TableView;
import java.util.Collections;
import java.util.List;
import org.testfx.util.WaitForAsyncUtils;

class InventoryGuiTest {



  private InventoryGui gui;
  private Inventory inventoryMock;
  private InventoryController controllerMock;



  // gui and mock initialization
//  @BeforeEach
//  void setUp() {
//    new JFXPanel();
//
//    // mock of the Inventory class
//    inventoryMock = mock(Inventory.class);
//
//    // mock of the InventoryController class
//    controllerMock = mock(InventoryController.class);
//    when(inventoryMock.getInventoryItems()).thenReturn(Collections.emptyList());
//
//    gui = new InventoryGui(inventoryMock, controllerMock);
//    controllerMock = new InventoryController(inventoryMock);
//  }
  @BeforeEach
  void setUp() {
    new JFXPanel(); // Initializes JavaFX toolkit

    inventoryMock = mock(Inventory.class);
    controllerMock = mock(InventoryController.class);

    // Configure the mock to return an empty list initially
    when(inventoryMock.getInventoryItems()).thenReturn(Collections.emptyList());

    // Instantiate GUI with the mocked controller and inventory
    gui = new InventoryGui(inventoryMock, controllerMock);

    // Ensure that the controller's actions that update the GUI are simulated
    doAnswer(invocation -> {
      gui.updateTableView();
      return null;
    }).when(controllerMock).updateTableView();
  }






  /**
   * Test to verify that the table view is empty when the GUI is initialized.
   */
  @Test
  void testGuiInitialization_CheckTableEmpty() {
    TableView<Grocery> tableView = gui.getTableView();
    assertTrue(tableView.getItems().isEmpty());
  }



  /**
   * Test to verify that the table view is updated when the updateTableView method is called.
   */
//  @Test
//  void testUpdateTableView_ItemsPresent() {
//    when(inventoryMock.getInventoryItems()).
//        thenReturn(List.of(new Grocery("Apples", "5 kg")));
//
//    controllerMock.updateTableView();
//    TableView<Grocery> tableView = gui.getTableView();
//    assertFalse(tableView.getItems().isEmpty());
//  }
  @Test
  void testUpdateTableView_ItemsPresent() throws InterruptedException {
    when(inventoryMock.getInventoryItems()).thenReturn(List.of(new Grocery("Apples", "5 kg")));

    // Run update on the JavaFX thread and wait for completion
    Platform.runLater(() -> controllerMock.updateTableView());
    WaitForAsyncUtils.waitForFxEvents();  // Wait for JavaFX operations to complete

    TableView<Grocery> tableView = gui.getTableView();
    assertFalse(tableView.getItems().isEmpty(), "Table should not be empty after items are added");
  }



}

